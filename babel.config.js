module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    ['module-resolver', {
        root: ['./App'],
        "extensions": [".ios.js", ".android.js", ".js", ".json", ".service.js", ".schema.js"],
        "alias": {
          "~": "./App",
          "~/Services": "./App/Services",
          "~/Themes": "./App/Themes",
          "~/shared": "./App/shared",
          "~/Views": "./App/Views",
          "~/Components": "./App/Components",
        }
    }],
    ["module:react-native-dotenv",{
      envName: "APP_ENV",
      moduleName: "@env",
      path: ".env"
    }]
  ],
};
