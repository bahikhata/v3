import {realm} from 'services/realm.schema';
import {useStore} from '../store';
import {generateObjectId} from '../utils';
import {useAccount} from './useAccount';
import {useEffect, useState} from 'react';

export function useTransaction() {
  const userId = useStore(state => {
    return state.user?.objectId;
  });

  const {createAccount, updateAccountBalance} = useAccount();
  const [refresh, setRefresh] = useState(0);

  useEffect(() => {
    realm.addListener('change', () => {
      setRefresh(refresh + 1);
    });
  }, [setRefresh]);

  return {
    addTransaction: (
      _account,
      amount,
      detail,
      attachment,
      date,
      multiplier,
    ) => {
      const accounts = realm
        .objects('Account')
        .filtered(`name = "${_account}"`);
      const result = {data: null};

      const amountToBeAdded = amount * multiplier;
      const _attachment = attachment?.uri || '';
      if (accounts.length > 0) {
        realm.write(() => {
          result.data = realm.create('Transaction', {
            objectId: generateObjectId(),
            date: date.toISOString(),
            accountId: accounts[0].objectId,
            attachment: _attachment,
            amount: amountToBeAdded,
            detail,
            userId,
            type: '',
          });
        });
        updateAccountBalance(accounts[0].objectId, amountToBeAdded);
      } else {
        let accountId = '';
        if (_account.trim() !== '') {
          const account = createAccount(_account, '', amountToBeAdded);
          accountId = account[0].objectId;
        }

        realm.write(() => {
          result.data = realm.create('Transaction', {
            objectId: generateObjectId(),
            date: date,
            accountId: accountId,
            attachment: _attachment,
            amount: amountToBeAdded,
            detail,
            userId,
            type: '',
          });
        });
      }

      return result;
    },
    getTransactionsByDate: (from, to) => {
      const transactions = realm
        .objects('Transaction')
        .filtered(
          `date >= $0 AND date <= $1 AND userId = $2`,
          from,
          to,
          userId,
        ).sorted('date', false);

      return transactions;
    },
    getCloseAmount: _date => {
      const transactions = realm
        .objects('Transaction')
        .filtered(
          `date <= $0 AND cancelled = false AND userId = $1`,
          _date,
          userId,
        );
      let total = 0;
      transactions.forEach(transaction => {
        total += transaction.amount;
      });
      return total;
    },
    getOpenAmount: _date => {
      const transactions = realm
        .objects('Transaction')
        .filtered(
          `date < $0 AND cancelled = false AND userId = $1`,
          _date,
          userId,
        );
      let total = 0;
      transactions.forEach(transaction => {
        total += transaction.amount;
      });
      return total;
    },
    updateTransaction: (id, _detail) => {
      realm.write(() => {
        const transaction = realm.objectForPrimaryKey('Transaction', id);
        transaction.detail = _detail;
        transaction.updatedAt = new Date();
      });
    },
    getOpeningBalance: (_id, _date) => {
      const transactions = realm
        .objects('Transaction')
        .filtered(
          `accountId = $0 AND date < $1 AND cancelled = false AND userId = $2`,
          _id,
          _date,
          userId,
        );
      let balance = 0;
      transactions.forEach(transaction => {
        balance += transaction.amount;
      });
      return balance;
    },
    getClosingBalance: (_id, _date) => {
      const transactions = realm
        .objects('Transaction')
        .filtered(
          `accountId = $0 AND date <= $1 AND cancelled = false AND userId = $2`,
          _id,
          _date,
          userId,
        );
      let balance = 0;
      transactions.forEach(transaction => {
        balance += transaction.amount;
      });
      return balance;
    },
    deleteTransaction: id => {
      realm.write(() => {
        const transaction = realm.objectForPrimaryKey('Transaction', id);
        transaction.cancelled = true;
        transaction.updatedAt = new Date();
      });
    },
    getTxnByMonth: (_id, from, to) => {
      const transactions = realm
        .objects('Transaction')
        .filtered(
          `accountId = $0 AND date >= $1 AND date <= $2 AND userId = $3`,
          _id,
          from,
          to,
          userId,
        );
      return transactions;
    },
  };
}
