import { realm } from 'services/realm.schema';
import { useStore } from '../store';
import { generateObjectId } from '../utils';
import { useEffect, useState } from 'react';

/**
 * This hook is used to manage the user account.
 */
export function useAccount() {
  const userId = useStore(state => state.user?.objectId);

  const [refresh, setRefresh] = useState(0);

  useEffect(() => {
    realm.addListener('change', () => {
      setRefresh(refresh + 1);
    });
  }, [setRefresh]);

  return {
    /**
     * Creates a new account.
     * @param {string} name - The name of the account.
     * @param {string} phone - The phone number associated with the account.
     * @param {number} openBalance - The initial balance of the account.
     */
    createAccount: (name, phone, openBalance = 0) => {
      realm.write(() => {
        realm.create('Account', {
          objectId: generateObjectId(),
          name,
          amount: openBalance,
          phone,
          userId: userId,
        });
      });

      return realm
        .objects('Account')
        .filtered(
          'name = $0 AND phone = $1 AND userId = $2',
          name,
          phone,
          userId,
        );
    },

    /**
     * Soft deletes an account.
     *
     * @param {string} accountId - The ID of the account to be deleted.
     */
    closeAccount: accountId => {
      // Soft delete an account
      realm.write(() => {
        realm.create(
          'Account',
          { objectId: accountId, cancelled: true, updatedAt: new Date() },
          'modified',
        );
      });
    },

    /**
     * Retrieves all accounts.
     *
     * @returns {Array} An array of all accounts.
     */
    getAccounts: () => {
      return realm.objects('Account');
    },
    updateAccountBalance: (accountId, balance) => {
      realm.write(() => {
        realm.create(
          'Account',
          {
            objectId: accountId,
            balance: balance,
            updatedAt: new Date(),
          },
          'modified',
        );
      });
    },
    checkAccount: (name, phone) => {
      return realm
        .objects('Account')
        .filtered('phone = $0', phone);
    },
    /**
     * Retrieves an account by its ID.
     *
     * @param {string} accountId - The ID of the account to retrieve.
     * @returns {Object} The account object.
     */
    getAccountById: accountId => {
      return realm.objectForPrimaryKey('Account', accountId);
    },
    reOpenAccount: accountId => {
      // Re-open a soft deleted account
      realm.write(() => {
        realm.create(
          'Account',
          { objectId: accountId, cancelled: false, updatedAt: new Date() },
          'modified',
        );
      });
    },
    getAccountByName: name => {
      const result = realm
        .objects('Account')
        .filtered('name = $0 AND userId = $1', name, userId);
      if (result.length > 0) {
        return result[0];
      }
      return null;
    },
    updateAccount: (id, account, phone) => {
      const newPhoneNumber = `+91${phone.replace(/\s/g, '')
        .slice(-10)}`
      
      realm.write(() => {
        realm.create(
          'Account',
          {
            objectId: id,
            name: account,
            phone: phone,
            updatedAt: new Date(),
          },
          'modified',
        );
      });
    },
    updateBalance: id => {
      if (!id) return;

      const transactions = realm
        .objects('Transaction')
        .filtered(
          `accountId = $0 AND cancelled = false AND userId = $1`,
          id,
          userId,
        );
      let balance = 0;
      transactions.forEach(txn => {
        balance += txn.amount;
      });
      realm.write(() => {
        realm.create(
          'Account',
          {
            objectId: id,
            balance: balance,
            updatedAt: new Date(),
          },
          'modified',
        );
      });
    },
    getReminderById: id => {
      return realm.objects('Account').filtered('objectId = $0', id);
    },
    addReminder: (id, dates) => {
      realm.write(() => {
        realm.create(
          'Account',
          {
            objectId: id,
            date: dates,
            enabled: true,
            updatedAt: new Date(),
          },
          true,
        );
      });
    },
    deleteReminderById: id => {
      realm.write(() => {
        const acc = realm.objectForPrimaryKey('Account', id);
        acc.date = undefined;
        acc.enabled = false;
        acc.updatedAt = new Date();
      });
    },
    setAutoSms: (id) => {
      realm.write(() => {
        const acc = realm.objectForPrimaryKey('Account', id);
        acc.checkboxSMS = !acc.checkboxSMS;
        acc.updatedAt = new Date();
      });
    }
  };
}
