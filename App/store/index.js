import { create } from 'zustand';
import { realm } from 'services/realm.schema';
import { debounce } from 'lodash';
import { syncToServer } from '../sync';
const moment = require("moment");

export const useStore = create((set, get) => ({
  user: null,
  realm: null,
  accounts: [],
  tnxs: [],
  cashIn: [],
  cashOut: [],
  givenPerAccount: [],
  receivedPerAccount: [],
  tnxsPerAccount: [],
  date: new Date(),
  khatapageTopName: '',
  openAmount: 0,
  closeAmount: 0,
  openBalancePerAccount: 0,
  closeBalancePerAccount: 0,
  accountId: null,
  accountDate: new Date(),
  cashbookEnabled: true,
  syncInfo: {
    accounts: {
      total: 0,
      synced: 0,
    },
    transactions: {
      total: 0,
      synced: 0,
    },
  },
  updateAccountBalance: id => {
    if (!id) return;
    const userId = get().user?.objectId;
    if (!userId) return;
    
    const transactions = realm
      .objects('Transaction')
      .filtered(
        `accountId = $0 AND cancelled = false AND userId = $1`,
        id,
        userId,
      );
    let balance = 0;
    transactions.forEach(txn => {
      balance += txn.amount;
    });
    realm.write(() => {
      realm.create(
        'Account',
        {
          objectId: id,
          balance: balance,
          updatedAt: new Date(),
        },
        'modified',
      );
    });
  },
  getOpeningBalance: () => {
    const userId = get().user?.objectId;
    const _id = get().accountId;
    const _date = moment(get().accountDate).startOf('month').toDate();

    if (!userId || !_id) {
      return 0;
    }
    const transactions = realm
      .objects('Transaction')
      .filtered(
        `accountId = $0 AND date < $1 AND cancelled = false AND userId = $2`,
        _id,
        _date,
        userId,
      );
    let balance = 0;
    transactions.forEach(transaction => {
      balance += transaction.amount;
    });
    return balance;
  },
  getClosingBalance: (id, date) => {
    const userId = get().user?.objectId;
    const _id = id || get().accountId;
    const _date = moment(date || get().accountDate)
      .endOf('month')
      .toDate();

    if (!userId || !_id) {
      return 0;
    }
    const transactions = realm
      .objects('Transaction')
      .filtered(
        `accountId = $0 AND date <= $1 AND cancelled = false AND userId = $2`,
        _id,
        _date,
        userId,
      );
    let balance = 0;
    transactions.forEach(transaction => {
      balance += transaction.amount;
    });
    return balance;
  },
  getTransactionsByDate: (from, to) => {
    const userId = get().user?.objectId;
    if (!userId) {
      return [];
    }
    const transactions = realm
      .objects('Transaction')
      .filtered(`date >= $0 AND date <= $1 AND userId = $2`, from, to, userId).sorted("date", true);
    return transactions;
  },
  getCloseAmount: () => {
    const userId = get().user?.objectId;
    const closeDate = new Date(get().date.getTime());
    closeDate.setHours(23, 59, 59, 999);

    if (!userId) {
      return 0;
    }
    const transactions = realm
      .objects('Transaction')
      .filtered(
        `date <= $0 AND cancelled = false AND userId = $1`,
        closeDate,
        userId,
      );
    let total = 0;
    transactions.forEach(transaction => {
      total += transaction.amount;
    });
    return total;
  },
  getOpenAmount: () => {
    const userId = get().user?.objectId;
    if (!userId) {
      return 0;
    }
    const date = get().date;
    const openDate = new Date(date.getTime());
    openDate.setHours(0, 0, 0, 0);

    const transactions = realm
      .objects('Transaction')
      .filtered(
        `date < $0 AND cancelled = false AND userId = $1`,
        openDate,
        userId,
      );
    let total = 0;
    transactions.forEach(transaction => {
      total += transaction.amount;
    });
    return total;
  },
  refresh: () => {
    const date = get().date;
    console.log('Refreshing...', new Date());

    const firstDate = new Date(
      date.getFullYear(),
      date.getMonth(),
      date.getDate() - 1,
      23,
      59,
      59,
      999,
    );
    const nextDate = new Date(
      date.getFullYear(),
      date.getMonth(),
      date.getDate(),
      23,
      59,
      60,
      0,
    );

    const tnxs = get().getTransactionsByDate(firstDate, nextDate);
    const cashIn = tnxs.filter(txn => txn.amount > 0);
    const cashOut = tnxs.filter(txn => txn.amount < 0);

    const tnxsPerAccount = get().getTxnByMonth();
    const givenPerAccount = tnxsPerAccount.filter(txn => txn.amount < 0);
    const receivedPerAccount = tnxsPerAccount.filter(txn => txn.amount > 0);

    const openBalancePerAccount = get().getOpeningBalance();
    const closeBalancePerAccount = get().getClosingBalance();
    let accounts = realm.objects('Account');
    const openAmount = get().getOpenAmount();
    const closeAmount = get().getCloseAmount();

    set({
      tnxs,
      cashIn,
      cashOut,
      accounts,
      openAmount,
      closeAmount,
      openBalancePerAccount,
      closeBalancePerAccount,
      tnxsPerAccount,
      givenPerAccount,
      receivedPerAccount,
    });
  },
  getTxnByMonth: () => {
    const userId = get().user?.objectId;
    const _id = get().accountId;
    const from = new Date(get().accountDate);
    from.setDate(1);
    from.setHours(0, 0, 0, 0);
    const to = new Date(from.getTime());
    to.setMonth(to.getMonth() + 1);
    to.setDate(0);
    to.setHours(23, 59, 59, 999);

    if (!userId) {
      return [];
    }

    const transactions = realm
      .objects('Transaction')
      .filtered(
        `accountId = $0 AND date >= $1 AND date <= $2 AND userId = $3`,
        _id,
        from,
        to,
        userId,
      ).sorted('date', false);
    return transactions;
  },
  setupRealmListener: () => {
    const debouncedRefresh = debounce(get().refresh, 250);
    const debouncedSync = debounce(syncToServer, 1000);

    realm.addListener('change', () => {
      debouncedRefresh()
      debouncedSync() // sync to server whenever there is a change!
    });
  },
  write(obj) {
    if (typeof obj === 'function') {
      const result = obj(get());
      set(result);
      return;
    }

    set(state => ({...state, ...obj}));
  },
}));
