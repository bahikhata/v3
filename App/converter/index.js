import {generateObjectId} from '../utils';

const convertToRealmDate = date => {
  if (date.includes('T')) return new Date(date);
  date = date.replace(' ', 'T') + '.000Z';
  return new Date(date);
};

export const Account = {
  convertToParse: ({
    objectId,
    name,
    balance,
    phone,
    cancelled,
    userId,
    createdAt,
    updatedAt,
    enabled,
  }) => {
    return {
      objectId,
      name,
      balance,
      phone,
      cancelled,
      userId: {
        __type: 'Pointer',
        className: '_User',
        objectId: userId,
      },
      createdAt: {
        __type: 'Date',
        iso: createdAt,
      },
      updatedAt: {
        __type: 'Date',
        iso: updatedAt,
      },
      enabled: !!enabled,
    };
  },

  convertToRealm: account => {
    return {
      objectId: account.objectId,
      name: account.name,
      balance: account.balance,
      phone: account.phone || '',
      cancelled: account.cancelled,
      userId: account.userId.objectId,
      createdAt: account.createdAt?.iso || account.createdAt,
      updatedAt: account.updatedAt?.iso || account.updatedAt,
      enabled: !!account.enabled,
    };
  },
};

export const Transaction = {
  convertToParse: ({
    objectId,
    date,
    accountId,
    attachment,
    amount,
    detail,
    type,
    userId,
    createdAt,
    updatedAt,
    enabled,
    cancelled
  }) => {
    return {
      objectId,
      date: {
        __type: 'Date',
        iso: date,
      },
      accountId: {
        __type: 'Pointer',
        className: 'Account',
        objectId: accountId,
      },
      attachment,
      cancelled,
      amount,
      detail,
      type,
      userId: {
        __type: 'Pointer',
        className: '_User',
        objectId: userId,
      },
      createdAt: {
        __type: 'Date',
        iso: createdAt,
      },
      updatedAt: {
        __type: 'Date',
        iso: updatedAt,
      },
      enabled: !!enabled,
    };
  },

  convertToRealm: transaction => {
    return {
      objectId: transaction.objectId,
      date: transaction.date?.iso || transaction.date,
      accountId: transaction?.accountId?.objectId || '',
      attachment: transaction.attachment || '',
      amount: transaction.amount,
      detail: transaction.detail,
      type: transaction.type || '',
      cancelled: !!transaction.cancelled,
      userId: transaction.userId.objectId,
      createdAt: transaction.createdAt?.iso || transaction.createdAt,
      updatedAt: transaction.updatedAt?.iso || transaction.updatedAt,
      enabled: !!transaction.enabled,
    };
  },
};

export function convertAllTransactionEntries(entries, accObjMap, userId) {
  return entries.map(entry => {
    const objectId = generateObjectId();

    return {
      objectId,
      date: convertToRealmDate(entry.createdAt),
      accountId: accObjMap[entry.accountId] || '',
      attachment: entry.media || '',
      amount: parseInt(entry.amount),
      detail: entry.detail,
      cancelled: !!entry.cancelled,
      userId: userId,
      createdAt: convertToRealmDate(entry.createdAt),
      updatedAt: convertToRealmDate(entry.updatedAt),
      type: '',
      synced: false,
    };
  });
}

export function convertAllAccountEntries(accounts, accObjMap, userId) {
  return accounts.map(account => {
    let objectId = accObjMap[account.id];

    if (!objectId) {
      objectId = generateObjectId();
      accObjMap[account.id] = objectId;
    }

    return {
      objectId: objectId,
      name: account.name,
      balance: parseInt(account.amount),
      phone: account.phone,
      cancelled: !!account.cancelled,
      userId: userId,
      createdAt: convertToRealmDate(account.createdAt),
      updatedAt: convertToRealmDate(account.updatedAt),
      enabled: true,
      synced: false,
    };
  });
}
