import React, { useState, useEffect } from 'react';
// Other imports remain the same
import {AppProvider, UserProvider, RealmProvider} from '@realm/react';
import {appId, baseUrl} from '../atlasConfig.json';
import {View, Text} from 'react-native';
import Index from './index';
import {
  TransactionSchema,
  AccountSchema,
} from 'services/realm.schema';
import { RealmInit } from './components/realm.init';

export const AppWrapper = () => {
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    // Simulate data loading
    setTimeout(() => {
      setIsLoaded(true);
    }, 1000);
  }, []);

  return (
    <AppProvider id={appId} baseUrl={baseUrl}>
      <UserProvider>
        <RealmProvider
          schema={[TransactionSchema, AccountSchema]}
          sync={{
            flexible: true,
            onError: (_session, error) => {
              console.error(error);
            },
          }}>
          {isLoaded ? (
            <Index />
          ) : (
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <Text>Loading...</Text>
            </View>
          )}
          <RealmInit />
        </RealmProvider>
      </UserProvider>
    </AppProvider>
  );
};

export default AppWrapper;
