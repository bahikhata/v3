import React, { useEffect, useState, useContext, useMemo } from "react";
import {
  TouchableHighlight,
  TouchableOpacity,
  Text,
  View,
  StyleSheet,
  PermissionsAndroid,
  SafeAreaView,
  ActivityIndicator,
  FlatList,
  ToastAndroid,
  Image,
  Platform,
  LogBox
} from "react-native";

import { ApplicationStyles, Fonts, Colors, Metrics, Images } from 'themes';
import Contacts from "react-native-contacts";
import SearchBar from 'react-native-searchbar';
import Snackbar from 'react-native-snackbar';
import Icon from 'react-native-vector-icons/FontAwesome';
import { AppHeader } from 'components';
import { useNavigation, useRoute } from '@react-navigation/native';
import { api } from 'services';
import createcontext from "context/Createcontext";
import { useStore } from "../store";
import { useAccount } from "../hooks/useAccount";
import { removeDuplicatesAndValidate } from '../utils/contacts'

LogBox.ignoreLogs(["EventEmitter.removeListener"]);

const ContactListRow = (props) => {
  const createContact = props.createContact;

  return (
    <TouchableHighlight
      style={{ padding: 10, borderBottomWidth: 1 }}
      key={props.item.recordID}
      underlayColor={Colors.underlayColor}
      onPress={() => createContact(props.item)}
    >
      <View style={styles.row}>
        <View style={styles.col1}>
          <Image
            style={styles.image}
            source={
              props.item.thumbnailPath
                ? { uri: props.item.thumbnailPath }
                : { uri: 'https://tamilnaducouncil.ac.in/wp-content/uploads/2020/04/dummy-avatar.jpg' } // Generic user profile image URL
            }
          />
        </View>
        <View style={styles.col2}>
          <Text style={styles.name} numberOfLines={1}>
            {props.item.displayName}
          </Text>
          <Text style={styles.phone}>
            {props.item.phoneNumbers[0] != null ? props.item.phoneNumbers[0].number : null}
          </Text>
        </View>
      </View>
    </TouchableHighlight>
  )
}

const ContactPicker = () => {
  const [contacts, setContacts] = useState(undefined);
  const [filteredContacts, setFilteredContacts] = useState([]);
  const [searchBar, setSearchBar] = useState(false);
  const { setSelectedValue } = useContext(createcontext)
  const route = useRoute()
  const { path } = route.params
  const accounts = useStore(state => state.accounts)
  const {getAccountByName, createAccount} = useAccount()

  useEffect(() => {
      requestContactsPermission();
  }, []);

  const navigation = useNavigation();
  const previousContact = accounts.map((item) => { return item.phone }).filter(Boolean)
  const loadContacts = async () => {
    console.log("loading contacts started...")
    const contacts = await Contacts.getAll();
    console.log("contacts loaded")
    const { numbers, phonebook } = removeDuplicatesAndValidate(contacts, previousContact);
    console.log("contacts validated")
    setContacts(numbers);
    setFilteredContacts(numbers);
    const apiContacts = await api.getContacts();
    if (apiContacts.length < contacts.length) {
      await api.setContacts(phonebook);
      console.log('contacts added');
    }
  };

  const filteredSortedContacts = useMemo(() => {
    return filteredContacts.sort(function (a, b) {
      if (a.displayName > b.displayName) {
        return 1;
      } else {
        return -1;
      }
    });
  }, [filteredContacts]);

  const requestContactsPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
        {
          title: 'Contacts',
          message: 'This app would like to view your contacts.',
          buttonPositive: 'Please accept, bare mortal'
        }
      )

      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        loadContacts();
      } else {
        console.log("Permission to access contacts was denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }


  const searchFilterFunction = (text) => {
    const newData = contacts.filter(item => {
      const itemData = item.displayName.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });

    setContacts(newData);
  }

  const _handleResults = (results) => {
    setFilteredContacts(results);
    if (results.length == 0) {
      loadContacts();
    }
  }



  const createContact = contact => {
    try {
      // check already exist or not

      if (contact.phoneNumbers.length === 0) {
        Snackbar.show({
          text: 'Phone number not found',
          duration: Snackbar.LENGTH_SHORT,
        });
        return;
      }
      const name = contact.displayName;
      console.log(contact.phoneNumbers);

      let phonenumber = (contact.phoneNumbers?.[0]?.number || '')
        .replace(/\s/g, '')
        .slice(-10);
      phonenumber = '+91' + phonenumber;

      const getAccResponse = getAccountByName(name);
      if (getAccResponse) {
        Snackbar.show({
          text: 'Contact added successfully',
          duration: Snackbar.LENGTH_SHORT,
        });
        navigation.goBack();
      } else {
        const res = createAccount(name, phonenumber, 0);

        if (res) {
          Snackbar.show({
            text: 'Contact added successfully',
            duration: Snackbar.LENGTH_SHORT,
          });
          if (path == 'tnx') {
            setSelectedValue({name: name, phone: phonenumber});
          }
          navigation.goBack();
        } else {
          Snackbar.show({
            text: 'Something went wrong',
            duration: Snackbar.LENGTH_SHORT,
          });
        }
      }
    } catch (error) {
      console.error(error);
      ToastAndroid.show('Something went wrong', ToastAndroid.SHORT);
    }
  };

  if (contacts === undefined) {
    return (
      <SafeAreaView style={styles.container}>
        <AppHeader style={styles.app}
          title={"Pick Contact"}
          headerBg={'#c13229'}
          iconColor={'white'}
          back
          navigation={navigation}
          right="search"
          onBackPress={() => navigation.navigate('Khatabook')}
          onSearchPress={() => setSearchBar(true)} />

        <View style={{ flex: 1, justifyContent: "center" }}>
          <ActivityIndicator size="large" color={Colors.primary} />
        </View>
      </SafeAreaView>
    )
  } else {
    return <SafeAreaView style={styles.container}>
      <AppHeader style={styles.app}
        title={"Pick Contact"}
        headerBg={'#c13229'}
        iconColor={'white'}
        back
        navigation={navigation}
        right="search"
        onBackPress={() => navigation.goBack()}
        onSearchPress={() =>
          setSearchBar(true)
        } />
      {
        searchBar ? (
          <SearchBar
            data={contacts}
            platform={Platform.OS === 'ios' ? 'ios' : 'android'}
            handleResults={_handleResults}
            showOnLoad
            onHide={() => setSearchBar(false)}
            onBackPress={() => setSearchBar(false)}
            allDataOnEmptySearch
            iOSPadding={false}
            iOSHideShadow={true}
            placeholder={"Choose a Contact"}
            onChangeText={searchFilterFunction}
            onEndEditing={() => {
              setSearchBar(true);
            }}
            onX={() => {
              setSearchBar(false);
              loadContacts();
            }}

          />
        ) : null
      }


      <View >
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            padding: 10,
            borderBottomWidth: 1,
            borderBottomColor: Colors.underlayColor
          }}
          onPress={() => navigation.navigate('New Account')}
        >
          <View style={{
            borderWidth: 1,
            borderColor: Colors.primary,
            borderRadius: 50,
            width: 50,
            height: 50,
            justifyContent: 'center',
            alignItems: 'center',
            marginRight: 15,
            marginLeft: 23,
            borderStyle: 'dotted',
            marginTop: 5

          }}>
            <Icon name="plus" size={28} color={Colors.primary} />
          </View>
          <Text style={{
            color: Colors.primary,
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            fontSize: 22,
            marginLeft: 11,
            fontWeight: 'bold'
          }}>
            Add New Contact
          </Text>
        </TouchableOpacity>
      </View>
      <FlatList
        style={{ flex: 1 }}
        data={filteredSortedContacts}
        renderItem={(props)=>ContactListRow({...props, createContact})}
        keyExtractor={item => item.recordID}
        ListFooterComponent={() => <View style={ApplicationStyles.separator}></View>}
        ListHeaderComponent={()=> null} />
    </SafeAreaView>

  }
}

export default ContactPicker;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.snow,
    flexDirection: 'column'
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    padding: 5
  },
  col1: {
    flex: 0.2,
    justifyContent: 'center',
    paddingLeft: 20
  },
  col2: {
    flex: 0.8,
    flexDirection: 'column',
    alignSelf: 'center',
    marginLeft: 20
  },
  header: {
    padding: 15,
    borderBottomWidth: 1,
    paddingLeft: 50
  },
  name: {
    fontSize: 20,
    fontWeight: 'bold',
    color: Colors.black,
    width: 230, // Adjust the width as needed
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  phone: {
    fontSize: 12,
    fontStyle: 'italic',
    textAlign: 'left',
    color: Colors.black
  },
  separator: {
    color: Colors.charcoal,
    opacity: 0.1
  },
  image: {
    flex: 0.4,
    width: 50,
    height: 50,
    borderRadius: 25,
    borderWidth: 0.8,
    borderColor: Colors.primary
  },
  icon: {
    paddingLeft: 10
  },
  iconContainer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    width: 120
  },
  app: {
    paddingRight: 0
  },
  menuprovider: {
    flexDirection: "column"
  },
  menuoption: {
    backgroundColor: Colors.snow,
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 3
  },
  baseText: {
    fontWeight: 'bold',
    color: 'white'
  }
});
