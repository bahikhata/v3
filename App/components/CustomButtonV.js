import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TouchableHighlight, Text, View, StyleSheet } from 'react-native';
import { Colors, Metrics, Fonts } from 'themes';


const CustomButtonV = ({ text, iconName, onPress }) => {
  return (
    <TouchableHighlight
      style={styles.menuItem}
      onPress={onPress}
      underlayColor={Colors.underlayColor}
    >
      <View style={{ alignItems: 'center' }}>
        <Icon
            name={iconName}
            size={Metrics.icons.medium}
            color={Colors.snow}
            style={styles.menuButton}
        />
        <Text style={styles.menuText}>{text && text.toUpperCase()}</Text>
      </View>
    </TouchableHighlight>
  );
}

export default CustomButtonV

const styles = StyleSheet.create({
  menuItem: {
    flex: 0.333,
    flexDirection: 'column',
    alignItems: 'center',
    paddingBottom: Metrics.marginHorizontal,
    paddingTop: Metrics.marginHorizontal
  },
  menuButton: {
    paddingBottom: Metrics.marginHorizontal
  },
  menuText: {
    color: Colors.snow,
    fontSize: Fonts.size.regular
  }
});
