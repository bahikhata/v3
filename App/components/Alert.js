import { Alert } from 'react-native';


export async function showAlert(title, message) {
    return new Promise((resolve, reject) => {
        Alert.alert(title, message, [
            {
                text: 'Cancel',
                onPress: () => resolve(false),
                style: 'cancel',
            },
            {
                text: 'OK', onPress: () => {
                    console.log('OK Pressed') 
                    resolve(true)
                }
            },
        ]);
    })
}