import React from 'react';
import { StyleSheet, Text, TouchableHighlight, View } from 'react-native';
import PropTypes from 'prop-types';
import { Fonts, Colors, Images } from 'themes';
import Icon from 'react-native-vector-icons/FontAwesome'

const PRIMARY_COLOR = 'rgb(0,98,255)';
const WHITE = '#ffffff';
const BORDER_COLOR = '#DBDBDB';

const ActionSheet = (props) => {

  const { actionItems,dissmiss,filterClick,nameasc,amountasc } = props;
  const actionSheetItems = [
    ...actionItems,
    {
      id: '#cancel',
      label: 'Cancel',
      onPress: props?.onCancel
    }
  ]

   const onActionPress = (item) => {
    if (item.onPress) {
      item.onPress();
      dissmiss()
    }
  }


  return (
    <View style={styles.modalContent}>
      {
        actionSheetItems.map((actionItem, index) => {
          return (
            <TouchableHighlight
              style={[
                styles.actionSheetView,
                index === 0 && {
                  borderTopLeftRadius: 12,
                  borderTopRightRadius: 12,
                },
                index === actionSheetItems.length - 2 && {
                  borderBottomLeftRadius: 12,
                  borderBottomRightRadius: 12,
                },
                index === actionSheetItems.length - 1 && {
                  borderBottomWidth: 0,
                  backgroundColor: WHITE,
                  marginTop: 8,
                  borderTopLeftRadius: 12,
                  borderTopRightRadius: 12,
                  borderBottomLeftRadius: 12,
                  borderBottomRightRadius: 12,
                }]}
              underlayColor={'#f7f7f7'}
              key={index} onPress={() => onActionPress(actionItem)}
            >
              {
                filterClick?<>
               <Text allowFontScaling={false}
                style={[
                  styles.actionSheetText,
                  actionItem.label === "Delete" ? { color: '#fa1616' } : { color: PRIMARY_COLOR},
                  props?.actionTextColor && {
                    color: props?.actionTextColor
                  },
                  index === actionSheetItems.length - 1 && {
                    color: '#fa1616',
                  }
                ]}>

                {
                nameasc&&actionItem.label=='Name'?<>
                {actionItem.label}
                <Icon name="sort-alpha-asc" size={20} color={PRIMARY_COLOR}  />
                </>:
                 !nameasc&&actionItem.label=='Name'?<>
                 {actionItem.label}
                 <Icon name="sort-alpha-desc" size={20} color={PRIMARY_COLOR} />
                 </>:
                amountasc&&actionItem.label=='Amount'?<>
                {actionItem.label}
                <Icon name="sort-numeric-asc" size={20} color={PRIMARY_COLOR} />
                </>: !amountasc&&actionItem.label=='Amount'?<>
                {actionItem.label}
                <Icon name="sort-numeric-desc" size={20} color={PRIMARY_COLOR} />
                </>:
                <>{actionItem.label}</>

                }
              </Text>
                </>:
                <>

                {/* <Text allowFontScaling={false}
                style={[
                  styles.actionSheetText,
                  actionItem.label === "Delete"  ? { color: '#fa1616' } : { color: PRIMARY_COLOR},
                  props?.actionTextColor && {
                    color: props?.actionTextColor
                  },
                  index === actionSheetItems.length - 1 && {
                    color: '#fa1616',
                  }
                ]}>

                {actionItem.label}
              </Text> */}

              
              {actionItem.label === "Delete" ?
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Icon name="trash" size={20} color="#fa1616" />
                  <Text allowFontScaling={false} style={{ fontSize: 18, color: '#fa1616', marginLeft: 10 }}>{actionItem.label}</Text> 
                </View>
                :
                actionItem.label==='Edit'?
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Icon name="edit" size={20} color={PRIMARY_COLOR} />
                  <Text allowFontScaling={false} style={{ fontSize: 18, color: PRIMARY_COLOR, marginLeft: 10 }}>{actionItem.label}</Text>
                </View>
                :
                actionItem.label==='Edit Account'?
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Icon name="edit" size={20} color={PRIMARY_COLOR} />
                  <Text allowFontScaling={false} style={{ fontSize: 18, color: PRIMARY_COLOR, marginLeft: 10 }}>{actionItem.label}</Text>
                </View>
                :
                actionItem.label==='Close Account'?
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Icon name="close" size={20} color="#fa1616" />
                  <Text allowFontScaling={false} style={{ fontSize: 18, color: '#fa1616', marginLeft: 10 }}>{actionItem.label}</Text>
                </View>
                :

                <Text allowFontScaling={false}
                  style={[
                    styles.actionSheetText,
                    actionItem.label === "Delete" ? { color: '#fa1616' } : { color: PRIMARY_COLOR},
                    props?.actionTextColor && {
                      color: props?.actionTextColor
                    },
                    index === actionSheetItems.length - 1 && {
                      color: '#fa1616',
                    }
                  ]}>
                  {actionItem.label}
                </Text>
              }

                </>
              }
            </TouchableHighlight>
          )
        })
      }
    </View>
  )
}

const styles = StyleSheet.create({
  modalContent: {
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    borderBottomLeftRadius: 12,
    borderBottomRightRadius: 12,
    marginLeft: 8,
    marginRight: 8,
    marginBottom: 20,
  },
  actionSheetText: {
    fontSize: 18,
  },
  actionSheetView: {
    backgroundColor: WHITE,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 16,
    paddingBottom: 16,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: BORDER_COLOR
  }
});

ActionSheet.propTypes = {
  actionItems: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      label: PropTypes.string,
      onPress: PropTypes.func
    })
  ).isRequired,
  onCancel: PropTypes.func,
  actionTextColor: PropTypes.string
}


ActionSheet.defaultProps = {
  actionItems: [],
  onCancel: () => { 
    console.log('cancel')
  },
  actionTextColor: null
}


export default ActionSheet;