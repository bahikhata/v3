import Modal from 'react-native-modal';
import { View, Text, TouchableOpacity } from 'react-native';
import { Colors, Metrics, Images, Fonts, ApplicationStyles } from 'themes';
import { Button } from "react-native-paper";
import React from 'react';

const CustomAlert = ({ visible, onClose, onConfirm, Title }) => {
  return (
    <Modal isVisible={visible}>
      <View style={{ backgroundColor: 'white', width: "90%", marginLeft: 15, padding: 20, borderRadius: 10 }}>
        <Text style={{ color: Colors.black, fontSize: 22, padding: 10, fontWeight: 'bold' }}>
          {Title ? Title : "Alert"}
        </Text>
        <Text style={{ fontSize: 18, padding: 10 }}>
          Are you sure ?
        </Text>

        <View style={{ width: "90%", marginLeft: 10, flexDirection: 'row', justifyContent: "flex-end", marginTop: 20 }}>
          <TouchableOpacity onPress={onClose} style={{ fontSize:20 }}>
            <Text style={{ fontSize: 16, color: Colors.cole, fontWeight: 'bold', paddingRight: 10, paddingLeft: 10 }}>CANCEL</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={onConfirm} style={{  fontSize:20}}>
            <Text style={{ fontSize: 16, color: Colors.cole, fontWeight: 'bold', paddingRight: 10, paddingLeft: 10 }}>OK</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default CustomAlert
