import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableHighlight, Text, View, StyleSheet, Button } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Fonts, Colors, Metrics } from 'themes';


const IconButton = ({ text, iconName, onPress, disabled, btn }) => {
  return (
    <TouchableHighlight onPress={!disabled ? onPress : null} underlayColor={Colors.windowTint} >
      <View style={{
        ...(disabled ? styles.optionItemDisabled : styles.optionItem),
        backgroundColor: disabled
          ? Colors.windowTint
          : btn === "Add"
            ? Colors.darkGreen
            : btn === "Remove"
              ? Colors.primary
              : Colors.windowTint,
      }}>
        <Icon
          name={iconName}
          size={Metrics.icons.small}
          color={Colors.snow}
        />
        <Text style={styles.optionText}>{text && text.toUpperCase()}</Text>
      </View>
    </TouchableHighlight>
  )
}

IconButton.propTypes = {
  text: PropTypes.string,
  iconName: PropTypes.string,
  onPress: PropTypes.func,
  disabled: PropTypes.bool
};

IconButton.defaultProps = {
  disabled: false
};

export default IconButton;

const styles = StyleSheet.create({
  optionItem: {
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    paddingTop: 12,
    paddingBottom: 12,
    borderWidth: 0.2,
    borderColor: Colors.hairline,
    borderRadius: 3,
    paddingLeft: Metrics.doubleBaseMargin * 2,
    shadowColor: Colors.windowTint,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },
  optionItemDisabled: {
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    paddingTop: 12,
    paddingBottom: 12,
    borderWidth: 0.2,
    borderColor: Colors.hairline,
    borderRadius: 3,
    paddingLeft: Metrics.doubleBaseMargin * 2
  },
  optionText: {
    color: Colors.snow,
    fontSize: Fonts.size.medium,
    marginLeft: Metrics.baseMargin,
    alignItems: 'center',
    textAlignVertical: 'center'
  }
});
