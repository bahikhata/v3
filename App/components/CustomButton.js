import React, { Component } from 'react';
import { TouchableHighlight, Text, View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Fonts, Colors, Metrics } from 'themes';

const CustomButton = ({ text, iconName, onPress, subtitle, trailElement }) => {
  return (
    <TouchableHighlight onPress={onPress} style={styles.optionItem} underlayColor={Colors.whitish}>
      <View style={{
        flexDirection: 'row',
        display: 'flex',
        alignItems: 'center',
      }}>
        {typeof iconName === "string" ? <Icon
          name={iconName}
          size={Metrics.icons.small}
          color={Colors.hairline}
        /> : iconName}
        <View
          style={{
            flexDirection: 'column',
            marginLeft: 10
          }}
        >
          <Text style={styles.optionText}>{text && text.toUpperCase()}</Text>
          <Text style={styles.subtitle}>{subtitle && subtitle}</Text>
        </View>
        {trailElement ? (<View style={styles.trailElement}>
          {trailElement}
        </View>)
          : null}

      </View>
    </TouchableHighlight>
  );
}

export default CustomButton;

const styles = StyleSheet.create({
  optionItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 20,
    marginTop: 8,
  },
  optionText: {
    color: Colors.black,
    fontSize: Fonts.size.regular,
    marginLeft: 35

  },
  subtitle: {
    color: Colors.hearline,
    fontSize: Fonts.size.small,
    marginLeft: 35,
    padding: 1
  },
  trailElement: {
    flexGrow: 1,
    marginRight: 35
  }
});
