
import React, { useMemo } from 'react';
import { View, Text, StyleSheet, Platform } from 'react-native';
import accounting from 'accounting';
import { Colors, Metrics, Images, ApplicationStyles, Fonts } from 'themes';

export default OpenBalance = ({ OpenBalance, give }) => {
    const amountText = useMemo(() => {
        return accounting.formatMoney(Math.abs(OpenBalance), '₹', 0);
    }, [OpenBalance]);

    return (
        <View>
            <View style={{ ...styles.group }}>
                <Text style={styles.amountText}>
                    {amountText}
                </Text>
                <Text style={{...styles.detailText,
                    marginLeft: OpenBalance&&OpenBalance.toString().length>2?8:20,
                }}>OpenBalance</Text>
            </View>
            {
                give.length > 0 ? <View style={{ ...ApplicationStyles.separator }} /> : null
            }
        </View>
    );
}

const styles = StyleSheet.create({
    group: {
        flexDirection: 'row',
        marginTop: Metrics.baseMargin / 2,
        marginBottom: Metrics.baseMargin / 2,  
    //     justifyContent:'center',
    //   alignItems: 'center' 
    },
    amountText: {
        fontFamily: 'Hind-Regular',
        color: Colors.black,
        fontSize: Fonts.size.small,
        fontStyle: 'italic',
        textAlignVertical: 'center',
        marginLeft: Metrics.baseMargin
    },
    detailText: {
        color: Colors.hairline,
        fontFamily: 'Hind-Regular',
        fontSize: Fonts.size.medium,
        textAlignVertical: 'center',
        fontWeight: 'bold',
    }  
})

