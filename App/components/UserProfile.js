import { Linking, ScrollView, StyleSheet, Text, View, Image, Touch, TouchableOpacity, ToastAndroid } from 'react-native'
import React, { useContext, useEffect, useState } from 'react'
import Icon1 from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import Icon3 from 'react-native-vector-icons/FontAwesome';
import Icon4 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon5 from 'react-native-vector-icons/Ionicons';
import { useNavigation, useRoute } from '@react-navigation/native'
import { Colors, Metrics, Fonts, ApplicationStyles } from 'themes'
import { Button } from "react-native-paper";
import { CustomAlert } from 'components'
import { api } from 'services'
import AppHeader from './AppHeader'
import createcontext from '../context/Createcontext';
import { useAccount } from "../hooks/useAccount"

const UserProfile = () => {
    const { getAccountById, getReminderById,closeAccount, setAutoSms } = useAccount();
    const route = useRoute().params
    console.log("Route => ", route)
    const [reminderStatus, setReminderStatus] = useState(() => {
        const response = getReminderById(route.userId)
        return response.enabled
    });
    const [visibleAlart, setVisibleAlart] = useState(false);

    const data = getAccountById(route.userId)
    const [toggle, setToggle] = useState(data.checkboxSMS)

    const {reminderRefresh, editRefresh, setRefreshData, refreshData } = useContext(createcontext)
    const navigation = useNavigation()

    const handleDelte = () => {
        try {
            const response = closeAccount(route.userId)
            if (response) {
                setRefreshData(!refreshData)
                navigation.navigate('Khatabook')
            }
        } catch (err) {
            console.error(err);
        }
    }


    const Box = ({ title, Icon, onpress }) => {
        return (
            <TouchableOpacity style={styles.box}
                onPress={onpress}
            >
                {Icon == "phone" && <Icon3 name={Icon} size={25} color={Colors.black} />}
                {Icon == "whatsapp" && <Icon3 name={Icon} size={25} color={Colors.black} />}
                {Icon == "alarm-sharp" && <Icon5 name={Icon} size={25} color={
                    reminderStatus ? Colors.darkGreen : Colors.black
                }  />}
                <Text style={{ fontSize: 15, fontWeight: 'bold', textAlign: 'center', top: 2 }}>{title}</Text>
            </TouchableOpacity>
        )
    }

    const Box1 = ({ title, value }) => {
        return (
            <View style={styles.box}>
                <Text style={{
                    fontSize: 15, fontWeight: 'bold', textAlign: 'center', top: 2,
                    color: value < 0 ? Colors.primary : Colors.darkGreen
                }}>{value?"₹" + Math.abs(value):0}</Text>
                <Text style={{ fontSize: 15, textAlign: 'center', top: 2 }}>{title}</Text>
            </View>
        )
    }

    const hanelePhone = (phone) => {
        Linking.openURL(`tel:${phone}`);
    }

    const handleToggle = () => {
        if(data.phone){
            setAutoSms(route.userId)
            setToggle(!toggle)
        } else {
            ToastAndroid.show('Phone number is not available', ToastAndroid.SHORT)
        }
    }


    const handleCancel = () => {
        setVisibleAlart(false)
    }

    const handleClose = () => {
        if (data.balance > 0) {
            ToastAndroid.show('Balance must be 0', ToastAndroid.SHORT)
            return
        }
        setVisibleAlart(true)
    }

    const handleReminder = () => {
        navigation.navigate('Set Reminder', { id: route.userId });

    }

    const handleEditAcc = () => {
        navigation.navigate('Edit Account', { id: route.userId })
    }

    return (
        <View>
            <CustomAlert visible={visibleAlart} Title="Close Account" onClose={handleCancel} onConfirm={handleDelte} />
            <AppHeader
                iconColor={'#000'}
                title={"Account Details"}
                back={true}
                onBackPress={() => navigation.goBack()}
            />
            <View>

                <TouchableOpacity style={styles.editIcon} onPress={handleEditAcc}>
                    <Icon2 name="user-edit" size={25} color={Colors.black} />
                </TouchableOpacity>

                <TouchableOpacity style={styles.camaraIcon} onPress={() => navigation.goBack()}>
                    <Icon1 name="camera" size={25} color={Colors.black} />
                </TouchableOpacity>
                <Image
                    source={{ uri: 'https://tamilnaducouncil.ac.in/wp-content/uploads/2020/04/dummy-avatar.jpg' }}
                    style={styles.profileImage}
                />
            </View>
            <View style={{ marginTop: 20 }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold', color: Colors.charcoal, textAlign: 'center' }}>{data ? data.name : null}</Text>
                <Text style={{ fontSize: 12, textAlign: 'center' }}>{data ? data.phone : null}</Text>
            </View>

            <View style={{ marginTop: 27 }}>
                {/* three box wiht bacground shodow in horizantla  */}
                < View style={{ flexDirection: 'row', justifyContent: 'space-around', marginHorizontal: 20 }}>
                    <Box onpress={() => hanelePhone(data.phone)} title="Phone" Icon={"phone"} />
                    <Box title="whatsapp" Icon={"whatsapp"} onpress={route?.handleOpenWhatsApp} />
                    <Box title="Reminder" Icon={"alarm-sharp"} onpress={handleReminder} />
                </View>
            </View>

            <View style={{ marginTop: 20 }}>
                < View style={{ flexDirection: 'row', justifyContent: 'space-around', marginHorizontal: 20 }}>
                    <Box1 title="Balance" value={data.balance} />

                </View>
            </View>

            <View style={{ ...ApplicationStyles.separator, marginTop: 15, padding: 1 }} />

            <View style={
                {
                    display: 'flex',
                    flexDirection: "column",
                    backgroundColor: Colors.snow,
                    height: 300,
                    justifyContent: 'space-between',
                }
            }>
                <View style={styles.bottomView}>
                    <Icon2 name="bell" size={25} color={Colors.hairline} />
                    <View
                        style={styles.notification} >
                        <Text style={styles.notificationText}>Notifications</Text>
                        {
                            toggle ? <Icon2 name="toggle-on" style={styles.bellicon} onPress={handleToggle} /> : <Icon2 style={styles.bellicon} name="toggle-off" onPress={handleToggle} />
                        }
                    </View>
                </View>
                 <View style={{ ...ApplicationStyles.buttonWrapper, bottom: 0 }}>
                        <Button
                            onPress={data.balance==0?handleClose:null}
                            buttonColor={Colors.darkPrimary}
                            rippleColor={Colors.primary}
                            uppercase
                            dark
                            compact
                            mode="contained"
                            // disabled={data.balance==0?true:false}
                            style={{
                                padding: 7, marginTop: 30,
                                backgroundColor: data.balance==0 ? Colors.darkPrimary : Colors.hairline,
                                
                            }}>
                            <Text style={{ color: Colors.snow }}>
                                Close Account
                            </Text>
                        </Button>
                    </View>

                {/* <View style={styles.media}>
                    <Icon2 name="file" size={25} color={Colors.hairline} />
                    <Text style={styles.editText}>Media content</Text>
                </View> */}


            </View>

        </View>
    )
}

export default UserProfile

const styles = StyleSheet.create({
    profileImage: {
        width: 120,
        height: 120,
        borderRadius: 60,
        alignSelf: 'center',

    },
    box: {
        width: 100,
        height: 80,
        backgroundColor: Colors.snow,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: Colors.black,
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

    },
    bellicon: {
        marginLeft: 'auto',
        marginRight: 20,
        color: Colors.hairline,
        fontSize: 25
    }
    ,
    notificationText: {
        fontSize: 20,
        textAlign: 'left',
        marginLeft: 10,
        color: Colors.black
    },
    editText: {
        fontSize: 20,
        textAlign: 'left',
        marginLeft: 20,
        color: Colors.black
    },
    editPhone: {
        fontSize: 20,
        textAlign: 'left',
        marginLeft: 20,
        color: Colors.black
    },
    bottomView: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingLeft: 10,
        borderBottomWidth: 0.57,

        borderBottomColor: Colors.hairline,
    },
    camaraIcon: {
        position: 'absolute',
        top: 70,
        left: 210,
        zIndex: 1,
        backgroundColor: Colors.lightPrimary,
        padding: 9,
        borderRadius: 50
    },

    notification: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        padding: 15,

    }
    ,
    buttomSubView: {
        dlseplay: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        padding: 15,
        borderBottomWidth: 0.57,
        borderBottomColor: Colors.hairline,
    },
    media: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-start",
        padding: 15,
        borderBottomWidth: 0.57,
        borderBottomColor: Colors.hairline,
    },
    editIcon: {
        position: 'absolute',
        right: 20,
        top: -33,
    }
})