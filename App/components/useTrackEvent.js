import {useCallback} from 'react';
import {mixpanel} from 'services';

export default function useTrackEvent(eventName) {
  return useCallback(
    properties => {
      try {
        mixpanel.track(eventName, properties);
      } catch (e) {
        console.error(e);
      }
    },
    [eventName],
  );
}