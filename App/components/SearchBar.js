import React, { Component ,useRef,useState} from "react";
import PropTypes from "prop-types";
import {
  View,
  TextInput,
  UIManager,
  LayoutAnimation,
  Animated,
  ActivityIndicator,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Text,
  StyleSheet
} from "react-native";

const SearchBar = (props) => {
  const inputRef = useRef(null);

  const [hasFocus, setHasFocus] = useState(false);
  const [isEmpty, setIsEmpty] = useState(true);
  const [showLoader, setShowLoader] = useState(false);
  const [query, setQuery] = useState('');

  const handleSearch = () => {
    // Call the callback function with the current search query
    onSearch(query);
  };

  const {
    searchPlaceholder,
    onClear,
    onFocus: propOnFocus,
    onBlur: propOnBlur,
    onChangeText: propOnChangeText,
    style
  } = props;

  const focus = () => {
    inputRef.current.focus();
  };

  const blur = () => {
    inputRef.current.blur();
  };

  const clear = () => {
    inputRef.current.clear();
    propOnChangeText("");
    onClear();
  };

  const cancel = () => {
    blur();
  };

  // const showLoader = () => {
  //   setShowLoader(true);
  // };

  // const hideLoader = () => {
  //   setShowLoader(false);
  // };

  const onFocus = () => {
    propOnFocus();
    if (UIManager.configureNextLayoutAnimation) LayoutAnimation.easeInEaseOut();
    setHasFocus(true);
  };

  const onBlur = () => {
    propOnBlur();
    if (UIManager.configureNextLayoutAnimation) LayoutAnimation.easeInEaseOut();
    setHasFocus(false);
  };

  const onChangeText = (text) => {
    propOnChangeText(text);
    setIsEmpty(text === "");
  };

  const {
    container,
    inputStyle,
    leftIconStyle,
    rightContainer,
    rightIconStyle,
    activityIndicator
  } = styles;

  const inputStyleCollection = [inputStyle];

  if (hasFocus) inputStyleCollection.push({ flex: 1 });

  return (
    <TouchableWithoutFeedback onPress={focus} style={style}>
      <Animated.View style={container}>
        <View style={leftIconStyle}>
          <Text>🔍</Text>
        </View>
        <TextInput
          onFocus={onFocus}
          onBlur={onBlur}
          onChangeText={onChangeText}
          placeholder={searchPlaceholder}
          style={inputStyleCollection}
          placeholderTextColor="#515151"
          autoCorrect={false}
          ref={inputRef}
        />
        <View style={rightContainer}>
          {hasFocus && showLoader ? (
            <ActivityIndicator
              key="loading"
              style={activityIndicator}
              color="#515151"
            />
          ) : (
            <View />
          )}
          {hasFocus && !isEmpty ? (
            <TouchableOpacity onPress={clear}>
              <View style={rightIconStyle}>
                <Text
                  style={{ fontSize: 20,fontFamily: "Roboto",color:"#515151",marginRight:6}}
                >ⅹ</Text>
              </View>
            </TouchableOpacity>
          ) : (
            <View />
          )}
        </View>
      </Animated.View>
    </TouchableWithoutFeedback>
  );
};

SearchBar.propTypes = {
  searchPlaceholder: PropTypes.string,
  onClear: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onChangeText: PropTypes.func,
  style: PropTypes.object
};

SearchBar.defaultProps = {
  searchPlaceholder: "Search",
  onClear: () => null,
  onFocus: () => null,
  onBlur: () => null,
  onChangeText: () => null
};


const styles = StyleSheet.create({
  container: {
    height: 40,
    borderRadius: 5,
    backgroundColor: "#ddd",
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5,
    marginTop: 5,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  inputStyle: {
    alignSelf: "center",
    marginLeft: 5,
    height: 40,
    fontSize: 14
  },
  leftIconStyle: {
    height: 30,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 8
  },
  rightContainer: {
    flexDirection: "row"
  },
  rightIconStyle: {
    height: 30,
    justifyContent: "center",
    alignItems: "center",
    marginRight: 8
  },
  activityIndicator: {
    marginRight: 5
  }
});

export default SearchBar;
