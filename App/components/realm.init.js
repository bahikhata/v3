import { useEffect } from 'react';
import { useStore } from 'store';
import { syncToServer } from '../sync';
import { useNetInfo } from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { onFirstLaunchApp, realm } from '../services/realm.schema';
import { CASHBOOK_KEY } from '../views/SplashScreen';

const SYNC_INTERVAL = 60000;

export function RealmInit() {
  const write = useStore(state => state.write);
  const setupRealmListener = useStore(state => state.setupRealmListener);
  const date = useStore(state => state.date);
  const accountDate = useStore(state => state.accountDate);
  const accountId = useStore(state => state.accountId);
  const user = useStore(state => state.user);
  const cashbookEnabled = useStore(state => state.cashbookEnabled);

  const refreshData = useStore(state => state.refresh);
  const { isConnected } = useNetInfo();

  useEffect(() => {
    AsyncStorage.getItem('user')
      .then(user => {
        console.log('User from async storage', user);
        write({ user: JSON.parse(user) });
      })
      .catch(err => console.log(err));
  }, [write]);

  useEffect(() => {
    onFirstLaunchApp(realm).then(() => {
      console.log('Realm initialized');
    });
  }, []);

  useEffect(() => {
    if (cashbookEnabled) {
      AsyncStorage.setItem(CASHBOOK_KEY, "1")
    } else {
      AsyncStorage.removeItem(CASHBOOK_KEY)
    }
  }, [cashbookEnabled])

  useEffect(() => {
    refreshData();
  }, [refreshData, user, date, accountDate, accountId]);

  useEffect(() => {
    setupRealmListener();
  }, [setupRealmListener]);

  useEffect(() => {
    let timeout = null;
    let cleanup = false;

    const handler = async () => {
      if (isConnected) {
        syncToServer().then(() => {
          if (cleanup) return;
          timeout = setTimeout(handler, SYNC_INTERVAL);
        });
      } else {
        console.log('No internet connection');
      }
    };
    handler();
    return () => {
      cleanup = true;
      clearTimeout(timeout);
    };
  }, [isConnected, syncToServer]);

  return null;
}
