import React, { useContext } from 'react'
import { useNavigation, useRoute } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome'
import Svg, { Path } from 'react-native-svg';
import { View, StyleSheet, TouchableOpacity, Text, TouchableNativeFeedback } from "react-native";
import { Subheading, Surface, Title } from 'react-native-paper'
import { Colors } from 'themes';
import Feather from 'react-native-vector-icons/Feather'
import Ripple from 'react-native-material-ripple';

import createcontext from '../context/Createcontext';
import { Linking } from 'react-native';
import { useStore } from '../store';
const IconSize = 24;

const AppHeader = ({ style, menu, back, title, right,
	onSearchPress, onMenuPress, onBackPress, businessName, rightComponent, headerBg, iconColor, phone, handleOpenWhatsApp, userId, openBtn,
	titleAlight, filterbtn, closebtn }) => {
	const { hideCloseAccount } = useContext(createcontext)
	if (!title) {
		title = "Welcome to Bahi Khata"
	}

	const cancelAccountLength = useStore(state => state.accounts.filter(account => account.cancelled).length)
	const route = useRoute()
	const pathname = route.name
	const nevigation = useNavigation()

	const LeftView = () => (
		<View style={styles.view}>
			{back && <TouchableOpacity onPress={onBackPress}>
				<Feather name="arrow-left" size={IconSize} color={
					pathname === "UserProfile" ? "black" : "white"
				} />
			</TouchableOpacity>}
			{menu && <TouchableOpacity onPress={() => { onMenuPress }}>
				<Feather name="menu" size={IconSize} color={Colors.snow} />
			</TouchableOpacity>}
		</View>
	)
	const RightView = () => (
		rightComponent ? rightComponent :
			<View style={[styles.view, styles.rightView]}>
				{right && <TouchableOpacity style={{ paddingRight: 20 }} onPress={onSearchPress}>
					<Feather name={right} size={IconSize} color={Colors.snow} />
				</TouchableOpacity>}

				{
					filterbtn && <TouchableOpacity style={styles.rowView} onPress={filterbtn}>
						<Feather name="filter" size={IconSize} color={Colors.snow}

						/>
					</TouchableOpacity>
				}
				{
					closebtn && cancelAccountLength > 0 && <TouchableOpacity style={styles.rowView} onPress={closebtn}>
						{/* <Feather name="filter-circle" size={IconSize} color={Colors.snow} /> */}
						<View style={{ marginLeft: 12 }}><Svg width="24" height="24" viewBox="0 0 16 16" fill={hideCloseAccount ? "black" : 'white'}>
							<Path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zM3.5 5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1 0-1zM5 8.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5m2 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5" />
						</Svg>
						</View>
					</TouchableOpacity>
				}
				{/* {downloadBtn && <TouchableOpacity style={{...styles.rowView,paddingRight:10 }} onPress={
					() => {
						setDownloadOption(!downloadOption)
					}
				}>
					
					<Download name="file-download" size={IconSize} color={Colors.snow}  />
					</TouchableOpacity>} */}
				{pathname == "Single-Khata" && <TouchableOpacity style={{ ...styles.rowView, paddingRight: 8 }} >
					<Icon name="whatsapp" size={IconSize} color={Colors.snow}
						onPress={() => {
							handleOpenWhatsApp()
						}
						}
					/>
				</TouchableOpacity>
				}
				{
					pathname == "Single-Khata" && <TouchableOpacity style={styles.rowView} >
						<Feather name="phone" size={IconSize} color={Colors.snow}
							onPress={() => {
								Linking.openURL(`tel:${phone}`);
							}
							}
						/>
					</TouchableOpacity>
				}
				{/* {optionalBtn && <TouchableOpacity style={styles.rowView} onPress={
					() => {
						setOpenMore(!openMore)
					}
				}>
					<Feather name={optionalBtn} size={IconSize} color={Colors.snow} />
				</TouchableOpacity>} */}

			</View>
	)
	const TitleView = () => {
		return (
			<View style={styles.titleView} >
				<Text
					style={{
						color: iconColor,
						textAlign: titleAlight,
						padding: 0,
						margin: 0,
						marginLeft: title === 'Welcome to Bahi Khata' ? -20 : 0,
						fontSize: 22,
						fontWeight: "700"
					}}
					ellipsizeMode="tail"
					numberOfLines={1}
				>
					{title}
				</Text>
				{pathname == 'Single-Khata'
					? <Text style={{
						color: iconColor,
						textAlign: titleAlight,
						padding: 0,
						marginBottom: 1,
					}}>View profile</Text> : null}
			</View>
		);
	};

	return (
		<Surface style={[styles.header, style, { backgroundColor: headerBg }]}>
			<LeftView />
			{pathname === "Single-Khata" ? (
				<Ripple onPress={() =>
					pathname == 'Single-Khata'
						? nevigation.navigate('UserProfile', {
							userId,
							title,
							businessName,
							closebtn,
							openBtn,
							handleOpenWhatsApp,
						})
						: null}
					style={{
						width: '100%',
						display: 'flex',
						justifyContent: 'flex-start'
					}}>
					<TitleView />
				</Ripple>) : <TitleView />}
			<RightView />
		</Surface>

	)
}

export default AppHeader;

const styles = StyleSheet.create({
	header: {
		height: 50,
		elevation: 8,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		backgroundColor: Colors.black,
	},
	view: {
		marginHorizontal: 16,
		alignItems: 'center',
		flexDirection: 'row',
	},
	titleView: {
		flex: 1,
	},
	rightView: {
		justifyContent: 'flex-end',
	},
	rowView: {
		flexDirection: 'row',
		alignItems: 'center',
		marginRight: 10,
	}
})
