import React, { useState, useRef, useContext, useEffect } from 'react';
import { Text, View, StyleSheet, Platform, TextInput, TouchableOpacity, ScrollView ,FlatList} from 'react-native';
import { Colors, Metrics, ApplicationStyles, Fonts } from 'themes';
import createcontext from '../context/Createcontext';

const AutoComplete = (props) => {
  const { account,setCheckBox } = props;
  const { selectedValue, setSelectedValue } = useContext(createcontext);
  const [filteredData, setFilteredData] = useState([]);


  const renderItem = (item) => {
    return (
      <TouchableOpacity onPress={() => {
        setSelectedValue({ name: item.name, phone: item.phone });
        setFilteredData([]);
        setCheckBox(item.checkboxSMS)
      }}>
        <View style={{ padding: 10}}>
          <Text style={{ color: 'black', fontSize: 16, paddingTop: 7, paddingBottom: 7 }}>{item.name}</Text>
        </View>
      </TouchableOpacity>
    );
  };
  console.log(account)
  const handleChange = (text) => {
    const newData = text ? account.filter(item => {
      const itemData = item.name.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    }) : [];
    setFilteredData(newData);
    setSelectedValue({ name: text, phone: '' });
  };

  return (
    <View style={[styles.container]}>
      <View>
        <TextInput
          style={ApplicationStyles.inputControl}
          onChangeText={handleChange}
          value={selectedValue.name}
          placeholder={'Account'}
          focusable={true}
        />
      </View>
      {filteredData.length > 0 && (
        <ScrollView style={{
          ...styles.list,
          maxHeight: 211,
          overflow:"scroll",
          zIndex: 1,
          position:'absolute',
          top: 60,
          left: 0,
          width: '124%',
          backgroundColor: 'white',
          
        }}>
          {filteredData.map((item, index) => (
            <View key={index}>
              {renderItem(item)}
              {index !== filteredData.length - 1 && <View style={{ height: 1, backgroundColor: '#b9b9b9' }} />}
            </View>
          ))}
        </ScrollView>
      )}

    </View>
  );
};


const border = {
  borderColor: '#b9b9b9',
  borderRadius: 1,
  borderWidth: 1
};

const androidStyles = {
  container: {
    flex: 1
  },
  inputContainer: {
    marginBottom: 0
  },
  list: {
    ...border,
    backgroundColor: Colors.whitish,
    borderTopWidth: 0,
    margin: 10,
    marginTop: 0
  }
};

const iosStyles = {
  container: {
    zIndex: 1
  },
  inputContainer: {
    ...border
  },
  input: {
    backgroundColor: Colors.whitish,
    height: 40,
    paddingLeft: 3,
    color: 'black'
  },
  list: {
    ...border,
    backgroundColor: Colors.whitish,
    borderTopWidth: 0,
    left: 0,
    position: 'absolute',
    right: 0
  }
};

const styles = StyleSheet.create({
  input: {
    backgroundColor: Colors.whitish,
    height: 40,
    paddingLeft: 3
  },
  ...Platform.select({
    android: { ...androidStyles },
    ios: { ...iosStyles }
  })
});

export default AutoComplete;
