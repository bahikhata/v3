import React from 'react';
import {
    Text,
    View,
    StyleSheet
} from 'react-native';
import { Metrics, Colors } from 'themes';

const Separator = () => {
    return (
        <View style={styles.separator} />
    )
}

export default Separator;

const styles = StyleSheet.create({
    separator: {
        height: Metrics.horizontalLineHeight,
        backgroundColor: Colors.hairline,
        opacity: 0.5
    }
});