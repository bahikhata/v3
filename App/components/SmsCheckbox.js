import CheckBox from '@react-native-community/checkbox'
import {useNetInfo} from '@react-native-community/netinfo'
import {ToastAndroid} from 'react-native'
import {Colors} from '~/themes'
import React from 'react'
import {Text} from 'react-native'

export function SmsCheckbox({value, onValueChange}) {
    const {isConnected} = useNetInfo()

    function showNoInternetAlert() {
        ToastAndroid.show('No internet connection!', ToastAndroid.SHORT)
    }

    return (
        <>
            <CheckBox
                value={isConnected ? value : false}
                onValueChange={e =>
                    isConnected ? onValueChange(e) : showNoInternetAlert()
                }
                style={{
                    alignSelf: 'center',
                }}
            />
            <Text
                style={{
                    fontSize: 16,
                    paddingLeft: 5,
                    color: Colors.hairline,
                    fontWeight: 'bold',
                }}>
                Send SMS Notification
            </Text>
        </>
    )
}
