import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { TouchableHighlight, Text, View, StyleSheet } from 'react-native';
import { Colors, Metrics, Fonts } from 'themes';
import Icon from 'react-native-vector-icons/FontAwesome';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';

const Topbar = () => {
  const [show, setShow] = useState(false);
  const [date, setDate] = useState(new Date());

  const showPicker = () => {
    setShow(true);
  };

  const handleDateChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(false);
    setDate(currentDate);

    if (currentDate !== undefined) {
      // Call your updateTxns function here if needed
    }
  };

  return (
    <TouchableHighlight
      style={styles.dateCalGroup1}
      onPress={showPicker}
    >
      <>
        <Icon
          name='calendar'
          size={Metrics.icons.small}
          color={Colors.snow}
          style={styles.calendarMenu1}
          onPress={showPicker}
        />
        <Text
          style={styles.date1}
          onPress={showPicker}
        >
          {moment(date).format('DD MMM')}
        </Text>
        {show && (
          <DateTimePicker
            value={date}
            minimumDate={new Date()}
            maximumDate={new Date()}
            onChange={handleDateChange}
          />
        )}
      </>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  calendarMenu1: {
    flex: 0.1,
    textAlignVertical: 'center',
  },
  date1: {
    flex: 0.3,
    fontSize: Fonts.size.h4,
    color: Colors.snow,
    fontStyle: 'italic',
    textAlignVertical: 'center'
  },
  dateCalGroup1: {
    flex: 1,
    flexDirection: 'row',
    borderWidth: 1,
    alignItems: 'center'
  }
});

export default Topbar;
