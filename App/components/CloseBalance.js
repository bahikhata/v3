import React, { useMemo } from 'react';
import { View, Text,StyleSheet,Platform } from 'react-native';
import accounting from 'accounting';
import { Colors, Metrics, Images, ApplicationStyles, Fonts } from '../themes';

export default CloseBalance = ({ CloseBalance, receive }) => {
  
  const amountText = useMemo(() => {
    return accounting.formatMoney(CloseBalance, '₹', 0);
  }, [CloseBalance]);
                   
    return (
      <View>
        { receive.length > 0 ? <View style= {ApplicationStyles.separator} /> : null } 
        <View style={styles.group}>
          <Text style={{...styles.amountText}}>
            {amountText}
          </Text>
          <Text style={{...styles.detailText,
                    marginLeft: CloseBalance&&CloseBalance.toString().length>2?8:19,
          }}>CloseBalance</Text>
        </View>
        <View style= {ApplicationStyles.separator} />
      </View>
  );
}
  
const styles = StyleSheet.create({
  group: {
      flexDirection: 'row',
      marginTop: Metrics.baseMargin / 2,
      marginBottom: Metrics.baseMargin / 2, 
  },
  amountText: {
      fontFamily: 'Hind-Regular',
      color: Colors.black,
      fontSize: Fonts.size.small,
      fontStyle: 'italic',
      textAlignVertical: 'center',
      marginLeft: Metrics.baseMargin
  },
  detailText: {
      color: Colors.hairline,
      fontFamily: 'Hind-Regular',
      fontSize: Fonts.size.medium,
      textAlignVertical: 'center',
      fontWeight: 'bold',
      marginLeft: Metrics.smallMargin
  } 
})