export function generateObjectId(len = 10) {
    const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let objectId = '';
    for (let i = 0; i < len; i++) {
        objectId += chars[Math.floor(Math.random() * chars.length)];
    }
    return objectId;
}