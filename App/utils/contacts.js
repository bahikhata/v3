// handle emoji?
const validateName = name => {
  const re = /^[A-Za-z\s]+$/;
  return re.test(name);
};

const validatePhoneNumber = phoneNumber => {
  const re1 = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
  const re2 = /^(?:\+91\s?)?(?:\d{5}\s?\d{5}|\d{10})$/;
  const re3 = /^(?:\+91\s?)?(?:\d{4}\s?\d{3}\s?\d{3}|\d{10})$/;
  const re4 = /^(?:\+91\s?)?(?:\d{3}\s?\d{3}\s?\d{4}|\d{10})$/;
  const re5 = /^(?:\+91\s?)?(?:\d{5}\s?\d{4}|\d{9})$/;
  return (
    phoneNumber.length >= 10 &&
    (re1.test(phoneNumber) ||
      re2.test(phoneNumber) ||
      re3.test(phoneNumber) ||
      re4.test(phoneNumber) ||
      re5.test(phoneNumber))
  );
};

const extractNumber = number => {
  const re = /(\d+)/g;
  const numbers = number.match(re);
  const phone = numbers ? numbers.join('') : '';
  return phone.slice(-10);
};

/**
 * @param {Array<{name: String, number: String}>} contacts
 * @param {Array<String>} previousNumbers
 */
export function removeDuplicatesAndValidate(contacts, previousNumbers) {
  const AllContactsMap = new Map();

  contacts.forEach(contact => {
    const number = extractNumber(contact?.phoneNumbers?.[0]?.number || '');
    if (validatePhoneNumber(number) && validateName(contact.displayName)) {
      AllContactsMap.set(number, contact);
    }
  });

  previousNumbers.forEach(fullNumber => {
    const number = extractNumber(fullNumber);
    AllContactsMap.delete(number);
  });

  const phonebook = [];
  const numbers = [];

  AllContactsMap.forEach((value, key) => {
    phonebook.push({
      name: value.displayName,
      number: `+91${key}`,
    });
    numbers.push(value);
  });

  return {
    phonebook,
    numbers,
  };
}
