import {getAllAccounts, getAllTransactions} from 'services/api.service';

import {realm} from 'services/realm.schema';
import Axios from 'services/axios.service';
import {Account, Transaction} from 'converter';
import {fetchUser} from 'services/auth.service';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useStore } from '../store';

const LAST_SYNCED = 'last_synced';

function setSynced(obj, className) {
  if (className === 'Account') {
    realm.write(() => {
      const account = {
        objectId: obj.objectId,
        name: obj.name,
        balance: obj.balance,
        phone: obj.phone,
        cancelled: obj.cancelled,
        userId: obj.userId,
        createdAt: obj.createdAt,
        updatedAt: obj.updatedAt,
        enabled: obj.enabled,
        synced: true,
      };
      realm.create('Account', account, 'modified');
    });
  } else {
    if (obj.amount === null) return console.log('Amount is null');
    
    realm.write(() => {
      const transaction = {
        objectId: obj.objectId,
        date: obj.date,
        accountId: obj.accountId,
        attachment: obj.attachment,
        amount: obj.amount,
        detail: obj.detail,
        cancelled: obj.cancelled,
        userId: obj.userId,
        createdAt: obj.createdAt,
        updatedAt: obj.updatedAt,
        type: obj.type,
        synced: true,
      };
      realm.create('Transaction', transaction, 'modified');

      if (obj.accountId) {
        const updateAccountBalance = useStore.getState().updateAccountBalance;
        updateAccountBalance(obj.accountId); // update account balance
      }
    });
  }
}

function createAccount(account) {
  const convertedAccount = Account.convertToParse(account);
  return Axios.post('classes/Account', convertedAccount).then(
    response => response.data,
  );
}

function updateAccount(account) {
  const convertedAccount = Account.convertToParse(account);
  return Axios.put(
    `classes/Account/${account.objectId}`,
    convertedAccount,
  ).then(response => response.data);
}

function createTransaction(transaction) {
  const convertedTransaction = Transaction.convertToParse(transaction);
  return Axios.post('classes/Transaction', convertedTransaction).then(
    response => response.data,
  );
}

function updateTransaction(transaction) {
  const convertedTransaction = Transaction.convertToParse(transaction);
  return Axios.put(
    `classes/Transaction/${transaction.objectId}`,
    convertedTransaction,
  ).then(response => response.data);
}

function swapTempUserIdInRealmDb(tempId, userId) {
  const acc = realm.objects('Account');
  const tnx = realm.objects('Transaction');

  acc.map(entry => {
    if (entry.userId === tempId) {
      realm.write(() => {
        const account = {
          objectId: entry.objectId,
          name: entry.name,
          balance: entry.balance,
          phone: entry.phone,
          cancelled: entry.cancelled,
          userId: userId,
          createdAt: entry.createdAt,
          updatedAt: entry.updatedAt,
          enabled: entry.enabled,
          synced: false,
        };

        realm.create('Account', account, 'all');
      });
    }
  });
  tnx.map(entry => {
    if (entry.userId === tempId) {
      realm.write(() => {
        const transaction = {
          objectId: entry.objectId,
          date: entry.date,
          accountId: entry.accountId,
          attachment: entry.attachment,
          amount: entry.amount,
          detail: entry.detail,
          cancelled: entry.cancelled,
          userId: userId,
          createdAt: entry.createdAt,
          updatedAt: entry.updatedAt,
          type: entry.type,
          synced: false,
        };
        realm.create('Transaction', transaction, 'all');
      });
    }
  });
}

export async function syncToServer() {
  console.log('Syncing to server started!');
  const user = await fetchUser();
  const tempId = await AsyncStorage.getItem('temp_user_id');
  const syncStarted = new Date().toISOString();

  if (tempId && user) {
    console.log('Temp user id found'); // need to remigrate data before pushing to production
    swapTempUserIdInRealmDb(tempId, user.objectId);
    await AsyncStorage.removeItem('temp_user_id');
  }

  if (!user) {
    console.log('User not found');
    return;
  }

  const syncPending = await AsyncStorage.getItem('syncPending');

  if (syncPending) {
    console.log('Sync pending');
    const syncData = await AsyncStorage.getItem('syncData');
    const parsedData = JSON.parse(syncData);
    await setContacts(parsedData);
    await AsyncStorage.removeItem('syncPending');
  }

  const userId = user.objectId;
  const accounts = realm.objects('Account').filtered('userId = $0', userId);
  const transactions = realm
    .objects('Transaction')
    .filtered('userId = $0', userId);
  let lastSynced = await AsyncStorage.getItem(LAST_SYNCED);
  if (!lastSynced) {
    lastSynced = undefined;
  }

  const onlineTransactions = (await getAllTransactions(lastSynced)) || [];
  const onlineAccount = (await getAllAccounts(lastSynced)) || [];

  if (!onlineAccount || !onlineTransactions) {
    console.log('No data to sync');
    return;
  }

  // Sync accounts
  const accountsPromise = accounts.map(async account => {
    if (!account.synced) {
      try {
        await createAccount(account);
        setSynced(account, 'Account');
      } catch (error) {
        console.log('Error in account promise', error.message);
      }
    } else {
      // accounts updated after last sync
      const timestamp = new Date(account.updatedAt).getTime();
      const lastSyncedTimestamp = new Date(lastSynced).getTime();

      const existInOnline = onlineAccount.find(
        acc => acc.objectId === account.objectId,
      );

      if (timestamp > lastSyncedTimestamp && !existInOnline) {
        try {
          await updateAccount(account);
          setSynced(account, 'Account');
        } catch (error) {
          console.log('Error in account promise', error.message);
        }
      }
    }
  });

  // sync transactions
  const transactionPromise = transactions.map(async transaction => {
    if (!transaction.synced) {
      try {
        await createTransaction(transaction);
        setSynced(transaction, 'Transaction');
      } catch (error) {
        console.log('Error in tnx promise', error.message);
      }
    } else {
      // transactions updated after last sync
      const timestamp = new Date(transaction.updatedAt).getTime();
      const lastSyncedTimestamp = new Date(lastSynced).getTime();

      const existInOnline = onlineTransactions.find(
        tnx => tnx.objectId === transaction.objectId,
      );

      if (timestamp > lastSyncedTimestamp && !existInOnline) {
        try {
          await updateTransaction(transaction);
          setSynced(transaction, 'Transaction');
        } catch (error) {
          console.log('Error in tnx promise', error.message);
        }
      }
    }
  });

  await Promise.all([...accountsPromise, ...transactionPromise]);

  // Syncing from server
  const onlineAccSync = onlineAccount.map(async account => {
    const localAccount = realm.objectForPrimaryKey('Account', account.objectId);
    if (!localAccount) {
      return realm.write(() => {
        realm.create(
          'Account',
          {...Account.convertToRealm(account), synced: true},
          'modified',
        );
      });
    }
    const localUpdateTimestamp = localAccount.updatedAt.getTime();
    const onlineUpdateTimestamp = new Date(account.updatedAt).getTime();

    if (onlineUpdateTimestamp > localUpdateTimestamp) {
      realm.write(() => {
        realm.create(
          'Account',
          {...Account.convertToRealm(account), synced: true},
          'modified',
        );
      });
    } else if (onlineUpdateTimestamp < localUpdateTimestamp) {
      await updateAccount(localAccount);
    }
  });

  const onlineTnxSyncPromise = onlineTransactions.map(async transaction => {
    const localTransaction = realm.objectForPrimaryKey(
      'Transaction',
      transaction.objectId,
    );
    if (!localTransaction) {
      return realm.write(() => {
        if (transaction.amount === null) return console.log('Amount is null');
        
        realm.create(
          'Transaction',
          {...Transaction.convertToRealm(transaction), synced: true},
          'modified',
        );
      });
    }

    const localUpdateTimestamp = localTransaction.updatedAt.getTime();
    const onlineUpdateTimestamp = new Date(transaction.updatedAt).getTime();

    if (onlineUpdateTimestamp > localUpdateTimestamp) {
      realm.write(() => {
        realm.create(
          'Transaction',
          {...Transaction.convertToRealm(transaction), synced: true},
          'modified',
        );
      });
    } else if (onlineUpdateTimestamp < localUpdateTimestamp) {
      await updateTransaction(localTransaction);
    }
  });

  await Promise.all([...onlineAccSync, ...onlineTnxSyncPromise]);
  await AsyncStorage.setItem(LAST_SYNCED, syncStarted); // set last synced time

  console.log('Syncing to server completed!');
}
