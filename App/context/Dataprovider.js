import React, { useState} from 'react';
import createcontext from './Createcontext';

export default Dataprovider = ({children}) => {
  const [namePhone, setNamePhone] = useState('');
  const [editRefresh, seteditRefresh] = useState(false);
  const [refreshData, setRefreshData] = useState(false);
  const [hideCloseAccount, setHideCloseAccount] = useState(false);
  const [modalView, setModalView] = useState(false);
  const [referesh, setReferesh] = useState(false);
  const [filterClick, setFilterClick] = useState(false);
  const [openMore, setOpenMore] = useState(false);
  const [overDue, setoverDue] = useState(new Date());
  const [showSuccess, setShowSuccess] = useState(false);
  const [showSuccesskhata, setShowSuccesskhata] = useState(false);
  const [downloadOption, setDownloadOption] = useState(false);
  const [selectedValue, setSelectedValue] = useState({name: '', phone: ''});
  const [deviceId, setDeviceId] = useState('');
  const [autoSelectCheckbox, setAutoSelectCheckbox] = useState([]);
  const [reminderRefresh, setReminderRefresh] = useState(false);

  return (
    <createcontext.Provider
      value={{
        editRefresh,
        seteditRefresh,
        namePhone,
        setNamePhone,
        refreshData,
        setRefreshData,
        hideCloseAccount,
        setHideCloseAccount,
        modalView,
        setModalView,
        referesh,
        setReferesh,
        filterClick,
        setFilterClick,
        openMore,
        setOpenMore,
        showSuccess,
        setShowSuccess,
        showSuccesskhata,
        setShowSuccesskhata,
        overDue,
        setoverDue,
        downloadOption,
        setDownloadOption,
        selectedValue,
        setSelectedValue,
        deviceId,
        setDeviceId,
        autoSelectCheckbox,
        setAutoSelectCheckbox,
        reminderRefresh,
        setReminderRefresh,
      }}>
      {children}
    </createcontext.Provider>
  );
};
