/**
 * @format
 * @flow strict-local
 */

import React, { useContext, useState, useEffect } from 'react';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { LogLevel, OneSignal } from 'react-native-onesignal';

import {
  Dummy,
  Khata,
  OpenSource,
  Home,
  Contact,
  ViewImage,
  SetReminder,
  SplashScreen
} from './views';

import {
  EditTxn,
  NewTxn,
  EditName,
  EditAccount,
  NewAccount,
  Onboarding
} from "./views/forms";

import {
  Login,
  Profile,
  VerifyOtp
} from "./views/auth";

import { ContactPicker, UserProfile } from './components';
import { Colors } from './themes';
import createcontext from './context/Createcontext';
import { SyncScreen } from './views/SyncScreen';

const Stack = createNativeStackNavigator();
const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: 'rgb(255, 45, 85)',
  },
};

export default function Index() {

  const APP_ID = process.env.NODE_ENV === "development" ? process.env.REACT_APP_ONESIGNAL_APP_ID : process.env.REACT_APP_ONESIGNAL_APP_ID_PROD;
  const { setDeviceId } = useContext(createcontext)
  const defaultOptions = {
    headerTintColor: Colors.snow,
    headerStyle: { backgroundColor: Colors.primary },
  }

  useEffect(() => {
    const initialize = async () => {
      OneSignal.Debug.setLogLevel(LogLevel.Verbose);
      OneSignal.initialize(APP_ID);
      OneSignal.Notifications.requestPermission(true);
      OneSignal.User.pushSubscription.optIn();
      OneSignal.User.pushSubscription.addEventListener('change', (subscription) => {
          console.log(subscription?.current?.id)
          setDeviceId(subscription?.current?.id)
      });
    }

    // initialize(); // TODO: uncomment this line later!
  }, [])


  return (
    <NavigationContainer theme={MyTheme}>
      <Stack.Navigator initialRouteName={"SplashScreen"}>
        <Stack.Screen name="SplashScreen" component={SplashScreen} options={{ headerShown: false }} />
        <Stack.Screen name="Onboarding" component={Onboarding} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="VerifyOTP" component={VerifyOtp} options={{ headerShown: false }} />
        <Stack.Screen name="Profile" component={Profile} options={{ ...defaultOptions, headerShown: false }} />
        <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
        <Stack.Screen name="Single-Khata" component={Khata} options={{ ...defaultOptions, headerShown: false }} />
        <Stack.Screen name="UserProfile" component={UserProfile} options={{ ...defaultOptions, headerShown: false }} />
        <Stack.Screen name="View Image" component={ViewImage} options={defaultOptions} />
        <Stack.Screen name="Contact" component={Contact} />
        <Stack.Screen name="ContactPicker" component={ContactPicker} options={{ animation: 'slide_from_right', headerShown: false }} />
        <Stack.Screen name="New Transaction" component={NewTxn} options={{ ...defaultOptions, }} />
        <Stack.Screen name="Edit Transaction" component={EditTxn} options={defaultOptions} />
        <Stack.Screen name="Edit Account" component={EditAccount} options={defaultOptions} />
        <Stack.Screen name="New Account" component={NewAccount} options={defaultOptions} />
        <Stack.Screen name="Edit Name" component={EditName} options={defaultOptions} />
        <Stack.Screen name="Set Reminder" component={SetReminder} options={defaultOptions} />
        <Stack.Screen name="OpenSource" component={OpenSource} options={defaultOptions} />
        <Stack.Screen name="Dummy" component={Dummy} />
        <Stack.Screen name="SyncScreen" component={SyncScreen} options={{ headerShown: false }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
