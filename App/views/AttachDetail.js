import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Image, ImageStore, ImageEditor, Text, Share } from 'react-native';
import { iconsMap, iconsLoaded } from '../app-icons';
import { Colors } from '../themes';

const AttachDetail = (props) => {
  useEffect(() => {
    props.navigator.toggleTabs({
      to: 'hidden',
      animated: false
    });

    iconsLoaded.then(() => {
      props.navigator.setButtons({
        rightButtons: [
          {
            icon: iconsMap.share,
            id: 'share',
            buttonColor: Colors.snow
          }
        ],
        animated: false
      });
    });

    // Cleanup
    return () => {
      props.navigator.toggleTabs({
        to: 'shown',
        animated: false
      });
    };
  }, []); // Empty dependency array ensures this effect runs only once, equivalent to componentDidMount

  const onNavigatorEvent = (event) => {
    if (event.id === 'share') {
      const imageURL = props.txn.attach;
      Image.getSize(imageURL, (width, height) => {
        const imageSize = {
          size: { width, height },
          offset: { x: 0, y: 0 }
        };
        ImageEditor.cropImage(imageURL, imageSize, (imageURI) => {
          ImageStore.getBase64ForTag(imageURI, (base64Data) => {
            // console.log(base64Data);
            /* Share.open({
              message: I18n.t('shareMessage'),
              title: I18n.t('shareTitle'),
              url: `data:image/png;base64,${base64Data}`,
              type: 'image/png'
            }); */
            ImageStore.removeImageForTag(imageURI);
          }, (reason) => console.log(reason));
        }, (reason) => console.log(reason));
      }, (reason) => console.log(reason));
    }
  };

  // Attach onNavigatorEvent to props.navigator only once
  useEffect(() => {
    props.navigator.setOnNavigatorEvent(onNavigatorEvent);

    // Cleanup
    return () => {
      props.navigator.setOnNavigatorEvent(null);
    };
  }, []); // Empty dependency array ensures this effect runs only once

  return (
    <View style={styles.container}>
      <Image
        style={{ flex: 0.9, resizeMode: 'contain' }}
        source={{ uri: props.txn.attach }}
      />
      <View style={{ flex: 0.1 }}>
        {props.txn.account ? (
          <Text style={[styles.text, { fontWeight: 'bold' }]}>
            {props.txn.account.name}
          </Text>
        ) : null}
        <Text style={styles.text}>{props.txn.detail}</Text>
      </View>
    </View>
  );
};

AttachDetail.propTypes = {
  navigator: PropTypes.object,
  txn: PropTypes.object,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.black,
  },
  text: {
    color: Colors.snow,
    textAlign: 'center',
  },
});

export default AttachDetail;
