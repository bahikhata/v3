import React from 'react';
import {
  Text,
  View,
  TextInput,
  ScrollView,
  TouchableHighlight,
  FlatList,
  StyleSheet,
  Button,
  Linking
} from 'react-native';
import { Colors, Metrics, Images } from 'themes';

const Contact = ({navigation}) => {
    return (
      <View style={styles.container}>
        {/* <View style={styles.headerView}>
          <Text style={styles.headerText}>Contact</Text>
        </View> */}
        <View style={styles.border} />
        <TouchableHighlight
          underlayColor={Colors.underlayColor}
          style={styles.buttonView}
          onPress={() => {
            Linking.openURL('whatsapp://send?text=hello&phone=+91-7456883325');
          }}
        >
          <Text style={styles.buttonText}>Whatsapp</Text>
        </TouchableHighlight>
        <TouchableHighlight
          underlayColor={Colors.underlayColor}
          style={styles.buttonView}
          onPress={() => {
            Linking.openURL('tel:+91-7456883325');
          }}
        >
          <Text style={styles.buttonText}>Phone</Text>
        </TouchableHighlight>
        <TouchableHighlight
          underlayColor={Colors.underlayColor}
          style={styles.buttonView}
        >
          <Text style={styles.supportText}>Timing : Mon-Sat (10am - 7pm)</Text>
        </TouchableHighlight>
      </View>
    );
}

export default Contact;

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 180,
    backgroundColor: Colors.snow,
    borderRadius: 10
  },
  border: {
    width: '100%',
    height: 1,
    backgroundColor: Colors.primary,
    marginBottom: 10
  },
  buttonView: {
    width: '100%',
    padding: 10,
    paddingLeft: 20
  },
  buttonText: {
    color: Colors.black,
    fontSize: 16,
    fontStyle: 'normal'
  },
  supportText: {
    color: Colors.black,
    fontSize: 10,
    fontStyle: 'italic'
  },
  headerView: {
    width: '100%',
    padding: 10,
    alignItems: 'center'
  },
  headerText: {
    color: Colors.primary,
    fontSize: 18
  }
});
