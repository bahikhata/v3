import React from 'react';
import {View, Button} from 'react-native';

import {sendPaidSMS} from '../services/sms.service';

function Dummy({navigation}) {
  const handler = async () => {
    sendPaidSMS(100, '+918778257327', 'accountId');
  };

  return (
    <View>
      <Button title="Process" onPress={handler} />
    </View>
  );
}

export default Dummy;
