import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, TextInput, ScrollView, ToastAndroid } from 'react-native';
import { Colors, Metrics, ApplicationStyles, Fonts } from 'themes'; // Ensure these are correctly imported from your themes

import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/Feather';
import Usericon from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';

import SelectDropdown from 'react-native-select-dropdown';
import { useNavigation } from '@react-navigation/native';
import { auth } from 'services';

const Profile = () => {
  const [editMode, setEditMode] = useState(false);
  const [emailVerified, setEmailVerified] = useState(false);
  const [previousEmail, setPreviousEmail] = useState('');
  const [refresh, setRefresh] = useState(false);
  const [userInfo, setUserInfo] = useState({
    username: '',
    Phone: '',
    BusinessName: '',
    TypeOfBusiness: '',
    Email: '',
  });
  const navigation = useNavigation();

  useEffect(() => {
    const fetchUser = async () => {
      const user = await auth.fetchUser();
      setUserInfo({
        username: user.username ? user.username : null,
        Phone: user.phone ? user.phone : "Enter Phone",
        BusinessName: user.businessName ? user.businessName : "Enter Business Name",
        TypeOfBusiness: user.typeOfBusiness ? user.typeOfBusiness : "Enter Type of Business",
        Email: user.email ? user.email : "Enter your Email",
      });
      setPreviousEmail(user.email);
      setEmailVerified(user.emailVerified);
    }
    fetchUser();

  }, [refresh]);

  const validateEmail = (email) => {

    if (email === "") {
      return true;
    }
    const re = /\S+@\S+\.\S+/;
    return  re.test(email) ;
  };

  const toggleEditMode = () => {
    setEditMode(!editMode);
  };

  // Handler to update user info state
  const updateUserInfo = (key, value) => {
    setUserInfo(prevState => ({
      ...prevState,
      [key]: value,
    }));
  };

  const handleSave = async () => {
    let check = false;
    if (userInfo.Phone) {
      const res = await auth.setPhone(userInfo.Phone);
      if (res) {
        check = true;
      }
    }
    if (userInfo.BusinessName) {
      const res = await auth.setBusinessName(userInfo.BusinessName)
      if (res) {
        check = true;
      }
    }
    if (validateEmail(userInfo.Email)) {
      if (!emailVerified) {
        const res = await auth.setEmail(userInfo.Email)
        if (res) {
          check = true;
        }
      }
      else if(emailVerified &&userInfo.Email!="" &&userInfo.Email!==privousEmail){
        const res = await auth.setEmail(userInfo.Email)
        .then((res)=>{
          if(res){
            check = true;
            setRefresh(!refresh);
          }
        })
      }
      else if(emailVerified &&userInfo.Email===""){
        const res = await auth.setEmail(userInfo.Email)
        .then((res)=>{
          if(res){
            check = true;
            setRefresh(!refresh);
          }
        })
      }
    }
    else {
      ToastAndroid.show('Email Not Valid', ToastAndroid.SHORT);
      setEditMode(true);
    }

    if (userInfo.TypeOfBusiness) {
      const res = await auth.setTypeOfBusiness(userInfo.TypeOfBusiness)
      if (res) {
        check = true;
      }
    }
    if (check) {
      ToastAndroid.show('Data Updated', ToastAndroid.SHORT);
      setEditMode(false);
    }
    else {
      ToastAndroid.show('Data Not Updated', ToastAndroid.SHORT);
      setEditMode(true);
    }

  }


  return (
    <ScrollView
      contentContainerStyle={{
        justifyContent: 'center',
        // padding: 10, // Moved from `style` to `contentContainerStyle`
      }}
      style={{ flex: 1, backgroundColor: "#ffffff" }} // Keep only ScrollView specific styles here
    >


      <View style={styles.imageContainer}>
        <TouchableOpacity style={{ position: 'absolute', top: 20, left: 20 }} onPress={() => navigation.goBack()}>
          <Icon1 name="arrow-left" size={30} color={Colors.snow} />
        </TouchableOpacity>
        <Text style={{ top: 35, fontSize: 25,fontWeight:'bold', color: Colors.snow }}>{userInfo.BusinessName}</Text>

        <Image
          source={{ uri: 'https://tamilnaducouncil.ac.in/wp-content/uploads/2020/04/dummy-avatar.jpg' }}
          style={styles.profileImage}
        />
      </View>



      <View style={{ ...styles.userInfoSection, top: 20 }}>
        <View style={{ ...styles.optionItem }} >
          <Usericon name='user' style={styles.userKey} />
          <Text style={{...styles.userValue,}}>{
            userInfo.username
          }</Text>
        </View>
        <View style={ApplicationStyles.separator} />

        <View style={{ ...styles.optionItem }}>
          <Icon name='mobile-phone' style={styles.userKey} />
          {editMode ? (
            <TextInput
              style={styles.userValue}
              value={userInfo.Phone}
              onChangeText={(text) => updateUserInfo("Phone", text)}
            />
          ) : (
            <Text style={{...styles.userValue,marginLeft:40}}>{
              userInfo.Phone
            }</Text>
          )}
        </View>
        <View style={ApplicationStyles.separator} />

        <View style={{ ...styles.optionItem }}>
          <Ionicons name='business-outline' style={styles.userKey} />
          {editMode ? (
            <TextInput
              style={styles.userValue}
              value={userInfo.BusinessName}
              onChangeText={(text) => updateUserInfo("BusinessName", text)}
            />
          ) : (
            <Text style={styles.userValue}>{
              userInfo.BusinessName
            }</Text>
          )}
        </View>
        <View style={ApplicationStyles.separator} />

        <View style={{
          ...styles.optionItem,
          // marginLeft: editMode ? 0 : 15,
        }}>
          <Ionicons name='options' style={{...styles.userKey}} />
          {
            editMode ? (
              <SelectDropdown
                data={["Manufacturer", "Service", "Trader"]}
                onSelect={(selectedItem, index) => {
                  updateUserInfo('TypeOfBusiness', selectedItem)
                }}
                buttonTextAfterSelection={(selectedItem, index) => {
                  return selectedItem
                }}
                rowTextForSelection={(item, index) => {
                  return item
                }}

                defaultButtonText={userInfo.TypeOfBusiness}
                buttonStyle={styles.userValue}
                buttonTextStyle={styles.dropdownText}
                dropdownStyle={styles.dropdown}
                rowStyle={styles.dropdownText}
                rowTextStyle={styles.dropdownText}
              />

            ) : (
              <Text style={styles.userValue}>{userInfo.TypeOfBusiness}</Text>
            )
          }
        </View>
        <View style={ApplicationStyles.separator} />

        <View style={{
          ...styles.optionItem,
        }}>
          <Icon name='envelope' style={styles.userKey} />
          {
            editMode ? (
              <TextInput
                style={styles.userValue}
                value={userInfo.Email}
                onChangeText={(text) => updateUserInfo('Email', text)}
              />
            ) : (
              <Text style={styles.userValue}>{userInfo.Email.length>0?userInfo.Email:"Enter your Email"}</Text>
            )
          }
        </View>
        <View style={ApplicationStyles.separator} />

        <View style={
          styles.veryEmail
        }>
          {
            userInfo.Email!=="Enter your Email"&&<Text style={{
              ...styles.emailText,
              color: emailVerified  ? 'green' : 'red',
              marginLeft: 19,
            }}>
  
              {emailVerified ? 'Email Verified' : 'Email Not Verified '}
            </Text>
          }
          {
            !emailVerified  && <TouchableOpacity style={styles.verifyButton} onPress={async () => {
              const res = await auth.setEmail(userInfo.Email);
             
              if (res) {
                ToastAndroid.show('Email Verification Code Sent', ToastAndroid.SHORT);
                navigation.goBack();
              }
              else {
                ToastAndroid.show('Email Verification Code Not Sent', ToastAndroid.SHORT);
              }
            }}>
              <Text style={styles.button}>Verify</Text>
            </TouchableOpacity>
          }
        </View>
        {/* Edit Button */}
        <TouchableOpacity style={styles.editButton} onPress={
          editMode ? handleSave : toggleEditMode
        }>
          <Text style={{
            ...styles.editButtonText
          }}>{editMode ? 'Save ' : 'Edit'}</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default Profile;

// Styles remain unchanged except for possibly adding a style for TextInput
const styles = StyleSheet.create({

  dropdownControl: {
   width:"100%",
   backgroundColor: Colors.whitish,

  },
  dropdown: {
    padding: Metrics.baseMargin,
    backgroundColor: Colors.snow,
    height: 165,
    width: "73%",
  },
  dropdownText: {
    color: Colors.black,
    marginLeft: 16,
    paddingLeft: 6,
    fontSize: 16,
    justifyContent: 'flex-start',
    textAlign: 'left',
  },
  userValue: {
    fontSize: 16,
    marginBottom: 5,
    marginLeft:30,
    width: "100%",
    backgroundColor: Colors.whitish,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 10,
    backgroundColor: Colors.snow,
  },
  optionItem: {
    flex:1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    alignSelf: 'center',
    paddingLeft: Metrics.baseMargin,
    borderColor: Colors.whitish,
    marginTop: 8,
    marginLeft:Metrics.baseMargin+15,
  },
  imageContainer: {
    // height: 150,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 10,
    padding: 45,
    position: 'relative',
    backgroundColor: Colors.primary,
    borderBottomEndRadius: 145,
    borderBottomStartRadius: 145,
  },
  profileImage: {
    width: 90,
    height: 90,
    borderRadius: 60,
    top: 75,
  },
  userInfoSection: {
    marginBottom: 20,
    padding: 10,
  },
  userKey: {
    fontSize: 24,
    marginBottom: 5,
    padding: 10,
    marginLeft: 10,
  },

  emailText: {
    fontSize: 16,
    marginBottom: 5,
  },

  editButton: {
    width: "40%",
    marginTop: 12,
    backgroundColor: Colors.primary,
    paddingHorizontal: 20,
    paddingVertical: 12,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },

  veryEmail: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 10,
  },
  verifyButton: {
    backgroundColor: Colors.primary,
    marginLeft: 10,
    fontSize: 16,
    borderRadius: 5,
  },
  button: {
    color: '#ffffff',
    fontSize: 16,
    padding: 5,
  },
  editButtonText: {
    color: '#ffffff',
    fontSize: 18,
    textTransform: 'uppercase',
  },
});
