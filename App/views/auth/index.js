import Login from './Login';
import Profile from './Profile';
import VerifyOtp from './VerifyOtp';

export {
    Login,
    Profile,
    VerifyOtp
}

