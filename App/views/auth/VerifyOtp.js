import React, { useContext, useEffect, useState } from 'react'
import {
    ToastAndroid, Text, View, TextInput,
    ScrollView, StyleSheet, PermissionsAndroid, ActivityIndicator, StatusBar,
    TouchableOpacity
} from 'react-native';
import { Button } from "react-native-paper";
import { useNavigation, useRoute } from '@react-navigation/native';
import { sms, auth, mixpanel, api } from 'services';
import { Colors } from 'themes';
import AsyncStorage from '@react-native-async-storage/async-storage';
//import RNOtpVerify from 'react-native-otp-verify';

import { useStore } from 'store';

const VerifyOtp = () => {
    const [seconds, setSeconds] = useState(30);
    const [isActive, setIsActive] = useState(true);
    const [loading, setLoading] = useState(false);
    const [resend, setResend] = useState(false);
    const [recall, setRecall] = useState(false);
    const [otp, setOTP] = useState("");
    const [error, setError] = useState("");
    const navigation = useNavigation();
    const route = useRoute();
    let phone = route.params?.phone || '';
    const [isLoginSuccess, setIsLoginSuccess] = useState(false);

    const write = useStore(state => state.write);

    useEffect(() => {
        if (seconds > 0 && isActive) {
            setTimeout(() => setSeconds(seconds - 1), 1000);
        } else if (seconds === 0 && isActive) {
            setIsActive(false)
        }
    }, [otp, seconds, navigation]);

    useEffect(() => {
        const requestSMSPermission = async () => {
            if (Platform.OS === 'android') {
                try {
                    const granted = await PermissionsAndroid.request(
                        PermissionsAndroid.PERMISSIONS.READ_SMS,
                        {
                            title: 'SMS Permission',
                            message: 'This app needs access to your SMS messages to automatically fill OTP.',
                            buttonNeutral: 'Ask Me Later',
                            buttonNegative: 'Cancel',
                            buttonPositive: 'OK',
                        }
                    );
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                        //SetAutofillOTP();
                    } else {
                        console.log('SMS permission denied');
                    }
                } catch (err) {
                    console.warn(err);
                }
            }
        };
        requestSMSPermission();
    }, []);

    const SetAutofillOTP = async () => {

        /*RNOtpVerify.getHash().then((hash) => {
            console.log('Hash: ', hash)
        }
        );

        RNOtpVerify.getOtp()
            .then(p =>
                RNOtpVerify.addListener(message => {
                    console.log(message)
                    try {
                        if (message) {
                            const otp = /(\d{4})/g.exec(message[0])[1];
                            setOTP(otp);
                        }
                    } catch (error) {
                        console.log(error);
                    }
                })
            )
            .catch(p => console.log(p));*/
    };

    const configMixpanel = async (phone, _res) => {
        api.getInstallationId().then((result) => {
            console.log(result);
        });

        const old_id = await AsyncStorage.getItem("old_user_id")
        if (old_id) mixpanel.identify(old_id);
        else mixpanel.identify("+91" + phone);

        mixpanel.getPeople().set({
            "$name": _res.result ? _res.result.businessName : "",
            "$phone": phone,
            "$email": _res.result ? _res.result.email : "",
            "$created": new Date(),
        });
    }

    const Signup = async () => {
        if (loading) return;

        setLoading(true);
        if (otp.length === 4) {
            const res = await sms.verifyOTP("91" + phone, otp);
            console.log(res);

            if (res.result.sessionToken) {
                const Token = res.result.sessionToken;

                await AsyncStorage.setItem('sessionToken', Token);
                auth.fetchUser(true).then(async (_res) => {
                    if (_res.businessName) {
                        console.log(_res);
                        await AsyncStorage.setItem('user', JSON.stringify(_res));
                        navigation.navigate('SyncScreen');
                    } else {
                        navigation.navigate('Onboarding');
                    }
                });

                configMixpanel(phone, res);

                setIsLoginSuccess(true);
                ToastAndroid.showWithGravity(
                    'Login Success!, Syncing data',
                    ToastAndroid.SHORT,
                    ToastAndroid.CENTER,
                );
            }
            else {
                setLoading(false)
                setError("Please enter a valid OTP");
            }
        } else {
            setLoading(false)
            setError("Please enter a valid OTP");
        }
        setTimeout(() => {
            setLoading(false);
        }, 5000);
    }

    const handleResendOTPviaCall = async () => {
        if (recall) {
            return;
        }
        setRecall(true)
        const res = await sms.resendOTP("91" + phone, "call")
        if (res.result.type === "success") {
            setRecall(false)
            setIsActive(true)
            setSeconds(30)
        } else if (res.result.type === "error") {
            setRecall(false)
            ToastAndroid.showWithGravity(
                res.result.message,
                ToastAndroid.SHORT,
                ToastAndroid.CENTER
            );
        }

        setTimeout(() => {
            setRecall(false);
        }, 4000);

    }

    const handleResendOTPviaText = async () => {
        if (resend) {
            return;
        }
        setResend(true)
        const res = await sms.resendOTP("91" + phone, "text")

        if (res.result.type === "success") {
            setResend(false)
            setIsActive(true)
            setSeconds(30)
        } else if (res.result.type === "error") {
            setResend(false)
            ToastAndroid.showWithGravity(
                res.result.message,
                ToastAndroid.SHORT,
                ToastAndroid.CENTER
            );
        }
        setTimeout(() => {
            setResend(false);
        }, 4000);

    }

    return (
        <ScrollView style={styles.main}
            keyboardShouldPersistTaps='always'
            // hide scrole bar 
            showsVerticalScrollIndicator={false}>
            <StatusBar backgroundColor={Colors.primary} barStyle="light-content" />

            <View style={styles.Card} >
                <Text style={styles.h1}>Bahi<Text style={styles.h1_span}>Khata</Text></Text>
            </View>
            <View>
                <View>
                    <Text style={styles.label}>+91 {phone} </Text>
                    <TextInput
                        style={styles.input}
                        keyboardType='numeric'
                        autoComplete="sms-otp"
                        textContentType="oneTimeCode"
                        placeholder="Enter OTP"
                        multiline={false}
                        autoCorrect={false}
                        autoFocus
                        maxLength={4}
                        onChangeText={
                            (text) => {
                                setOTP(text),
                                    setError("")
                            }
                        }
                    />
                    <View
                        style={{
                            marginTop: 10,
                            display: "flex",
                            flexDirection: "row",

                        }}
                    >
                        {
                            error ?
                                <Text style={{
                                    color: Colors.fire,
                                    justifyContent: "flex-start",
                                    textAlign: "left",
                                    marginLeft: 0,
                                    fontSize: 13,
                                }}>
                                    {error}
                                </Text> : null
                        }
                        <Text style={{
                            fontSize: 13,
                            flex: 1,
                            justifyContent: "flex-end",
                            textAlign: "right",
                        }}>Resend OTP in {seconds} s </Text>

                    </View>

                </View>

            </View>

            <View style={{ marginTop: 30 }}>
                <Button
                    disabled={loading}
                    onPress={Signup}
                    uppercase
                    dark
                    compact
                    mode="contained"
                    style={{
                        backgroundColor: loading ? Colors.hairline : Colors.primary,
                        padding: 10, marginTop: 30
                    }}>
                    <Text style={{ color: Colors.snow }}>
                        {isLoginSuccess ? 'Syncing data!' : 'Verify & LogIn'}
                    </Text>
                </Button>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: "space-between", marginTop: 20 }}>
                    <Button
                        disabled={isActive && !resend && !recall}
                        onPress={handleResendOTPviaText}
                        style={{ backgroundColor: isActive && !resend && !recall ? Colors.hairline : Colors.primary }}
                        uppercase
                        dark
                        compact
                        mode="contained"
                    ><Text style={{ fontSize: 10, color: Colors.snow }}>Resend via SMS</Text></Button>
                    <Button
                        disabled={isActive && !resend && !recall}
                        onPress={handleResendOTPviaCall}
                        style={{ backgroundColor: isActive && !resend && !recall ? Colors.hairline : Colors.primary }}
                        mode="contained"
                        compact
                        uppercase
                        dark
                    >
                        <Text style={{ fontSize: 10, color: Colors.snow }}>Resend via Call</Text>
                    </Button>

                </View>
            </View>
            {loading ? (
                <View style={{ flex: 0.6, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="large" color="red" />
                </View>
            ) : null}
        </ScrollView>
    )
}

export default VerifyOtp;

const styles = StyleSheet.create({
    resendOTP: {
        color: "red",
        fontSize: 16,
        fontWeight: "500",
        marginTop: 5,
    },
    main: {
        marginTop: 30,
        marginRight: 50,
        marginLeft: 50,
    },
    Card: {
        display: "flex",
        flexDirection: "column",
        alignItems: 'center',
        alignContent: 'center',
        marginTop: 30,
    },
    h1: {
        fontSize: 32,
        color: "black",
        borderBottomWidth: 2,
        borderBottomColor: 'red',
        fontWeight: 'bold',

    },
    h1_span: {
        fontSize: 32,
        color: "red",
        fontWeight: 'bold',

    },
    phone: {
        marginTop: 30,
        fontSize: 28,
        fontWeight: 'bold',
        color: "black"
    },
    input: {
        borderBottomWidth: 1,
        borderColor: "gray",
        width: 300
    },
    label: {
        marginTop: 30,
        fontSize: 18,
        fontWeight: '600',
        color: "black"
    },

    button: {

        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        borderRadius: 10,
        fontSize: 20,
        fontWeight: 'bold',
        backgroundColor: 'red',
    },

});
