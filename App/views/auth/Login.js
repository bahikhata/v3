import React, { useState, useContext, useEffect } from 'react';
import {
  Text, View, TextInput, ScrollView, StyleSheet, Image, TouchableOpacity, Alert, ToastAndroid, StatusBar
} from 'react-native';
import  * as sms  from 'services/sms.service';
import { Images, Colors,ApplicationStyles } from "themes";
import { useNavigation } from '@react-navigation/native';
import { Button } from "react-native-paper";

const Login = () => {
  const [phone, setPhone] = useState("");
  const [error, setError] = useState("");
  const navigation = useNavigation();

  const [loading, setLoading] = useState(false);
  const validatePhone = (phone) => {
    const reg = /^[0]?[6789]\d{9}$/;
    return reg.test(phone);
  };

  useEffect(() => {
    setError("");
  }, [phone]);

  const handleClick = () => {
    if (loading) {
      return;
    }
    setLoading(true); // Start loading
    if (phone.length === 10 && validatePhone(phone)) {
      sms.sendOTP("91" + phone).then((res) => {
        if (res.type == "success") {
          setPhone("");
          navigation.navigate('VerifyOTP', { phone: phone });
        } else {
          setError("Something went wrong. Please try again");
        }
      });
    } else {
      setError("Please enter a valid phone number");
    }
    setLoading(false); // Stop loading

    setTimeout(() => {
      setLoading(false);
    }, 4000);
  };

  return (
    <ScrollView
      style={styles.main}
      keyboardShouldPersistTaps='always'
      showsVerticalScrollIndicator={false}>
      <StatusBar  backgroundColor={Colors.primary} barStyle="light-content" />

      <View style={styles.Card} >
        <Text style={styles.h1}>Bahi<Text style={styles.h1_span}>Khata</Text></Text>
      </View>

      <View >
        <View>
          <View style={{ flexDirection: 'row', alignItems: "center", paddingTop: 50 }}>
            <Image
              source={Images.india}
              style={{ width: 30, height: 30, marginRight: 10, borderRadius: 500 }}
            />
            <TextInput
              style={styles.input}
              keyboardType='numeric'
              placeholder="Enter Number"
              multiline={false}
              autoCorrect={false}
              autoFocus
              maxLength={10}
              onChangeText={(text) => setPhone(text)}
              value={phone}
            />
          </View>
          { error ? <Text style={{...ApplicationStyles.inputError, top:6, paddingLeft:19 }}>
                        {error}
                    </Text> : null
            }
        </View>

      </View>

      <Button
        disabled={loading}
        onPress={handleClick}
        buttonColor={Colors.darkPrimary}
        rippleColor={Colors.primary}
        uppercase
        dark
        compact
        mode="contained"
        style={{ padding: 10, marginTop: 30, 
        backgroundColor: loading ? Colors.hairline : Colors.primary,
        }}>
        <Text style={{color:Colors.snow}}>
        Send OTP
        </Text>
      </Button>

    </ScrollView>
  )
    ;
}

export default Login;

const styles = StyleSheet.create({
  main: {
    marginTop: 30,
    marginRight: 50,
    marginLeft: 50,
  },
  Card: {
    display: "flex",
    flexDirection: "column",
    alignItems: 'center',
    alignContent: 'center',
    marginTop: 150,

  },
  h1: {
    fontSize: 32,
    color: "black",
    borderBottomWidth: 2,
    borderBottomColor: 'red',
    fontWeight: 'bold',

  },
  h1_span: {
    fontSize: 32,
    color: Colors.primary,
    fontWeight: 'bold',

  },
  phone: {
    marginTop: 30,
    fontSize: 28,
    fontWeight: 'bold',
    color: "black"
  },
  input: {
    borderBottomWidth: 1,
    borderColor: "gray",
    width: 300
  },
  label: {
    marginTop: 30,
    fontSize: 18,
    fontWeight: '600',
    color: "black"
  },
  button: {
    marginTop: 50,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: 60,
    borderRadius: 10,
    fontSize: 20,
    fontWeight: 'bold',
    backgroundColor: 'red',
    curser: 'pointer'
  }
});
