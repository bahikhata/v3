import { syncToServer } from '../sync';
import { useNavigation } from '@react-navigation/native';
import { ToastAndroid, Image, StyleSheet } from 'react-native';
import React, { useState, useEffect } from 'react';
import { View, Text, ProgressBarAndroid } from 'react-native';
import { useStore } from '../store';
import { produce } from 'immer';
import Images from '../themes/Images';

export function SyncScreen() {
  const [syncStatus, setSyncStatus] = useState('Syncing...');
  const navigation = useNavigation();
  const syncInfo = useStore(state => state.syncInfo);

  const user = useStore(state => state.user);
  const write = useStore(state => state.write);

  const accountsTotal = syncInfo.accounts.total;
  const accountsSynced = syncInfo.accounts.synced;
  const accountsRatio = accountsTotal
    ? accountsSynced / accountsTotal
    : undefined;

  const transactionsTotal = syncInfo.transactions.total;
  const transactionsSynced = syncInfo.transactions.synced;
  const transactionsRatio = transactionsTotal
    ? transactionsSynced / transactionsTotal
    : undefined;

  useEffect(() => {
    syncToServer()
      .then(() => {
        setSyncStatus('Synced!');
        navigation.navigate('Home');
      })
      .catch(err => {
        console.error(err);
        ToastAndroid.show('Sync failed!', ToastAndroid.SHORT);
        navigation.navigate('Home');
      });
  }, [syncToServer]);

  return (
    <View style={styles.screen}>
      <View style={styles.imageView}>
        <Image source={Images.syncBackground} style={styles.syncBg} />
      </View>
      <Text
        style={styles.header}>
        One moment, please
      </Text>
      <Text style={styles.subheader}>We're syncing your transactions for offline use</Text>

      <View style={styles.content}>
        <ProgressBarAndroid
          styleAttr="Horizontal"
          indeterminate={!transactionsRatio}
          progress={transactionsRatio}
          animating={true}
          color={"#bb6bce"}
        />
        <View style={{ display: 'flex' }}>
          <Text>
            Transactions synced:{' '}
            {transactionsRatio ? (
              <Text>
                {transactionsSynced}/{transactionsTotal}
              </Text>
            ) : (
              <Text>calculating...</Text>
            )}
          </Text>
        </View>
        {/* <ProgressBarAndroid
          styleAttr="Horizontal"
          indeterminate={!accountsRatio}
          progress={accountsRatio}
          animating={true}
        /> */}
        {/* <View style={{ display: 'flex' }}>
          <Text>
            Accounts synced:{' '}
            {accountsRatio ? (
              <Text>
                {accountsSynced}/{accountsTotal}
              </Text>
            ) : (
              <Text>calculating...</Text>
            )}
          </Text>
        </View> */}
      </View>
    </View>
  );
}


const styles = StyleSheet.create({
  syncBg: {
    width: '50%',
    resizeMode: 'contain'
  },
  imageView: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: "50%"
  },
  header: {
    textAlign: 'center',
    fontSize: 25,
    color: 'black',
    fontWeight: "500"
  },
  subheader: {
    textAlign: 'center',
    fontSize: 18,
    color: 'black',
    fontWeight: "100"
  },
  content: {
    padding: "10%"
  },
  screen: {
    height: '100%',
    width: '100%',
    backgroundColor: '#fff',
  }
});
