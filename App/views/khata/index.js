import React, { useEffect, useState, useContext, useRef, memo, useMemo } from 'react';
import {
    StyleSheet, Text, View, Image, FlatList, TouchableHighlight, ImageBackground,
    SafeAreaView, TouchableOpacity, ActivityIndicator, ToastAndroid
} from 'react-native';
import { useRoute, useNavigation } from '@react-navigation/native';
import createcontext from 'context/Createcontext';
import MonthPicker from 'react-native-month-year-picker';
import Svg, { Path } from 'react-native-svg';
import moment from 'moment';
import { Colors, Metrics, Images, ApplicationStyles, Fonts } from 'themes';
import Icon from "react-native-vector-icons/FontAwesome";
import { FloatingAction } from "react-native-floating-action";
import { api, auth } from 'services';
const _ = require("lodash");
const accounting = require('accounting');
import { Linking, Alert } from 'react-native';
import GestureRecognizer from 'react-native-swipe-gestures';
import * as Animatable from 'react-native-animatable';
import TransitionModal from './TransitionModal';
import {
    Separator, ActionSheet, CloseBalance,
    OpenBalance, AppHeader
} from 'components';
import Modal from 'react-native-modal';
import { useTransaction } from 'hooks/useTransaction';
import { useAccount } from 'hooks/useAccount';
import { useStore } from 'store';
import AntIcon from 'react-native-vector-icons/AntDesign';

const AccountDetail = () => {
    const route = useRoute()
    const { id, dateview } = route.params

    const writeStore = useStore(state => state.write);
    const khatapageTopName = useStore(state => state.khatapageTopName);
    const date = useStore(state => state.accountDate);
    const setDate = _date => {
        writeStore({
            accountDate: _date,
        });
    };

    const tnx = useStore(state => state.tnxsPerAccount);
    const cashIn = useStore(state => state.receivedPerAccount);
    const cashOut = useStore(state => state.givenPerAccount);

    const closeAmount = useStore(state => state.closeBalancePerAccount);
    const openAmount = useStore(state => state.openBalancePerAccount);

    const [show, setShow] = useState(false)
    const [actionSheet, setActionSheet] = useState(false);
    const [downloadLoading, setDownloadLoading] = useState(false)
    const [BusinessName, setBusinessName] = useState('');
    const closeActionSheet = () => setActionSheet(false);
    const [selectTxn, setSelectTxn] = useState({});
    const [selectedAccoutnId, setSelectedAccoutnId] = useState(-1);
    const [selected, setSelected] = useState(-1);

    const navigation = useNavigation()

    const txnsList = useRef(null);
    const config = {
        velocityThreshold: 0.3,
        directionalOffsetThreshold: 80,
    };


    const { getTxnByMonth, getClosingBalance, getOpeningBalance, deleteTransaction } = useTransaction()
    const { getAccountById, updateBalance, deleteReminderById } = useAccount()

    const { downloadOption, setDownloadOption, overDue, setoverDue, showSuccesskhata, setShowSuccesskhata, openMore, setOpenMore, modalView, setModalView, setRefreshData, refreshData, referesh, setReferesh } = useContext(createcontext)
    const closeDownloadOption = () => setDownloadOption(!downloadOption);

    const accountObj = getAccountById(id)
    const account = accountObj ? accountObj.name : ''
    const phone = accountObj ? accountObj.phone : ''
    const balance = accountObj ? accountObj.balance : 0

    const cancelPress = () => {
        try {
            deleteReminderById(id)
        } catch (error) {
            console.log(error)
        }
    }


    useEffect(() => {
        writeStore({
            accountId: id,
        });

        return () => {
            writeStore({
                accountId: null,
            });
        };
    }, [id]);

    useEffect(() => {
        writeStore({
            accountDate: dateview ? dateview : new Date(),
        });

        return () => {
            writeStore({
                accountDate: new Date(),
            });
        };
    }, [dateview]);

    useEffect(() => {
        setTimeout(() => {
            setShowSuccesskhata(false);
        }, 3000);

        try {
            auth.fetchUser().then(res => {
                if (res.businessName !== '') {
                    setBusinessName(res.businessName);
                }
            });

            const account = getAccountById(id);

            if (overDue == account.date) {
                Alert.alert('Payment Overdue', 'you want to send a reminder?', [
                    {
                        text: 'Cancel',
                        onPress: () => cancelPress(),
                        style: 'cancel',
                    },
                    {
                        text: 'OK',
                        onPress: () => {
                            handlePressonWhatsApp();
                        },
                    },
                ]);
            }
        } catch (error) {
            console.log(error);
        }
    }, [referesh, refreshData]);

    const newTxn = () => {
        setModalView(!modalView)
    }

    const updateTxns = () => {
        setRefreshData(!refreshData)
    }

    const longPress = (id, aId) => {
        setActionSheet(true);
        setSelected(id);
        setSelectedAccoutnId(aId);
        setSelectTxn(tnx.find(txn => txn.objectId == id));
    }

    const renderItem = (item, index) => {
        let amount = null;
        let detail = null;
        let date = null;
        if (item.item.cancelled) {
            amount = <Text style={ApplicationStyles.txnAmountCancel}>
                {accounting.formatMoney(Math.abs(item.item.amount), '₹', 0)}
            </Text>;
            detail = <Text style={{
                ...ApplicationStyles.detailCancel,
                marginLeft: item.item.amount.toString().length > 2 ? 15 : 22
            }}>{item.item.detail}</Text>
            date = <Text style={{ ...ApplicationStyles.dateRowCancel }}>{moment(item.item.createdAt).format('DD MMM')}</Text>
        } else {
            amount = <Text style={{ ...ApplicationStyles.txnAmount, color: Colors.black }}>
                {accounting.formatMoney(Math.abs(item.item.amount), '₹', 0)}
            </Text>;
            detail = <Text style={{
                ...ApplicationStyles.detail,
                marginLeft: item.item.amount.toString().length > 2 ? 15 : 22,
                color: Colors.black
            }}>{item.item.detail}</Text>;
            date = <Text style={{ ...ApplicationStyles.dateRow, marginRight: -3, color: Colors.black }}>{moment(item.item.date).format('DD MMM')}</Text>
        }

        return (
            <>
                <TouchableHighlight
                    underlayColor={Colors.underlayColor}
                    style={{ padding: Metrics.baseMargin }}
                    onLongPress={() => {
                        !item.item.cancelled && longPress(item.item.objectId, item.item.accountId)
                    }}>
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <View style={{ flex: 1, flexDirection: "row", alignItems: 'center' }}>
                            {amount}
                            {detail}
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-end' }}>
                            {date}
                        </View>
                    </View>
                </TouchableHighlight>
                {!item.item.cancelled ?
                    <TouchableOpacity
                        underlayColor={Colors.underlayColor}
                        onPress={() => navigation.navigate('View Image', { url: item.item.attachment })}
                        style={{ marginLeft: 30 }}>
                        {item.item.attachment ? <Image source={{ uri: item.attachment }}
                            style={{ width: 100, height: 100, borderRadius: 10 }} /> : null
                        }
                    </TouchableOpacity> : null
                }
            </>
        );
    }

    const renderSeparator = () => {
        return (
            <View style={ApplicationStyles.separator} />
        );
    }

    const handleDownload = async (value) => {
        const endMonth = new Date();
        let startMonth = new Date();
        startMonth.setHours(0, 0, 0, 0);
        if (value === 1) {
            startMonth.setDate(1);

        } else if (value === "lifetime") {
            startMonth = "lifetime";
        } else {
            startMonth.setMonth(endMonth.getMonth() - value);
        }
        await api.downloadReport(id, startMonth, endMonth).then((res) => {
            // console.log(res)
            setDownloadLoading(false)
        }).catch((err) => {
            console.log(err)
            setDownloadLoading(false)
        })
    }

    const DeleteTransaction = () => {
        try {
            deleteTransaction(selected)
            updateBalance(selectedAccoutnId)
            updateTxns();
            setReferesh(!referesh)
            setRefreshData(!refreshData)
        } catch (error) {
            console.log(error);
        }
    }

    const actionItems = [
        {
            id: 1,
            label: 'Edit',
            onPress: () => {
                navigation.navigate('Edit Transaction', { id: JSON.parse(JSON.stringify(selectTxn)) });

            }
        },
        {
            id: 2,
            label: 'Delete',
            onPress: DeleteTransaction
        }
    ];

    const actionItemsForMore = [
        {
            id: 1,
            label: 'Set Reminder Date',
            onPress: () => {
                navigation.navigate('Set Reminder', { id: id });

            }
        },
        {
            id: 2,
            label: 'Send Reminder',
            onPress: () => {
                DeleteTransaction();

            }
        }
    ];

    const downloadChoice = [
        {
            id: 1,
            label: 'Current Month',
            onPress: () => {
                setDownloadLoading(true)
                handleDownload(1);
            }
        },

        {
            id: 2,
            label: '3 Months',
            onPress: () => {
                setDownloadLoading(true)
                handleDownload(3);
            }
        },
        {
            id: 3,
            label: '6 Months',
            onPress: () => {
                setDownloadLoading(true)
                handleDownload(6);

            }
        },
        {
            id: 4,
            label: '1 Year',
            onPress: () => {
                setDownloadLoading(true)
                handleDownload(12);

            }
        },
        {
            id: 5,
            label: 'Lifetime',
            onPress: () => {
                setDownloadLoading(true)
                handleDownload("lifetime");
            }
        }

    ];

    const dismissMenuofDonwload = () => {
        setDownloadOption(!downloadOption)
    }

    const dissmissContextualMenu = () => {
        setSelected(-1);
        setSelectedAccoutnId(-1);
        setSelectTxn({});
        setActionSheet(false);
    }

    const showPicker = () => {
        setShow(true);
    }

    const enableRightSwipe = useMemo(() => {
        const newDate = moment(date).add(1, 'month').toDate();
        return moment(newDate).isBefore(moment())
    }, [date])

    const onSwipeLeft = () => {
        const newDate = moment(date).add(1, 'month').toDate();
        if (moment(newDate).isBefore(moment())) {
            setDate(newDate);
            setReferesh(!referesh)
            txnsList.current?.bounceInRight(); // Use current property to access the ref
        }
    }

    const onSwipeRight = () => {
        const newDate = moment(date).subtract(1, 'month').toDate();
        setDate(newDate);
        setReferesh(!referesh)
        txnsList.current?.bounceInLeft(); // Use current property to access the ref
    }

    const dismissMoreMenu = () => {
        setOpenMore(!openMore)
    }

    const handlePressonWhatsApp = () => {
        let phonenumber = phone;

        if (phone == "" || phone == null || phone == undefined) {
            ToastAndroid.show('Phone Number not found', ToastAndroid.SHORT)
            return
        }

        phonenumber = phonenumber.replace(/\s/g, '');
        let line1 = null;
        let line2 = null;
        if (balance < 0) {
            line1 = "Payment Reminder : ";
            line2 = `Balance *₹${-balance}* due to ${BusinessName}`;
        } else {
            line1 = "Payment details : ";
            line2 = `Balance *₹${balance}* advance to ${BusinessName}`;
        }
        const link = 'Ledger : https://web.bahikhata.org/p/' + id;
        const line3 = `Sent via BahiKhata App`;

        const whatsappLink = `whatsapp://send?phone=${phonenumber}&text=${encodeURIComponent(`${line1}\n${line2}\n${link}\n${line3}`)}`;
        Linking.openURL(whatsappLink).then(() => {
            console.log("WhatsApp opened");
        }).catch(err => {
            console.error("An error occurred", err);
        });
    }

    return (
        <>
            <SafeAreaView style={styles.container}>
                <AppHeader
                    title={khatapageTopName}
                    back
                    onSearchPress={() => { }}
                    headerBg={'#c13229'}
                    iconColor={'#fff'}
                    onBackPress={() => navigation.goBack()}
                    optionalBtn="more-vertical"
                    phone={phone}
                    userId={id}
                    downloadBtn="file-download"
                    businessName={BusinessName}
                    handleOpenWhatsApp={() => handlePressonWhatsApp()}
                />

                <GestureRecognizer
                    onSwipeLeft={onSwipeLeft}
                    onSwipeRight={onSwipeRight}
                    config={config}
                    style={{ flex: 1 }}
                >
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 15, backgroundColor: Colors.snow }}>
                        <TouchableOpacity onPress={() => onSwipeRight()}>
                            <AntIcon
                                name='left'
                                size={Metrics.icons.small} // Assuming Metrics.icons.small is 24
                                color={Colors.primary} // Assuming Colors.primary is Colors.primary 
                                style={{ marginRight: Metrics.baseMargin }} // Assuming Metrics.baseMargin is 8
                            />
                        </TouchableOpacity>

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <TouchableOpacity onPress={() => showPicker()}>
                                <AntIcon
                                    name='calendar'
                                    size={24}
                                    color={Colors.primary}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => showPicker()}>
                                <Text style={{ fontSize: 16, color: Colors.primary, marginLeft: 4 }}>
                                    {moment(date).format('MMM')}
                                </Text>
                            </TouchableOpacity>
                        </View>

                        <TouchableOpacity onPress={() => onSwipeLeft()}>
                            <AntIcon
                                name='right'
                                size={24}
                                color={enableRightSwipe ? Colors.primary : Colors.lightPrimary} // Assuming Colors.lightPrimary is '#ADD8E6'
                                style={{ marginLeft: 8 }}
                            />
                        </TouchableOpacity>

                        {show && (
                            <MonthPicker
                                onChange={(event, newDate) => {
                                    setShow(false);
                                    setDate(newDate || date);
                                    setReferesh(!referesh);
                                }}
                                value={date}
                                maximumDate={new Date()}
                                locale="en"
                            />
                        )}
                    </View>
                    <Animatable.View
                        ref={txnsList}
                        style={styles.container1}>
                        <ImageBackground
                            style={styles.imageBackground}
                            resizeMode='stretch'
                            source={Images.background}>
                            <View
                                style={{ flex: 0.5 }}>
                                <View style={{...styles.listHeader, backgroundColor: Colors.snow}}>
                                    <Icon
                                        name='arrow-circle-down'
                                        size={Metrics.icons.small}
                                        color={Colors.darkGreen}
                                        style={styles.listIcon}
                                    />
                                    <Text style={styles.headerTextRecive}>
                                        Receive
                                    </Text>
                                </View>
                                <FlatList
                                    showsVerticalScrollIndicator={false}
                                    showsHorizontalScrollIndicator={false}
                                    style={{ flex: 0.5 }}
                                    data={cashIn}
                                    renderItem={renderItem}
                                    ItemSeparatorComponent={renderSeparator}
                                    ListHeaderComponent={() => <OpenBalance OpenBalance={openAmount} give={cashIn} />}
                                    keyExtractor={(item) => item.objectId.toString()}
                                    ListFooterComponent={renderSeparator}
                                />
                            </View>

                            <View
                                style={{ flex: 0.5 }}>

                                <View style={{ ...styles.listHeader, paddingLeft: 50, backgroundColor: Colors.snow }}>
                                    <Icon
                                        name='arrow-circle-up'
                                        size={Metrics.icons.small}
                                        color={Colors.primary}
                                        style={styles.icon}
                                    />
                                    <Text style={styles.headerTextGive}>
                                        Give
                                    </Text>
                                </View>
                                <FlatList
                                    showsVerticalScrollIndicator={false}
                                    showsHorizontalScrollIndicator={false}
                                    style={{ flex: 0.9 }}
                                    data={cashOut}
                                    renderItem={renderItem}
                                    ItemSeparatorComponent={renderSeparator}
                                    keyExtractor={(item) => item.objectId.toString()}
                                    ListFooterComponent={() => <CloseBalance CloseBalance={closeAmount} receive={cashOut} />}
                                    ListHeaderComponent={Separator}
                                />
                            </View>


                        </ImageBackground>
                        <Text style={ApplicationStyles.balance}>
                            <Text style={{ color: Colors.black, fontSize: 18 }}>Balance </Text>
                            <Text style={balance >= 0 ? { fontSize: 16, color: Colors.darkGreen } : { fontSize: 16, color: Colors.fire }}>
                                {accounting.formatMoney(Math.abs(balance), '₹ ', 0)}
                            </Text>
                        </Text>

                        <TransitionModal
                            updateTxns={updateTxns}
                            account={account}
                            id={id}
                            phone={phone}
                        />
                        <FloatingAction
                            color={Colors.primary}
                            onPressMain={newTxn}
                            showBackground={false}
                            position="right"
                            distanceToEdge={20}
                            floatingIcon={<Icon name="pencil" color={Colors.snow} size={22} />}
                        />

                        {
                            showSuccesskhata && <View
                                style={{
                                    flex: 0.1,
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    position: 'absolute',
                                    alignItems: 'center',
                                    top: "50%",
                                    left: "45%",
                                }}
                            >
                                <Svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="green" class="bi bi-patch-check-fill" viewBox="0 0 16 16">
                                    <Path d="M10.067.87a2.89 2.89 0 0 0-4.134 0l-.622.638-.89-.011a2.89 2.89 0 0 0-2.924 2.924l.01.89-.636.622a2.89 2.89 0 0 0 0 4.134l.637.622-.011.89a2.89 2.89 0 0 0 2.924 2.924l.89-.01.622.636a2.89 2.89 0 0 0 4.134 0l.622-.637.89.011a2.89 2.89 0 0 0 2.924-2.924l-.01-.89.636-.622a2.89 2.89 0 0 0 0-4.134l-.637-.622.011-.89a2.89 2.89 0 0 0-2.924-2.924l-.89.01zm.287 5.984-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7 8.793l2.646-2.647a.5.5 0 0 1 .708.708" />
                                </Svg>
                            </View>
                        }
                        <Modal
                            isVisible={actionSheet}
                            style={{ margin: 0, justifyContent: 'flex-end' }}
                            onBackdropPress={dissmissContextualMenu}>
                            <ActionSheet
                                actionItems={actionItems}
                                onCancel={closeActionSheet}
                                dissmiss={dissmissContextualMenu}
                            />
                        </Modal>
                        <Modal
                            isVisible={downloadOption}
                            style={{ margin: 0, justifyContent: 'flex-end' }}
                            onBackdropPress={dismissMenuofDonwload}>
                            <ActionSheet
                                actionItems={downloadChoice}
                                onCancel={closeDownloadOption}
                                dissmiss={dismissMenuofDonwload}
                            />
                        </Modal>
                        <Modal
                            isVisible={openMore}
                            style={{ margin: 0, justifyContent: 'flex-end' }}
                            onBackdropPress={dismissMoreMenu}>
                            <ActionSheet
                                actionItems={actionItemsForMore}
                                onCancel={() => setOpenMore(!openMore)}
                                dissmiss={dismissMoreMenu}
                            />
                        </Modal>
                        {downloadLoading ? (
                            <View style={{ flex: 1, position: 'absolute', top: "45%", left: "45%" }}>
                                <ActivityIndicator size="large" color="red" />
                            </View>
                        ) : null}
                    </Animatable.View>
                </GestureRecognizer>
            </SafeAreaView>
        </>
    )
}

export default AccountDetail;


const styles = StyleSheet.create({
    imageBackground: {
        width: '100%',
        flex: 1,
        flexDirection: 'row'
    },
    listHeader: {
        flex: 0.05,
        flexDirection: 'row',
        backgroundColor: Colors.snow,
        padding: Metrics.baseMargin / 2,
        justifyContent: 'center',
        alignSelf: 'center',
        textAlign: 'center',
        borderBottomWidth: 1,
        borderBottomColor: Colors.darkPrimary,
    },


    listIcon: {
        flex: 0.4,
        textAlign: 'right',
        paddingRight: Metrics.baseMargin
    },
    calendarMenu: {
        flex: 0.06,
        textAlignVertical: 'center',
        marginRight: Metrics.baseMargin
    },
    dateContainer: {
        width: '100%',
        backgroundColor: Colors.snow,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignSelf: 'center'
    },
    date: {
        flex: 0.17,
        fontSize: Fonts.size.h4,
        color: Colors.primary,
        justifyContent: 'center',
        textAlign: 'center',
        alignSelf: 'center',
        fontStyle: 'italic',
        textAlignVertical: 'center'
    },
    container: {
        flex: 1,
        width: '100%',
        resizeMode: 'stretch'
    },

    container1: {
        flex: 1,
        flexDirection: 'column',
        width: '100%',
        resizeMode: 'cover'

    },
    headerView: {
        flex: 1,
        backgroundColor: Colors.snow,
        flexDirection: 'row',
        alignItems: 'flex-end',
        alignSelf: 'center',
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 30,
        borderBottomWidth: 0.5,
        borderBottomColor: Colors.primary
    },
    headerTextRecive: {
        flex: 0.75,
        paddingLeft: 2,
        fontSize: 16,
        color: Colors.darkGreen,
        fontWeight: 'bold',
    },
    headerTextGive: {
        flex: 0.85,
        paddingLeft: 5,
        fontSize: 16,
        color: Colors.primary,
        fontWeight: 'bold',
    },
    icon: {
        flex: 0.15
    },
    camera: {
        flex: 0.25,
        padding: Metrics.smallMargin,
        marginRight: Metrics.smallMargin,
        marginTop: Metrics.doubleBaseMargin,
        width: Metrics.images.placeholder + 4,
        height: Metrics.images.placeholder + 4,
        borderColor: Colors.primary,
        borderWidth: 0.5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    attachment: {
        width: Metrics.images.placeholder,
        height: Metrics.images.placeholder,
        resizeMode: 'stretch'
    },
});
