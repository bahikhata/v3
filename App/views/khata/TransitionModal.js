import React, { useContext, useEffect, useMemo, useState } from 'react';
import {
    ToastAndroid, StyleSheet, Text, View, Image,
    TouchableHighlight, TextInput, ScrollView, TouchableOpacity
} from 'react-native';
const accounting = require('accounting');
import moment from 'moment';
import DatePicker from '@react-native-community/datetimepicker';
import CheckBox from '@react-native-community/checkbox';

const validate = require("validate.js");
import { api, sms, mixpanel, sendPushNotification } from 'services';
import Modal from 'react-native-modal';
import createcontext from 'context/Createcontext';
import { Colors, Metrics, Images, ApplicationStyles, Fonts } from 'themes';
import Icon from "react-native-vector-icons/FontAwesome";
// import { launchImageLibrary } from 'react-native-image-picker';
// import { launchCamera } from 'react-native-image-picker';
import { ActivityIndicator } from 'react-native-paper';
import Sound from 'react-native-sound';
import { IconButton } from 'components';
import { useTransaction } from 'hooks/useTransaction';
import { useAccount } from 'hooks/useAccount';
import { SmsCheckbox } from '../../components/SmsCheckbox';

const option = {
    symbol: '₹',
    decimal: '.',
    thousand: ',',
    precision: 0,
    format: '%s%v',
};

const TransitionModal = ({ updateTxns, account, id, phone }) => {
    const [showTpicker, setShowTpicker] = useState(false)
    const [tranDate, setTranDate] = useState(new Date())
    const [amount, setAmount] = useState("")
    const [detail, setDetail] = useState("")
    const [amountError, setAmountError] = useState("");
    const [actionSheet, setActionSheet] = useState(false);
    const closeActionSheet = () => setActionSheet(false);
    const [selectedImage, setSelectedImage] = useState(null);
    const [loading, setLoading] = useState(false);
    const [showKeyboard, setShowKeyboard] = useState('always')
    const { updateBalance, getAccountById } = useAccount()
    const accountData = getAccountById(id);
    const [sendSmsCheckbox, setSendSmsCheckbox] = useState(accountData.checkboxSMS);


    useEffect(() => {
        setSendSmsCheckbox(accountData?.checkboxSMS)
    }, [accountData?.checkboxSMS])

    const { addTransaction, getCloseAmount } = useTransaction()

    const { deviceId, showSuccesskhata, setShowSuccesskhata, referesh, setReferesh, modalView, setModalView } = useContext(createcontext)
    const dismissModal = () => {
        setModalView(!modalView)
        setAmount("")
        setDetail("")
        setTranDate(new Date())
        setAmountError("")
        setSendSmsCheckbox(accountData.checkboxSMS)
    }
    const datePickerTransition = () => {
        setShowTpicker(true)
    }

    const playSound = () => {
        const newSound = new Sound("cash_add.mp3", Sound.MAIN_BUNDLE, (error) => {
            if (error) {
                console.log('Error loading sound:', error);
                return;
            }
            newSound.play((success) => {
                if (success) {
                    console.log('Sound played successfully');
                } else {
                    console.log('Playback failed due to audio decoding errors');
                }
            });
        });
    };

    const onTxnDone = multiplier => {
        setLoading(true);
        const amountUnformatted = accounting.unformat(amount).toString();
        setShowKeyboard('handled');
        mixpanel.track('Transaction Started');
        let constraints;
        const closeBalance = getCloseAmount(new Date());

        if (multiplier < 0) {
            constraints = {
                amount: {
                    presence: {
                        message: 'required',
                    },
                    // numericality: {
                    //   lessThanOrEqualTo: closeBalance == null ? 0 : closeBalance,
                    //   message: 'Not Enough Cash',
                    // },
                    format: {
                        pattern: /^[0-9]+$/,
                        message: 'invalidNo',
                    },
                },
            };
        } else {
            constraints = {
                amount: {
                    presence: {
                        message: 'required',
                    },
                    format: {
                        pattern: /^[0-9]+$/,
                        message: 'invalidNo',
                    },
                },
            };
        }
        const error = validate({ amount: amountUnformatted }, constraints);
        console.log({ error })
        
        if (!error) {
            const result = addTransaction(
                account,
                amountUnformatted,
                detail,
                selectedImage,
                tranDate,
                multiplier,
                id,
            );
            if (result.data) {
                updateBalance(id);
                ToastAndroid.show('Transaction Done', ToastAndroid.SHORT);
                mixpanel.track('Transaction Done');
                setLoading(false);
                updateTxns();
                setShowSuccesskhata(!showSuccesskhata);
                playSound();
                dismissModal();
                setReferesh(!referesh);

                if (sendSmsCheckbox && phone != null) {
                    let phoneNo = phone.replace(/\s/g, '').slice(-10);
                    phoneNo = '+91' + phoneNo;
                    if (multiplier > 0) {
                        sms
                            .sendReceiveSMS(amountUnformatted, phoneNo, id)
                            .then(result => {
                                console.log(result)
                            });
                    } else {
                        sms
                            .sendPaidSMS(amountUnformatted, phoneNo, id)
                            .then(result => {
                                console.log(result);
                            });

                    }
                    let message =
                        multiplier > 0
                            ? `You have received ₹${amountUnformatted} from ${account}`
                            : `You have paid ₹${amountUnformatted} to ${account}`;

                    sendPushNotification(message, deviceId)
                        .then(result => {
                            console.log("Push notification sent:", result);
                        })
                        .catch(error => {
                            console.error('Error sending push notification:', error);
                            throw error;
                        });
                }
            } else {
                setLoading(false);
                mixpanel.track('Transaction Failed');
                ToastAndroid.show('Transaction Failed', ToastAndroid.SHORT);
            }
            console.log(sendSmsCheckbox, amountUnformatted, phone, id);
        } else {
            setAmountError(error.amount[0]);
            setLoading(false);
        }
    };

    const dissmissContextualMenu = () => {
        closeActionSheet()
    }

    const toggleModal = () => {
        setModalView(!modalView)
        setAmount("")
        setDetail("")
        setTranDate(new Date())
        setAmountError("")
    };
    // const openImagePicker = () => {
    //     const options = {
    //         mediaType: 'photo',
    //         includeBase64: true,
    //         maxHeight: 2000,
    //         maxWidth: 2000,
    //         quality: 0.5
    //     };

    //     launchImageLibrary(options, (response) => {
    //         if (response.didCancel) {
    //             console.log('User cancelled image picker');
    //         } else if (response.error) {
    //             console.log('Image picker error: ', response.error);
    //         } else {
    //             let imageUri = response.uri || response.assets?.[0]?.uri;
    //             setSelectedImage(imageUri);
    //         }
    //     });
    // };
    // const handleCameraLaunch = () => {
    //     const options = {
    //         mediaType: 'photo',
    //         includeBase64: true,
    //         maxHeight: 2000,
    //         maxWidth: 2000,
    //         quality: 0.5
    //     };

    //     launchCamera(options, response => {
    //         if (response.didCancel) {
    //             console.log('User cancelled camera');
    //         } else if (response.error) {
    //             console.log('Camera Error: ', response.error);
    //         } else {
    //             // Process the captured image
    //             let imageUri = response.uri || response.assets?.[0]?.uri;
    //             setSelectedImage(imageUri);
    //         }
    //     });
    // }
    const handleCheckbox = () => {
        if (phone == null) {
            ToastAndroid.show('Phone Number Not Found', ToastAndroid.SHORT);
            setSendSmsCheckbox(false)
            return
        }
        setSendSmsCheckbox(x => !x)
    }

    // const actionItems = [
    //     {
    //         id: 1,
    //         label: 'Camera',
    //         onPress: () => {
    //             handleCameraLaunch();
    //         }
    //     },
    //     {
    //         id: 2,
    //         label: 'Gallery',
    //         onPress: () => {
    //             openImagePicker();
    //         }
    //     },


    // ];

    return (
        <>
            <Modal
                isVisible={modalView}
                onBackdropPress={toggleModal}
                swipeDirection={['down']}
                style={styles.bottomModal}
                onRequestClose={dismissModal}
            >
                <TouchableHighlight
                    style={styles.spacer}
                    onPress={() => dismissModal()}
                >
                    <Text></Text>
                </TouchableHighlight>
                <ScrollView style={styles.modalView} keyboardShouldPersistTaps={showKeyboard}>
                    {showTpicker && (
                        <DatePicker
                            testID="dateTimePicker"
                            value={tranDate}
                            mode='date'
                            is24Hour={true}
                            display="default"
                            onChange={(event, selectedDate) => {
                                const currentDate = selectedDate || tranDate;
                                setShowTpicker(Platform.OS === 'ios');
                                setTranDate(currentDate);
                            }}
                            maximumDate={new Date()}

                        />
                    )}
                    <View style={styles.buttonGroup}>
                        <View style={styles.inButton}>
                            <IconButton
                                iconName='arrow-circle-down'
                                disabled={!amount || loading}
                                text={'Receive'}
                                color={Colors.darkPrimary}
                                onPress={() => onTxnDone(1)}
                                btn={"Add"}
                            />
                        </View>
                        <View style={styles.outButton}>
                            <IconButton
                                iconName='arrow-circle-up'
                                disabled={!amount || loading}
                                text={'Give'}
                                color={Colors.darkPrimary}
                                onPress={() => onTxnDone(-1)}
                                btn={"Remove"}
                            />
                        </View>
                    </View>
                    <View style={styles.form}>
                        <View style={styles.formRow}>
                            <View style={styles.subRow}>
                                <TextInput
                                    style={styles.inputControlAmunt}
                                    placeholder={'Amount'}
                                    keyboardType='numeric'
                                    spellCheck={false}
                                    autoCorrect={false}
                                    maxLength={11}
                                    autoFocus
                                    value={amount}
                                    selectionColor={Colors.primary}
                                    placeholderTextColor={Colors.charcoal}
                                    underlineColorAndroid={Colors.transparent}
                                    onChangeText={(text) => {
                                        let val;
                                        if (text === '₹') {
                                            val = '';
                                        } else {
                                            val = accounting.formatMoney(text, option);
                                        }
                                        setAmount(val);
                                        setAmountError(null);
                                    }}
                                />



                                <View style={styles.dateView}>

                                    <Icon
                                        name='calendar'
                                        size={Metrics.icons.small}
                                        color={Colors.primary}
                                        style={{
                                            flex: 0.8,
                                        }}
                                        onPress={() => datePickerTransition()}
                                    />

                                    <Text
                                        style={styles.dateText}
                                        onPress={() => datePickerTransition()}
                                    >
                                        {moment(tranDate).format('DD MMM')}
                                    </Text>
                                </View>
                            </View>
                            {amountError ? (
                                <Text style={ApplicationStyles.inputError}>{amountError}</Text>
                            ) : null}

                            <View style={{
                                flex: 1, flexDirection: 'row', alignItems: 'center',
                            }} >
                                <View
                                    style={{ flex: 0.98 }}
                                >
                                    <TextInput
                                        style={ApplicationStyles.inputControl}
                                        placeholder={'Detail'}
                                        multiline
                                        spellCheck={false}
                                        autoCorrect={false}
                                        keyboardType='default'
                                        maxLength={80}
                                        value={detail}
                                        selectionColor={Colors.black}
                                        placeholderTextColor={Colors.charcoal}
                                        underlineColorAndroid={Colors.transparent}
                                        onChangeText={
                                            (text) => {
                                                setDetail(text)
                                            }
                                        }
                                    />
                                </View>
                                {/* <View style={{ ...styles.attachImage }}>
                                    {selectedImage && (
                                        <Image
                                            source={{ uri: selectedImage }}
                                            style={{ flex: 1, height: 50, width: 50, marginLeft: 10 }}
                                            resizeMode="contain"
                                        />
                                    )}
                                    <View style={{ flex: 1, flexDirection: 'row', marginLeft: 10 }}>

                                        <TouchableOpacity
                                            onPress={() => setActionSheet(true)}
                                            style={{ marginRight: Metrics.baseMargin }}
                                        >
                                            <Icon
                                                name='paperclip'
                                                size={Metrics.icons.small}
                                                color={Colors.primary}
                                                style={styles.calculatorIcon}
                                            />
                                        </TouchableOpacity>

                                    </View>
                                </View> */}
                            </View>
                            {/* </View> */}
                            <View style={{ marginTop: 10, flex: 0.1, flexDirection: 'row', alignItems: 'center', marginLeft: Metrics.baseMargin }}>
                                <SmsCheckbox value={sendSmsCheckbox} onValueChange={handleCheckbox} />
                            </View>

                            {loading && <View style={{
                                flex: 1, justifyContent: 'center', alignItems: 'center',
                                marginTop: Metrics.doubleBaseMargin
                            }}>
                                <ActivityIndicator size="large"
                                    color={Colors.primary}
                                />
                            </View>}

                        </View>
                        {/* <Modal
                            isVisible={actionSheet}
                            style={{
                                margin: 0,
                                justifyContent: 'flex-end'
                            }}
                            onBackdropPress={dissmissContextualMenu}
                        >
                            <ActionSheet
                                actionItems={actionItems}
                                onCancel={closeActionSheet}
                                dissmiss={dissmissContextualMenu}
                            />
                        </Modal> */}
                    </View>
                </ScrollView>
            </Modal>
        </>
    )
}

export default TransitionModal;

const styles = StyleSheet.create({
    inputControlAmunt: {
        flex: 0.7,
        fontSize: Fonts.size.normal,
        margin: Metrics.baseMargin,
        borderBottomColor: Colors.fire,
        borderBottomWidth: 0.5
    },

    outButton: {
        flex: 0.5,
        marginLeft: Metrics.smallMargin,
        marginRight: Metrics.smallMargin,
        // backgroundColor:Colors.primary
    },
    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
    },
    spacer: {
        flex: 1,
        height: '100%',
        backgroundColor: Colors.underlayColor
    }, buttonGroup: {
        flex: 0.2,
        flexDirection: 'row',
        marginTop: Metrics.baseMargin
    },
    attachImage: {
        flex: 0.3,
        flexDirection: 'row',
        alignContent: 'center',
        alignItems: 'center',
        // marginLeft: Metrics.baseMargin,
    },
    modalView: {
        flex: 1,
        width: '100%',
        resizeMode: 'stretch',
        flex: 0.2, height: '60%',
        alignSelf: 'flex-start',
        backgroundColor: Colors.snow,
        borderColor: Colors.primary,
        borderTopWidth: 1,
        position: 'absolute',
        bottom: 0
    }, inButton: {
        flex: 0.5,
        marginLeft: Metrics.smallMargin,
        marginRight: Metrics.smallMargin,
        // backgroundColor:Colors.darkGreen
    }, form: {
        flex: 0.1,
        flexDirection: 'row'
    },
    formRow: {
        flex: 1,
        flexDirection: 'column'
    },
    subRow: {
        flex: 1,
        flexDirection: 'row'
    }, amountInput: {
        flex: 0.7,
        fontSize: Fonts.size.normal,
        margin: Metrics.baseMargin,
        borderBottomColor: Colors.fire,
        borderBottomWidth: 0.5
    }, dateText: {
        fontSize: 18,
        paddingLeft: 8,
        color: Colors.hairline,
        fontWeight: "bold"
    }, dateView: {
        flex: 0.3,
        flexDirection: 'row',
        padding: 10,
        marginTop: 10
    },
})
