import React, { useContext, useEffect, useState } from 'react';
import { useRoute } from '@react-navigation/native';
import DatePicker from '@react-native-community/datetimepicker';

import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  Button,
  TouchableOpacity,
} from 'react-native';
import { Colors, Metrics, Images, ApplicationStyles } from 'themes';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/AntDesign';

import moment from 'moment';
const _ = require('lodash');
import createcontext from 'context/Createcontext';
import { ActivityIndicator } from 'react-native-paper';
import { useAccount } from '../hooks/useAccount';

export default function SetReminder({ navigation }) {
  const route = useRoute();
  const id = route.params.id;
  const { addReminder, getAccountById, deleteReminderById } = useAccount();

  const [date, setDate] = useState(new Date(Date.now() + 24 * 60 * 60 * 1000));
  const [show, setShow] = useState(false);
  const [loding, setLoding] = useState(false);
  const [cancelView, setCancelView] = useState(false);
  const { reminderRefresh, setReminderRefresh } = useContext(createcontext);

  const onPressButton = () => {
    navigation.goBack();
  };

  useEffect(() => {
    try {
      const res = getAccountById(id);
      console.log('reminder => ', res);
      if (res.date) {
        setDate(new Date(res.date));
        setCancelView(true);
      }
    } catch (err) {
      console.log(err);
    }
  }, [id]);

  const onPressButtonSchedule = () => {
    setLoding(true);
    let dates = moment(date).format('YYYY-MM-DD');
    try {
      addReminder(id, dates, true);
      setLoding(false);
      setReminderRefresh(!reminderRefresh);
      navigation.goBack();
    } catch (err) {
      setLoding(false);
      console.error(err);
    }
  };

  const deleteReminder = () => {
    setLoding(true);
    try {
      deleteReminderById(id);
      setLoding(false);
      setReminderRefresh(!reminderRefresh);
      navigation.goBack();
    } catch (err) {
      setLoding(false);

      console.error(err);
    }
  };

  const EditView = () => {
    return (
      <>
        <View style={styles.view}>
          <Text style={styles.textView}>Current Reminder</Text>
        </View>
        <View style={styles.view}>
          <Text style={{ ...ApplicationStyles.inputControl }}>
            {moment(new Date(date.getTime())).format(
              'MMM Do YYYY',
            )}
          </Text>
          <TouchableOpacity onPress={() => setShow(true)}>
            <Icon1
              name="delete"
              size={Metrics.icons.small}
              color={Colors.primary}
              style={styles.switch}
              onPress={() => deleteReminder()}
            />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => setShow(true)}>
            <Icon
              name="edit"
              size={Metrics.icons.small}
              color={Colors.primary}
              style={styles.switch}
              onPress={() => setCancelView(!cancelView)}
            />
          </TouchableOpacity>
        </View>
      </>
    );
  };

  const InitialView = () => {
    return (
      <>
        <View style={styles.view}>
          <Text style={styles.textView}>When do you want to be Reminded</Text>
        </View>
        <View style={styles.view}>
          <Text
            style={ApplicationStyles.inputControl}
            onPress={() => setShow(true)}>
            {moment(new Date(date.getTime())).format('MMM Do YYYY')}
          </Text>
          <TouchableOpacity onPress={() => setShow(true)}>
            <Icon
              name="calendar"
              size={Metrics.icons.small}
              color={Colors.primary}
              style={styles.calendarMenu}
              onPress={() => {
                setShow(true);
              }}
            />
          </TouchableOpacity>
        </View>

        {show && (
          <DatePicker
            testID="dateTimePicker"
            value={date}
            mode="date"
            is24Hour={true}
            display="default"
            onChange={(event, selectedDate) => {
              const currentDate = selectedDate || date;
              setShow(Platform.OS === 'ios');
              setDate(currentDate);
            }}
            minimumDate={new Date(new Date().getTime() + 24 * 60 * 60 * 1000)}
          />
        )}
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            marginTop: 5,
            paddingLeft: 10,
            paddingRight: 10,
          }}>
          <View style={ApplicationStyles.buttonWrapper}>
            <Button
              title={'Cancel'}
              color={Colors.primary}
              onPress={onPressButton}
            />
          </View>

          <View style={ApplicationStyles.buttonWrapper}>
            <Button
              title={'Schedule'}
              color={Colors.primary}
              onPress={onPressButtonSchedule}
            />
          </View>
        </View>
      </>
    );
  };

  return (
    <ScrollView
      style={ApplicationStyles.container}
      keyboardShouldPersistTaps="always">
      {cancelView ? <EditView /> : <InitialView />}

      {loding && (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: Metrics.doubleBaseMargin,
          }}>
          <ActivityIndicator size="large" color={Colors.primary} />
        </View>
      )}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  imageView: {
    margin: Metrics.doubleSection,
    alignItems: 'center',
    alignContent: 'center',
  },
  calendarMenu: {
    marginRight: 15,
  },
  switch: {
    marginRight: 15,
    fontSize: 23,
    width: 30,
  },

  image: {
    width: Metrics.images.placeholder,
    height: Metrics.images.placeholder,
    resizeMode: 'contain',
  },
  view: {
    margin: Metrics.baseMargin,
    marginTop: 10,
    marginBottom: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textView: {
    color: Colors.charcoal,
    fontSize: 16,
    fontStyle: 'normal',
    margin: Metrics.baseMargin,
  },
});
