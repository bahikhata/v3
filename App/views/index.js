import SplashScreen from './SplashScreen';

import Home from './home';
import Khata from './khata';

import OpenSource from './home/settings/OpenSource';

import Dummy from './Dummy';

import Contact from './Contact';

import ViewImage from './ViewImage';
import Auth from './auth';

import SetReminder from './SetReminder';

export {
  SplashScreen,

  Home,
  Khata,
  
  OpenSource,
  
  Contact,
  ViewImage,
  Auth,
  Dummy,
  SetReminder,
}
