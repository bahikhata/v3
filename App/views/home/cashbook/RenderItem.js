import React from 'react';
import { Text, TouchableHighlight, TouchableOpacity, View } from 'react-native';
import { ApplicationStyles, Colors, Metrics, Fonts } from 'themes';

const RenderItem = ({ item, longPress, viewAccount }) => {
  return (
    <View>
      <TouchableHighlight
        underlayColor={Colors.underlayColor}
        onLongPress={() => {
          !item.cancelled && item.accountId
            ? longPress(item.objectId, item.accountId)
            : !item.cancelled
              ? longPress(item.objectId)
              : null;
        }}
        onPress={() => {
          if (!item.cancelled && item.accountId) {
            console.log('item.accountId', item.accountId)
            viewAccount(item.accountId, item.date)
          }
        }}
        style={{
          padding: Metrics.baseMargin,
          overflow: 'hidden',
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={
                item.cancelled
                  ? ApplicationStyles.amountCancel
                  : { ...ApplicationStyles.amount, fontSize: 12 }
              }>
              {item.amountText}
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'flex-start',
            }}>
            <Text
              style={
                item.cancelled
                  ? {
                    color: Colors.hairline,
                    fontSize: Fonts.size.medium,
                    textDecorationLine: 'line-through',
                    marginLeft: Metrics.doubleBaseMargin,
                    fontWeight: '700'
                  }
                  : {
                    ...ApplicationStyles.accountText,
                    marginLeft: item.amount.toString().length > 2 ? -9 : 3,
                    fontWeight: '700'
                  }
              }
              lineBreakMode='tail'
              ellipsizeMode='tail'
              numberOfLines={2}
            >
              {item.accountName}
            </Text>
            {item.detail && (
              <Text
              lineBreakMode='tail'
              ellipsizeMode='tail'
              numberOfLines={5}
                style={
                  item.cancelled
                    ? {
                      ...ApplicationStyles.detailCancel,
                    }
                    : {
                      ...ApplicationStyles.detailText,
                      marginLeft: item.amount.toString().length > 2 ? -9 : 3,
                    }
                }>
                {item.detail}
              </Text>
            )}
          </View>
        </View>
      </TouchableHighlight>
      {!item.cancelled && item.attachment ? (
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('View Image', {
              url: item.attachment,
            })
          }>
          <Image
            resizeMode={'cover'}
            style={{ width: 100, height: 100, borderRadius: 10, marginLeft: 30 }}
            source={{ uri: item.attachment }}
          />
        </TouchableOpacity>
      ) : null}
    </View>
  );
};

export default RenderItem;
