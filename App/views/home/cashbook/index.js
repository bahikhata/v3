import React, { useCallback, useContext, useEffect, useMemo, useRef, useState } from 'react';
import {
  StyleSheet, Text, View, FlatList,
  ImageBackground, StatusBar, ToastAndroid, TouchableOpacity,
  LogBox, BackHandler, Platform,
} from 'react-native';
import moment from 'moment';
import Svg, { Path } from 'react-native-svg';
import Icon from 'react-native-vector-icons/FontAwesome';
import AntIcon from 'react-native-vector-icons/AntDesign';
import GestureRecognizer from 'react-native-swipe-gestures';
import * as Animatable from 'react-native-animatable';
const accounting = require('accounting');
import { FloatingAction } from "react-native-floating-action";
import { Colors, Metrics, Images, ApplicationStyles, Fonts } from 'themes';
import createcontext from 'context/Createcontext';
import DatePicker from '@react-native-community/datetimepicker'
import { ActionSheet, CloseBalance, OpenBalance } from 'components';
import Modal from 'react-native-modal';
import { useFocusEffect } from '@react-navigation/native';
import RenderItem from './RenderItem';
// const _ = require('lodash');

import { useTransaction } from 'hooks/useTransaction';
import { useAccount } from 'hooks/useAccount';
import { useStore } from 'store';
import { showAlert } from '../../../components/Alert';

const _ = require('lodash');
LogBox.ignoreLogs(['Animated: `useNativeDriver`'])

const Cashbook = ({ navigation }) => {
  const write = useStore(state => state.write)

  const date = useStore(state => state.date)
  const setDate = useCallback((date) => {
    write({ date })
  }, [])

  const [selected, setSelected] = useState(-1);
  const [selectedAccoutnId, setSelectedAccoutnId] = useState(-1);
  const [selectTxn, setSelectTxn] = useState({});
  const [show, setShow] = useState(false);
  const cashIn = useStore(state => state.cashIn)
  const cashOut = useStore(state => state.cashOut)
  const tnxs = useStore(state => state.tnxs)
  const closeamount = useStore(state => state.closeAmount);
  const openamount = useStore(state => state.openAmount);

  const txnsList = useRef(null);
  const [referesh, setReferesh] = useState(false);
  const [actionSheet, setActionSheet] = useState(false);
  const closeActionSheet = () => setActionSheet(false);
  const { deleteTransaction } = useTransaction();
  const { getAccountById, updateBalance } = useAccount()

  const config = {
    velocityThreshold: 0.3,
    directionalOffsetThreshold: 80,
  };

  const { editRefresh, setRefreshData, refreshData, showSuccess, setShowSuccess } = useContext(createcontext)

  useEffect(() => {
    setTimeout(() => {
      setShowSuccess(false)
    }, 3000);
  }, [refreshData, referesh, editRefresh]);

  const cashInTransformed = useMemo(() => {
    return cashIn.map(txn => {
      const { name } = getAccountById(txn.accountId) || {};
      const amount = accounting.formatMoney(txn.amount < 0 ? txn.amount * -1 : txn.amount, '₹', 0);

      txn.amountText = amount;
      txn.accountName = name;

      return txn
    })
  }, [cashIn])

  const cashOutTransformed = useMemo(() => {
    return cashOut.map(txn => {
      const { name } = getAccountById(txn.accountId) || {};
      const amount = accounting.formatMoney(txn.amount < 0 ? txn.amount * -1 : txn.amount, '₹', 0);

      txn.amountText = amount;
      txn.accountName = name;

      return txn
    })
  }, [cashOut])


  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        BackHandler.exitApp();
        return true;
      };
      BackHandler.addEventListener('hardwareBackPress', onBackPress);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [])
  );

  const enableLeftDateBtn = useMemo(() => {
    const today = new Date();
    const newDate = moment(date).add(1, 'days').toDate();
    return newDate <= today;
  }, [date])



  const handlePress = () => {
    navigation.navigate('New Transaction');
  };



  const onSwipeLeft = () => {
    const today = new Date();
    const newDate = moment(date).add(1, 'days').toDate();
    if (newDate <= today) {
      setDate(newDate);
      setReferesh(!referesh)
    }
    txnsList.current?.bounceInRight(); // Use current property to access the ref
  }

  const onSwipeRight = () => {
    const newDate = moment(date).subtract(1, 'days').toDate();
    setDate(newDate);
    setReferesh(!referesh)
    txnsList.current?.bounceInLeft(); // Use current property to access the ref
  }

  const longPress = (id, aId) => {
    setActionSheet(true);
    setSelected(id);
    setSelectedAccoutnId(aId);
    setSelectTxn(tnxs.find(txn => txn.objectId == id));
  }

  const dissmissContextualMenu = () => {
    setSelected(-1);
    setSelectedAccoutnId(-1);
    setSelectTxn({});
    setActionSheet(false);
  }

  const renderSeparator = () => {
    return (
      <View style={ApplicationStyles.separator} />
    );
  }


  const showPicker = () => {
    setShow(!show);
  }

  const viewAccount = (id, date) => {
    try {
      const res = getAccountById(id);
      if (res.cancelled) {
        ToastAndroid.show('Account is closed', ToastAndroid.SHORT);
        return;
      }
      write({
        khatapageTopName: res.name,
      });
      navigation.navigate('Single-Khata', { id: id, dateview: date });
    } catch (err) {
      console.error(err);
    }
  };

  const DeleteTransaction = async () => {
    try {
      const deleteConfirm = await showAlert('Delete Transaction', 'Are you sure you want to delete this transaction?');
      if (!deleteConfirm) return;

      deleteTransaction(selected)
      updateBalance(selectedAccoutnId)
      setReferesh(!referesh)
      setRefreshData(!refreshData)
    } catch (error) {
      console.error(error);
    }
  }

  const actionItems = [
    {
      id: 1,
      label: 'Edit',
      onPress: () => {
        navigation.navigate('Edit Transaction', { id: JSON.parse(JSON.stringify(selectTxn)) });
      }
    },
    {
      id: 2,
      label: 'Delete',
      onPress: () => {
        DeleteTransaction();
      }
    },
  ];

  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor={Colors.primary} />
      <GestureRecognizer
        onSwipeLeft={onSwipeLeft}
        onSwipeRight={onSwipeRight}
        config={config}
        style={{ flex: 1 }}
      >
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 15, backgroundColor: Colors.snow }}>
          <TouchableOpacity onPress={() => onSwipeRight()}>
            <AntIcon
              name='left'
              size={24} // Assuming Metrics.icons.small is 24
              color={Colors.primary}
            />
          </TouchableOpacity>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => showPicker()}>
              <AntIcon
                name='calendar'
                size={24}
                color={Colors.primary}
                style={{ marginRight: 8 }}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => showPicker()}>
              <Text style={{ fontSize: 16, color: Colors.primary }}>
                {moment(date).format('DD MMM')}
              </Text>
            </TouchableOpacity>
            {show && (
              <View style={{
                backgroundColor: 'white',
                borderRadius: 10,
                padding: 10,
              }}>
                <DatePicker
                  testID="dateTimePicker"
                  value={date}
                  mode="date"
                  is24Hour={true}
                  display="default"
                  onChange={(event, selectedDate) => {
                    const currentDate = selectedDate || date;
                    setShow(Platform.OS === 'ios');
                    setDate(currentDate);
                    setRefresh(!refresh);
                  }}
                  maximumDate={new Date()}
                />
              </View>
            )}
          </View>
          <TouchableOpacity onPress={() => onSwipeLeft()}>
            <AntIcon
              name='right'
              size={24}
              color={enableLeftDateBtn ? Colors.primary : Colors.lightPrimary}
            />
          </TouchableOpacity>
        </View>
        <Animatable.View
          ref={txnsList}
          style={styles.container}
        >
          <ImageBackground
            style={styles.imageBackground}
            resizeMode='stretch'
            source={Images.background}
          >
            <View
              style={{ flex: 0.5 }}>
              <View style={styles.listHeader}>
                <Icon
                  name='arrow-circle-down'
                  size={Metrics.icons.small}
                  color={Colors.darkGreen}
                  style={styles.listIcon}
                />
                <Text style={styles.listHeaderTitlein}>Cash In</Text>
              </View>
              <FlatList
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                style={{ flex: 0.5 }}
                data={cashInTransformed.length > 0 ? cashInTransformed : null}
                ItemSeparatorComponent={renderSeparator}
                renderItem={(args) => RenderItem({ ...args, longPress, viewAccount })}
                ListHeaderComponent={() => <OpenBalance OpenBalance={openamount} give={cashIn} />}
                keyExtractor={(item) => item.objectId}
                ListFooterComponent={() => <View style={ApplicationStyles.separator} />}
              />
            </View>
            <View
              style={{ flex: 0.5 }}>
              <View style={styles.listHeader}>
                <Icon
                  name='arrow-circle-up'
                  size={Metrics.icons.small}
                  color={Colors.darkPrimary}
                  style={styles.listIcon}
                />
                <Text style={styles.listHeaderTitleout}>Cash Out</Text>
              </View>
              <FlatList
                style={{ flex: 0.9 }}
                data={cashOutTransformed.length > 0 ? cashOutTransformed : null}
                renderItem={(args) => RenderItem({ ...args, longPress, viewAccount })}
                keyExtractor={(item) => item.objectId}
                ItemSeparatorComponent={renderSeparator}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                ListFooterComponent={() => <CloseBalance CloseBalance={closeamount} receive={cashOut} />}
              />
            </View>
            {
              showSuccess && <View
                style={{
                  flex: 0.1,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  position: 'absolute',
                  alignItems: 'center',
                  top: "50%",
                  left: "45%",
                }}
              >
                <Svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="green" class="bi bi-patch-check-fill" viewBox="0 0 16 16">
                  <Path d="M10.067.87a2.89 2.89 0 0 0-4.134 0l-.622.638-.89-.011a2.89 2.89 0 0 0-2.924 2.924l.01.89-.636.622a2.89 2.89 0 0 0 0 4.134l.637.622-.011.89a2.89 2.89 0 0 0 2.924 2.924l.89-.01.622.636a2.89 2.89 0 0 0 4.134 0l.622-.637.89.011a2.89 2.89 0 0 0 2.924-2.924l-.01-.89.636-.622a2.89 2.89 0 0 0 0-4.134l-.637-.622.011-.89a2.89 2.89 0 0 0-2.924-2.924l-.89.01zm.287 5.984-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7 8.793l2.646-2.647a.5.5 0 0 1 .708.708" />
                </Svg>
              </View>
            }
          </ImageBackground>

          <FloatingAction
            color={Colors.primary}
            onPressMain={handlePress}
            showBackground={false}
            position="right"
            distanceToEdge={20}
            floatingIcon={<Icon name="pencil" color={Colors.snow} size={22} />}
          />
          <Modal
            isVisible={actionSheet}
            style={{
              margin: 0,
              justifyContent: 'flex-end'
            }}
            onBackdropPress={dissmissContextualMenu}

          >
            <ActionSheet
              actionItems={actionItems}
              onCancel={closeActionSheet}
              dissmiss={dissmissContextualMenu}
            />
          </Modal>

        </Animatable.View>
      </GestureRecognizer>
    </>
  )
}

export default Cashbook;

const styles = StyleSheet.create({
  amountCancel: {
    color: Colors.hairline,
    fontSize: Fonts.size.medium,
    textDecorationLine: 'line-through',
  },
  detailText: {
    color: Colors.black,
    fontSize: Fonts.size.medium,
  },
  accountText: {
    color: Colors.black,
    fontSize: Fonts.size.medium,
    fontWeight: 'bold'
  },
  detailCancel: {
    color: Colors.hairline,
    fontSize: Fonts.size.medium,
    textDecorationLine: 'line-through',
    marginLeft: Metrics.doubleBaseMargin
  },
  accountCancel: {
    color: Colors.hairline,
    fontSize: Fonts.size.medium,
    textDecorationLine: 'line-through',
    marginLeft: Metrics.doubleBaseMargin
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    width: '100%',
    resizeMode: 'cover'
  },
  imageBackground: {
    width: '100%',
    flex: 1,
    flexDirection: 'row',
    // backgroundColor:Colors.snow
  },
  calendarMenu: {
    flex: 0.07,
    textAlignVertical: 'center'
  },
  date: {
    flex: 0.25,
    fontSize: Fonts.size.h4,
    color: Colors.primary,
    justifyContent: 'center',
    textAlign: 'center',
    alignSelf: 'center',
    fontStyle: 'italic',
    textAlignVertical: 'center'
  },
  listHeader: {
    flex: 0.05,
    flexDirection: 'row',
    backgroundColor: Colors.snow,
    padding: Metrics.baseMargin / 2,
    justifyContent: 'center',
    alignSelf: 'center',
    textAlign: 'center',
    borderBottomWidth: 1,
    borderBottomColor: Colors.darkPrimary,
  },
  listIcon: {
    flex: 0.4,
    textAlign: 'right',
    paddingRight: Metrics.baseMargin
  },
  listHeaderTitlein: {
    flex: 0.6,
    color: Colors.darkGreen,
    fontSize: 14,
    // fontStyle: 'italic',
    fontWeight: 'bold'
  },
  listHeaderTitleout: {
    flex: 0.6,
    color: Colors.primary,
    fontSize: 14,
    fontWeight: 'bold',
  },
  actionsheet: {
    showSeparators: true,
    borderRadius: 20,
  }
});