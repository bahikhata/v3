import React, { useState, useEffect } from 'react';
import {
  Text, View, TextInput, ScrollView, StyleSheet, Button,
  Image, PermissionsAndroid, FlatList, TouchableHighlight, ImageBackground
} from 'react-native';
import { ApplicationStyles, Colors, Metrics, Images } from 'themes';
import { iconsMap, iconsLoaded } from 'app-icons';
import moment from 'moment';
const accounting = require('accounting');
//const I18n = require('../I18n');
//let SmsAndroid = require('react-native-android-sms');
const _ = require('lodash');
import { api } from 'services';
import { Separator } from 'components';

let filter = {
    box: 'inbox', // 'inbox' (default), 'sent', 'draft', 'outbox', 'failed', 'queued', and '' for all
    // the next 4 filters should NOT be used together, they are OR-ed so pick one
    //read: 0 /*, // 0 for unread SMS, 1 for SMS already read
    //_id: 1234, // specify the msg id
    address: 'BX-ATMSBI' //'IM-SBIUPI' //ATM-SBI // sender's phone number
    //body: 'Loans4sme' // content to match
    // the next 2 filters can be used for pagination
    //indexFrom: 0, // start from index 0
    //maxCount: 10, // count of SMS to return each time*/
};

const Passbook = () => {
  const [txns, setTxns] = useState([]);
  useEffect(() => {
    /*iconsLoaded.then(() => {
      this.initialMenu();
    });*/
  })

  const requestSMSPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_SMS, null
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        /*SmsAndroid.list(JSON.stringify(filter), (fail) => {
            console.log("OH Snap: " + fail);
        }, (count, smsList) => {
            this.setState({
              txns: JSON.parse(smsList)
            });
            /*for (let i = 0; i < this.state.txns.length; i++) {
                this.parseSMS(this.state.txns[i]);
            }
        });*/
      } else {
        console.log("SMS Read permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  const parseSMS = (sms) => {
    //find Rs in sms... and
    let body = sms.body;
    let date = sms.date;

  }

  const Row = (rowData) => {
    let msg = rowData.item.body.split('Rs')[1];
    if(msg) {
      let body = msg.split('on');
      return (
        <TouchableHighlight underlayColor={Colors.underlayColor} key={rowData.index}>
          <View style={ApplicationStyles.row}>
              <Text style={ApplicationStyles.txnAmount}>
                {accounting.formatMoney(body[0], '₹', 0)}
              </Text>
              <View style={ApplicationStyles.subrow}>
                  <Text style={ApplicationStyles.detail}>{body[1].split('.')[0].trim().split('at')[1]}</Text>
                  <Text style={ApplicationStyles.dateRow}>
                    {moment(new Date(rowData.item.date)).format('DD MMM YY')}
                  </Text>
              </View>
          </View>
      </TouchableHighlight>
      );
    }
  }

  return (
    <ImageBackground style={styles.container} source={Images.background} resizeMode='stretch'>
        <View style={ApplicationStyles.accListWrapper}>
          <FlatList
            style={{ flex: 0.5 }}
            data={txns.length > 0 ? txns : null}
            renderItem={({item}) => <Row item={item} />}
            keyExtractor={item => item._id}
            ItemSeparatorComponent={() => <Separator />}
          />
          <FlatList
            style={{ flex: 0.5 }}
            data={txns.length > 0 ? txns : null}
            renderItem={({item}) => <Row item={item} /> }
            keyExtractor={item => item._id.toString()}
            ItemSeparatorComponent={() => <Separator />}
          />
        </View>
      </ImageBackground>
  )
}

export default Passbook;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    resizeMode: 'stretch'
  },
  text: {
    color: Colors.black
  }
});
