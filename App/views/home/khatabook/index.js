import React, { useEffect, useState, useContext, useCallback, useMemo } from 'react';
import {
  Text,
  View,
  TouchableHighlight,
  FlatList,
  ToastAndroid,
  StyleSheet,
  ImageBackground,
  ActivityIndicator,
  LogBox,
  BackHandler
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Colors, Metrics, Images, ApplicationStyles } from 'themes';
import { FloatingAction } from "react-native-floating-action";
const _ = require('lodash');
const accounting = require('accounting');
import SafeAreaView from 'react-native-safe-area-view';
import Modal from 'react-native-modal';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import SearchBar from 'react-native-searchbar';

import {
  AppHeader, ActionSheet, CustomAlert
} from 'components';
import createcontext from 'context/Createcontext';
import { useStore } from '../../../store';
import { useAccount } from '../../../hooks/useAccount';
import { showAlert } from '../../../components/Alert';
LogBox.ignoreLogs(['Animated: `useNativeDriver`'])

const Khatabook = () => {
  const navigation = useNavigation();
  const [selected, setSelected] = useState(-1);
  const [searchBar, setSearchBar] = useState(false);
  const [filteredContacts, setFilteredContacts] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [sort, setSort] = useState(); // 'name' or 'amount' or undefined
  const [sortOrder, setSortOrder] = useState('asc'); // 'asc' or 'desc'
  const [actionSheet1, setActionSheet1] = useState(false);
  const closeActionSheet1 = () => setActionSheet1(false);
  const [actionSheet2, setActionSheet2] = useState(false);
  const closeActionSheet2 = () => setActionSheet2(false);
  const [loading, setLoading] = useState(true);
  const [actionItems2, setActionItems2] = useState([])
  const [reopen, setReopen] = useState({ state: false, id: 0 });
  const { filterClick, setFilterClick, hideCloseAccount, setHideCloseAccount } =
    useContext(createcontext);
  const { getAccountById, reOpenAccount: reOpenAccountRealm } = useAccount();

  const accounts = useStore(state => state.accounts);
  const { closeAccount: closeAccountFn } = useAccount();
  const writeStore = useStore(state => state.write);
  useFocusEffect(
    useCallback(() => {
      return () => {
        if (searchBar) {
          setSearchBar(false);
        }
      };
    }, [searchBar, setSearchBar]) // Add all dependencies here
  );

  useEffect(() => {
    const backAction = () => {
      if (searchBar) {
        setSearchBar(false)
        return true;
      }
      return false;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );
    return () => backHandler.remove();
  }, []);

  const viewAccount = (id) => {
    if (selected > 0) {
      dissmissContextualMenu();
    }
    else {
      try {
        const res = getAccountById(id)
        // check = res.cancelled;
        if (res.cancelled) {
          ToastAndroid.show('Account is closed', ToastAndroid.SHORT);
          return;
        }

        writeStore({ khatapageTopName: res.name })
        console.log("Khata")
        navigation.navigate('Single-Khata', { id: id });
      } catch (error) {
        console.log(error);
      }
    }
  }

  const close = (id) => {
    const res = getAccountById(id)
    if (res.balance === 0) {
      closeAccountFn(id);
      ToastAndroid.show('Account closed successfully', ToastAndroid.SHORT);
    } else {
      ToastAndroid.show('Balance is not zero', ToastAndroid.SHORT);
    }
    dissmissContextualMenu();
  }

  const dissmissContextualMenu = () => {
    setSelected(-1);
    setActionSheet1(false);
    setActionSheet2(false);
  }

  const dissmissfilter = () => {
    setActionSheet1(false);
    setFilterClick(!filterClick)

  }

  const reOpenAccount = () => {
    const id = reopen.id;
    const res = getAccountById(id);
    if (res.balance === 0) {
      reOpenAccountRealm(id);
      setReopen({ state: false, id: 0 });
      ToastAndroid.show('Account reopened successfully', ToastAndroid.SHORT);
    } else {
      ToastAndroid.show('Balance is not zero', ToastAndroid.SHORT);
    }

    dissmissContextualMenu();
  };

  useEffect(() => {
    setFilteredContacts(() => {
      let accountsList = accounts.filter(account => {
        return account.name.toLowerCase().includes(searchQuery.toLowerCase());
      });

      if (sort === 'amount' && sortOrder === 'asc') {
        accountsList = _.sortBy(accountsList, [
          account => parseInt(account.balance),
        ]);
      } else if (sort === 'amount' && sortOrder === 'desc') {
        accountsList = _.sortBy(accountsList, [
          account => parseInt(account.balance),
        ]).reverse();
      }

      if (sort === 'name' && sortOrder === 'asc') {
        accountsList = _.sortBy(accountsList, [
          account => account.name.toLowerCase(),
        ]);
      } else if (sort === 'name' && sortOrder === 'desc') {
        accountsList = _.sortBy(accountsList, [
          account => account.name.toLowerCase(),
        ]).reverse();
      }
      return accountsList;
    });
    console.log('sort', sort, sortOrder);
    setLoading(false);
  }, [accounts, searchQuery, sort, sortOrder]);

  useEffect(() => {
    if (!searchBar) {
      setSearchQuery('');
    }
  }, [searchBar])

  const longPress = (id, balance) => {
    setSelected(id);
    setActionSheet2(true);
    if (balance === 0) {
      setActionItems2([
        {
          id: 1,
          label: 'Edit Account',
          onPress: () => {
            navigation.navigate('Edit Account', { id: id });
            dissmissContextualMenu();
          }
        },
        {
          id: 2,
          label: 'Close Account',
          onPress: async () => {
            const closeConfirm = await showAlert('Close Account', 'Are you sure you want to close this account?');
            if (closeConfirm) {
              close(id);
            }
            dissmissContextualMenu();
          }
        },
      ])

    }
    else {
      setActionItems2([
        {
          id: 1,
          label: 'Edit Account',
          onPress: () => {
            navigation.navigate('Edit Account', { id: id });
            dissmissContextualMenu();
          }
        },
      ])
    }
  }

  const renderItem = ({ item }) => {
    let account = null;
    let amount = null;
    if (item.cancelled) {
      account = <Text style={{ ...ApplicationStyles.accountCancel, marginLeft: 10 }}>{item.name}</Text>;
    } else {
      account = (
        <Text style={[ApplicationStyles.account, { fontSize: 16, marginLeft: 10 }]}>{item.name}</Text>
      );
    }

    if (item.cancelled) {
      amount = (
        <Text style={ApplicationStyles.amountCancelKhatabook}>
          {accounting.formatMoney(Math.abs(item.balance), '₹', 0)}
        </Text>
      );
    } else {
      amount = (
        <Text
          style={[
            ApplicationStyles.amoutkhatbook,
            item.balance >= 0
              ? { color: Colors.darkGreen, fontSize: 14, fontWeight: 'bold' }
              : { color: Colors.fire, fontSize: 14, fontWeight: 'bold' },
          ]}
        >
          {accounting.formatMoney(Math.abs(item.balance), '₹', 0)}
        </Text>
      );

    }


    return (
      <>
        {
          !hideCloseAccount && item.cancelled ? null :
            <TouchableHighlight
              underlayColor={Colors.underlayColor}
              key={item.objectId}
              onPress={() => viewAccount(item.objectId)}
              onLongPress={() =>
                !item.cancelled ? longPress(item.objectId, item.balance) : setReopen({ state: true, id: item.objectId })
              }

            >

              <View
                style={[
                  ApplicationStyles.rowAccount,
                  selected === item.objectId
                    ? {
                      backgroundColor: Colors.lightPrimary,
                      paddingTop: Metrics.middleBaseMargin,
                      paddingBottom: Metrics.middleBaseMargin,
                    }
                    : {
                      backgroundColor: 'transparent',
                      paddingTop: Metrics.middleBaseMargin,
                      paddingBottom: Metrics.middleBaseMargin,
                    },
                ]}
              >
                {account}
                {amount}
              </View>

            </TouchableHighlight>

        }
        {
          !hideCloseAccount && !item.cancelled ? <View style={ApplicationStyles.separator} /> : hideCloseAccount ? <View style={ApplicationStyles.separator} /> : null
        }



      </>
    );
  };


  const handleclose = () => {
    setHideCloseAccount(!hideCloseAccount);
  }


  const handlefilter = () => {
    setActionSheet1(true);
    setFilterClick(!filterClick)
  }

  const handleopen = () => {
    setSearchBar(false);
    navigation.navigate('ContactPicker', { accounts: accounts });
  }

  const actionItems1 = [
    {
      id: 1,
      label: 'Name',
      onPress: () => {
        setSort('name');
        setSortOrder(s => {
          if (s === 'asc' && sort === 'name') {
            return 'desc';
          } else {
            return 'asc';
          }
        });
      },
    },
    {
      id: 2,
      label: 'Amount',
      onPress: () => {
        setSort('amount');
        setSortOrder(s => {
          if (s === 'asc' && sort === 'amount') {
            return 'desc';
          } else {
            return 'asc';
          }
        });
      },
    },
  ];

  return (
    <SafeAreaView style={styles.container}>
      <AppHeader style={styles.app}
        title={"Khata"}
        headerBg={'#c13229'}
        iconColor={'white'}
        navigation={navigation}
        right="search"
        filterbtn={handlefilter}
        closebtn={handleclose}
        onSearchPress={() => setSearchBar(!searchBar)} />
      {
        searchBar ? (
          <SearchBar
            data={accounts}
            platform={Platform.OS === 'ios' ? 'ios' : 'android'}
            showOnLoad
            onHide={() => setSearchBar(false)}
            onBackPress={() => setSearchBar(false)}
            allDataOnEmptySearch
            iOSPadding={false}
            iOSHideShadow={true}
            placeholder={'Search Account'}
            handleChangeText={(txt) => setSearchQuery(txt)}
            onEndEditing={() => {
              setSearchBar(true);
            }}
            onX={() => setSearchBar(false)}

          />
        ) : null
      }

      <ImageBackground
        style={{ flex: 1 }}
        source={Images.background}
        resizeMode='cover'>
        {loading ? (
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <ActivityIndicator size="large" color="red" />
          </View>
        ) : accounts.length > 0 ? (
          <FlatList
            style={{
              flex: 1,
              marginTop: searchBar ? 10 : 0,
            }}
            data={filteredContacts}
            renderItem={renderItem}
            keyExtractor={(item) => item.objectId.toString()}
          />
        ) : (
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>No Account Found</Text>
          </View>
        )}
      </ImageBackground>
      <FloatingAction
        color={Colors.primary}
        onPressMain={handleopen}
        showBackground={false}
        position="right"
        distanceToEdge={20}
        floatingIcon={<Icon name="user-plus" color={Colors.snow} size={22} />}
      />

      <Modal
        isVisible={actionSheet2}
        style={{
          margin: 0,
          justifyContent: 'flex-end'
        }}
        onBackdropPress={dissmissContextualMenu}
      >
        <ActionSheet
          actionItems={actionItems2}
          onCancel={closeActionSheet2}
          dissmiss={dissmissContextualMenu}
          filterClick={filterClick}
        />
      </Modal>
      <Modal
        isVisible={actionSheet1}
        style={{
          margin: 0,
          justifyContent: 'flex-end'
        }}
        onBackdropPress={dissmissfilter}
      >
        <ActionSheet
          actionItems={actionItems1}
          onCancel={closeActionSheet1}
          dissmiss={dissmissfilter}
          filterClick={filterClick}
          nameasc={sortOrder === 'asc' && sort === 'name'}
          amountasc={sortOrder === 'asc' && sort === 'amount'}
        />
      </Modal>
      <CustomAlert
        visible={reopen.state}
        onClose={() => setReopen({ state: false, id: 0 })}
        onConfirm={reOpenAccount}
        Title={"Reopen Account"}
      />
    </SafeAreaView>
  )
}

export default Khatabook;

const styles = StyleSheet.create({
  imageBack: {
    flex: 1,
    width: '100%'
  },
  app: {
    paddingRight: 0
  },
  container: {
    flex: 1,
    backgroundColor: 'white'
  },

});
