import React, { useContext, useEffect, useState } from 'react';
import { StyleSheet, View, Text, Alert, Linking, Share, Image, AppRegistry, ActivityIndicator, Switch, Platform, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { CustomButton, CustomAlert } from 'components';
import { Colors, Metrics, Images, Fonts, ApplicationStyles } from 'themes';
import { auth } from 'services';
import { iconsMap, iconsLoaded } from 'app-icons';
import { version, name } from '../../../../package.json';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { realm } from '../../../services/realm.schema'
import { useStore } from '../../../store';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import RNFS from 'react-native-fs';
import DocumentPicker from 'react-native-document-picker';
import ShareExt from 'react-native-share';

const Setting = ({ navigation }) => {
  const BusinessName = useStore(state => state.user?.businessName);
  const phoneNumber = useStore(state => state.user?.phone);
  const formattedPhoneNumber = phoneNumber ? phoneNumber.slice(-10) : '';
  const writeStore = useStore(state => state.write);
  const cashbookEnabled = useStore(store => store.cashbookEnabled);

  const [loading, setLoading] = useState(false);
  const [showAlert, setShowAlert] = useState(false);
  const handleCancel = () => {
    setShowAlert(false);
  };

  const handleConfirm = async () => {
    setLoading(true);
    try {
      await AsyncStorage.clear();
      await AsyncStorage.removeItem('sessionToken');

      writeStore({
        user: null,
        accounts: [],
        tnxs: [],
        cashIn: [],
        cashOut: [],
        date: new Date(),
        khatapageTopName: '',
        openAmount: 0,
        closeAmount: 0,
      });
      setLoading(false);
      navigation.reset({
        index: 0,
        routes: [{ name: 'Login' }],
      });
      realm.write(() => {
        realm.deleteAll();
      });

      setShowAlert(false);
    } catch (error) {
      console.error('Error logging out:', error);
      setLoading(false);
    }

    console.log('OK Pressed');
  };

  const editName = () => {
    navigation.navigate("Edit Name", { name: BusinessName });
  }

  const openShare = () => {
    Share.share({
      message: "Share Message",
      title: "Share Title"
    });
  }

  const openHelp = () => {
    let url = 'http://api.whatsapp.com/send?phone=+917456883325 &text=Hello.. I need help with Bahi Khata app.'
    Linking.openURL(url).then((data) => {
      console.log('WhatsApp Opened');
    }
    ).catch(() => {
      console.log('WhatsApp not installed');
    });
  }

  const openSource = () => {
    navigation.navigate('OpenSource');
  }

  const exportTransactions = async () => {
    try {
      const transactions = realm.objects('Transaction')
      const accounts = realm.objects('Account')
      const data = {
        transactions,
        accounts
      }

      const path = Platform.OS === 'ios' 
        ? `${RNFS.DocumentDirectoryPath}/bahikhata_transactions.json`
        : `${RNFS.ExternalDirectoryPath}/bahikhata_transactions.json`;

      await RNFS.writeFile(path, JSON.stringify(data), 'utf8');

      await ShareExt.open({
        url: Platform.OS === 'ios' ? path : `file://${path}`,
        message: 'Bahi Khata Transactions',
        type: 'application/json',
      });
      Alert.alert('Export Successful', 'Transactions have been exported successfully.');
    } catch (error) {
      console.error('Error exporting transactions:', error);
      Alert.alert('Export Failed', 'An error occurred while exporting transactions.');
    }
  };

  const importTransactions = async () => {
    try {
      const result = await DocumentPicker.pick({
        type: [DocumentPicker.types.json],
      });

      const fileContents = await RNFS.readFile(result[0].uri, 'utf8');
      const data = JSON.parse(fileContents);
      const { transactions, accounts } = data;

      realm.write(() => {
        // Import accounts
        accounts.forEach(account => {
          realm.create('Account', account, 'modified');
        });

        // Import transactions
        transactions.forEach(transaction => {
          realm.create('Transaction', transaction, 'modified');
        });
      });

      Alert.alert('Import Successful', 'Transactions and accounts have been imported successfully.');
    } catch (error) {
      if (DocumentPicker.isCancel(error)) {
        // User cancelled the picker
      } else {
        console.error('Error importing transactions:', error);
        Alert.alert('Import Failed', 'An error occurred while importing transactions and accounts.');
      }
    }
  };

  const toggleCashbook = () => {
    writeStore({
      cashbookEnabled: !cashbookEnabled
    })
  }

  return (
    <View style={[ApplicationStyles.container, styles.container]}>
      <View style={styles.header}>
        <View >
          <Image source={Images.avatar} style={{ width: 50, height: 50, borderRadius: 50 }} />
        </View>
        <View style={styles.subheader}>
          <Text style={styles.name}>{BusinessName}</Text>
          <Text>{formattedPhoneNumber}</Text>
        </View>
        <Icon
          name='pencil'
          size={Metrics.icons.medium}
          color={Colors.black}
          style={styles.editButton}
          onPress={editName}
        />
      </View>
      <View style={styles.line} />

      <ScrollView style={styles.menu}>
        {/* <CustomButton  text="Profile" iconName="user-circle" onPress={openProfile} /> */}
        <CustomButton text="Daily Cash" subtitle={cashbookEnabled ? "Disable Daily Cash" : "Enable Daily Cash"} iconName={<MaterialCommunityIcons name="cash" size={Metrics.icons.small} color={Colors.hairline} />} onPress={toggleCashbook} trailElement={<Switch value={cashbookEnabled} onValueChange={toggleCashbook} trackColor={{true: Colors.lightPrimary, false: 'grey'}} thumbColor={cashbookEnabled ? Colors.primary : "#f4f3f4"} />} />
        <CustomButton text="Support" subtitle="Help" iconName="life-ring" onPress={openHelp} />
        <CustomButton text="Share" subtitle="WhatsApp,Facebook,Instagram" iconName="share-alt" onPress={openShare} />
        <CustomButton text="Open Source" subtitle="React Native Version" iconName="code" onPress={openSource} />
        <CustomButton text="Legal" subtitle="Policy " iconName="user-secret" onPress={() => Linking.openURL("http://www.bahikhata.org/privacy-policy")} />
        <CustomButton text="Export" subtitle="Save your transactions" iconName="upload" onPress={exportTransactions} />
        <CustomButton text="Import" subtitle="Load saved transactions" iconName="download" onPress={importTransactions} />
        <CustomButton text="Logout" subtitle="Sign Out" iconName="sign-out" onPress={() => setShowAlert(true)} />
        <CustomAlert visible={showAlert} Title="Logout" onClose={handleCancel} onConfirm={handleConfirm} />
       
        <Text style={styles.version}>
          App Version : {version}
        </Text>
      </ScrollView>

      {loading ? (
        <View style={{ flex: 0.6, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator size="large" color="red" />
        </View>
      ) : null}
    </View>
  )
}

export default Setting;

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.snow,
  },
  header: {
    flex: 0.15,
    flexDirection: 'row',
    marginBottom: Metrics.doubleBaseMargin,
    marginLeft: Metrics.doubleBaseMargin,
    alignItems: 'center',
  },
  subheader: {
    flex: 0.9,
    flexDirection: 'column',
    marginLeft: Metrics.doubleBaseMargin,
    marginBottom: Metrics.baseMargin,
    marginTop: Metrics.doubleBaseMargin
  },
  editButton: {
    // flex: 0,
    // margin: Metrics.doubleBaseMargin,
    textAlignVertical: 'bottom'
  },
  name: {
    ...Fonts.style.h5,
    flex: 0.9,
    color: Colors.black,
    textAlignVertical: 'bottom',
    marginBottom: Metrics.smallMargin
  },
  period: {
    flex: 0.2,
    fontSize: 12,
    fontStyle: 'italic',
    color: Colors.snow
  },
  menu: {
    flex: 0.55,
    flexDirection: 'column',
    borderRadius: 10,
    marginLeft: Metrics.baseMargin

  },
  menuItem: {
    flex: 0.333,
    flexDirection: 'column',
    alignItems: 'center',
    paddingBottom: Metrics.marginHorizontal,
    paddingTop: Metrics.doubleBaseMargin
  },
  menuButton: {
    paddingBottom: Metrics.marginHorizontal
  },
  menuText: {
    color: Colors.snow,
    fontSize: Fonts.size.regular
  },
  optionItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: Metrics.doubleSection,
    borderWidth: 0.2,
    borderColor: Colors.hairline
  },
  optionText: {
    color: Colors.black,
    fontSize: Fonts.size.regular,
    marginLeft: Metrics.doubleBaseMargin
  },
  submenuButtonView: {
    flex: 0.1,
    margin: Metrics.baseMargin,
    alignItems: 'center',
    borderWidth: 1
  },
  submenuButtonText: {
    fontSize: 16,
    fontWeight: 'bold',
    textAlignVertical: 'bottom'
  },
  version: {
    // paddingTop: 10,
    alignSelf: 'center',
    marginVertical: Metrics.doubleBaseMargin,
    color: Colors.hairline,
  }
  ,
  line: {
    height: Metrics.horizontalLineHeight,
    backgroundColor: Colors.hairline,
    opacity: 0.3,
  }
});


