import React from "react";
import { Text, StyleSheet, Image } from "react-native";

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Cashbook from "./cashbook";
import Khatabook from "./khatabook";
import Passbook from './passbook';
import Setting from './settings';
import { Colors, Metrics, Images, ApplicationStyles } from 'themes';
import { useStore } from "../../store";

const Tab = createBottomTabNavigator();

/*
<Tab.Screen name="Passbook" component={Passbook}
options={{

tabBarIcon: ({ color, size }) => (
<MaterialCommunityIcons name="notebook" color={color} size={27} />
 ),
}}
 />
 */
const Home = () => {
  const cashbookEnabled = useStore(s => s.cashbookEnabled)

  return (
    <Tab.Navigator
      initialRouteName={cashbookEnabled ? "Daily Cash" : "Khata"}
      screenOptions={{
        tabBarStyle: { backgroundColor: Colors.primary },
        tabBarActiveTintColor: Colors.snow,
        tabBarInactiveTintColor: Colors.whitish,
        headerStyle: {
          backgroundColor: Colors.primary,
        },
        headerTintColor: Colors.snow,
        headerTitleStyle: {
          fontWeight: 'bold',
          fontFamily: 'Cochin',
        }
      }}>

      {cashbookEnabled && <Tab.Screen name="Daily Cash" component={Cashbook}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="cash" color={color} size={35} />
          )
        }} />}

      <Tab.Screen name="Khata" component={Khatabook}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Icon name="users" color={color} size={size} />
          ), headerShown: false
        }} />

      <Tab.Screen name="Setting" component={Setting}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialIcons name="settings" color={color} size={27} />
          )
        }} />

    </Tab.Navigator>
  )
}

export default Home;
