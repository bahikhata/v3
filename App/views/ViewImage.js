import { View, Text, Image } from 'react-native';
import React from 'react';
import { useRoute } from '@react-navigation/native';

const ViewImage = () => {
  const route = useRoute();
  const { url } = route.params;

  return (
    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
        <Image source={{uri:url}} style={{width:200,height:'auto'}} resizeMode={'cover'}/>
    </View>
  )
}

export default ViewImage;