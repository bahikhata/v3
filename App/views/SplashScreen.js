import React, { useEffect } from 'react';
import { View, StyleSheet, Image, StatusBar } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
import { Images, Colors } from 'themes';
import { auth } from 'services';
import { useStore } from '../store';

export const CASHBOOK_KEY = "LOCAL_CASHBOOK_STATE"

const SplashScreen = () => {
  const navigation = useNavigation();
  const write = useStore(s => s.write);

  useEffect(() => {
    const checkIfLoggedIn = async () => {
      const userToken = await AsyncStorage.getItem('sessionToken');
      if (userToken) {

        const enabled = await AsyncStorage.getItem(CASHBOOK_KEY)
        if (enabled) write({ cashbookEnabled: true })
        else write({ cashbookEnabled: false })

        var user = await auth.fetchUser();
        if (user && user?.businessName) {
          navigation.navigate('Home');
        } else {
          navigation.navigate('Onboarding');
        }
      } else {
        navigation.navigate('Login');
      }
    }

    checkIfLoggedIn();
  }, []);

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={Colors.primary} barStyle="light-content" />

      <View style={{
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
      }}>
        <Image
          source={Images.logo}
          style={styles.logo}
          resizeMode="contain"
        />
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  logo: {
    width: 200,
    height: 200,
    //paddingRight: 10,
  }
});

export default SplashScreen;
