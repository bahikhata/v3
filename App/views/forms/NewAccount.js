import React, { useState, useContext } from 'react';
import {
  View,
  TextInput,
  ScrollView,
  StyleSheet,
  Button,
  Alert,
  TouchableOpacity
} from 'react-native';
import { ApplicationStyles, Colors, Metrics, Images } from 'themes';

import createcontext from 'context/Createcontext';
import { ToastAndroid } from 'react-native';
import { useAccount } from '../../hooks/useAccount';


const NewAccount = ({ navigation }) => {
  const { setRefreshData, refreshData } = useContext(createcontext);

  const [name, setName] = useState('');
  const [mobile, setMobile] = useState('');
  const { createAccount, checkAccount } = useAccount();

  const validateMobile = (mobile) => {
    const reg = /^[0]?[6789]\d{9}$/;
    if (reg.test(mobile) === false) {
      ToastAndroid.show('Invalid Mobile Number', ToastAndroid.SHORT);
      return false;
    } else {
      return true;
    }
  }

  const validateName = (name) => {
    const reg = /^[a-zA-Z ]{2,30}$/;
    if (reg.test(name) === false) {
      ToastAndroid.show('Invalid Name', ToastAndroid.SHORT);
      return false;
    } else {
      return true;
    }
  }


  const createContact = async () => {
    if (name === '') {
      ToastAndroid.show('Please fill Name fields', ToastAndroid.SHORT);
      return false;
    } else {
      if (validateName(name) || validateMobile(mobile)) {
        const response = checkAccount(name, `+91${mobile}`);
        console.log('response', response);

        if (response.length > 0) {
          ToastAndroid.show('Account already exists', ToastAndroid.SHORT);
          return false;
        } else {
          const res = createAccount(name, "+91" + mobile, 0);
          if (res) {
            ToastAndroid.show('Account created successfully', ToastAndroid.SHORT);
            setRefreshData(!refreshData);
            navigation.navigate('Khatabook');
          } else {
            ToastAndroid.show('Something went wrong', ToastAndroid.SHORT);
            return false;
          }
        }
      }
    }
  }

  return (
    <ScrollView style={styles.container}
      keyboardShouldPersistTaps='always'>
      <View style={styles.container}>
        <TextInput
          style={ApplicationStyles.inputControl}
          placeholder="Name"
          maxLength={20}
          value={name}
          selectionColor={Colors.black}
          placeholderTextColor={Colors.charcoal}
          underlineColorAndroid={Colors.transparent}
          onChangeText={(text) => setName(text)}
        />
        <TextInput
          style={ApplicationStyles.inputControl}
          maxLength={10}
          placeholder="Mobile Number"
          keyboardType="numeric"
          selectionColor={Colors.black}
          placeholderTextColor={Colors.charcoal}
          underlineColorAndroid={Colors.transparent}
          value={mobile}
          onChangeText={(text) => setMobile(text)}
        />
        <View style={ApplicationStyles.buttonWrapper}>
          <TouchableOpacity onPress={() => createContact()} style={{
            paddingHorizontal: 20,
            paddingVertical: 12,
            borderRadius: 3,
            backgroundColor: Colors.primary,
          }}>
            <Text style={{
              color: Colors.snow,
              width: "100%",
              textAlign: 'center'
            }}>
              ADD
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};



const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: Colors.whitish
  },
  button: {
    backgroundColor: Colors.primary,
    paddingHorizontal: 20,
    paddingVertical: 12,
    borderRadius: 10,
  }
});

export default NewAccount;
