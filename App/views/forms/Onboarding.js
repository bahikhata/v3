import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, ScrollView, StatusBar, StyleSheet, BackHandler } from 'react-native';
import { Colors, ApplicationStyles } from 'themes';
import { auth } from '../../services';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/AntDesign';
import { AppHeader } from 'components';
const _ = require('lodash');
import { Button } from "react-native-paper";

import SelectDropdown from 'react-native-select-dropdown';

const Onboarding = () => {
    const [data, setData] = useState({ name: "", error1: "", type:"", error2: "" });
    
    const navigation = useNavigation();

    // on press harddware back button dont go to previous screen
    useEffect(() => {
        const backAction = () => {
            return true;
        };
        const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            backAction
        );
        return () => backHandler.remove();
    }, []);

    const onPressButton = async () => {
      console.log(data);
      try {
        if (!data.type) {
          setData({...data, error2: 'Type of Business is required'});
          return;
        }
        if (!data.name) {
          setData({...data, error1: 'Business Name is required'});
          return;
        } else {
          setData({...data, error1: '', error2: ''});
          await auth.setBusinessName(data.name);
          await auth.setTypeOfBusiness(data.type);
          await auth.fetchUser(true)
          console.log('user info refreshed!');
          navigation.navigate('SyncScreen')
        }
      } catch (error) {
          console.log(error);
          navigation.navigate("Home")
      }
    };

    return (
        <ScrollView  keyboardShouldPersistTaps='always'>
            <StatusBar  backgroundColor={Colors.primary} barStyle="light-content" />
            <AppHeader
                title={('Welcome to Bahi Khata')}
                onSearchPress={() => { }}
                headerBg={'#c13229'}
                iconColor={'#fff'}
            />
            <View style={{ ...ApplicationStyles.inputControl, top: 10 }}>
                <TextInput
                    style={{ fontSize: 14 }}
                    placeholder={('Business Name')}
                    value={data.name}
                    maxLength={35}
                    autoFocus
                    selectionColor={Colors.black}
                    placeholderTextColor={Colors.hairline}
                    underlineColorAndroid={Colors.transparent}
                    onChangeText={(text) => setData({...data, name: text})}
                />
            </View>
            { data.error1 ?
                    <Text style={ApplicationStyles.inputError}>
                        {data.error1}
                    </Text> : null
            }
            <View style={ApplicationStyles.inputControl}>
                <SelectDropdown
                    data={["Trading", "Manufacturing", "Service"]}
                    onSelect={(selectedItem, index) => setData({...data, type: selectedItem})}
                    buttonTextAfterSelection={(selectedItem, index) => selectedItem}
                    rowTextForSelection={(item, index) => item}
                    defaultButtonText={('Select Type of Business ')}
                    buttonStyle={styles.dropdown3BtnStyle}
                    buttonTextStyle={data.type === "" ? styles.buttonstyle2 : styles.buttonstyle1}
                    dropdownStyle={styles.dropdown}
                    rowStyle={styles.dropdownText}
                    rowTextStyle={styles.rowtextStyle}
                />
                {/* down arro here  */}
                    <Icon name="down" size={20} color={Colors.hairline} style={{position:'absolute',right:15,top:15,fontSize:15}} />
            </View>
            {
                data.error2 ?
                    <Text style={ApplicationStyles.inputError}>
                        {data.error2}
                    </Text> : null
            }
            <View style={ApplicationStyles.buttonWrapper}>

                <Button
                    onPress={onPressButton}
                    buttonColor={Colors.darkPrimary}
                    rippleColor={Colors.primary}
                    uppercase
                    dark
                    compact
                    mode="contained"
                    style={{
                        padding: 7, marginTop: 30,
                        backgroundColor: Colors.primary,
                    }}>
                    <Text style={{ color: Colors.snow }}>
                        Get Started
                    </Text>
                </Button>
            </View>
        </ScrollView>
       
    );
}

export default Onboarding;

const styles = StyleSheet.create({

    dropdown3BtnStyle: {
        width: '95%',
        height: 50,
        backgroundColor: "transparent",
        color: Colors.black,

    },
    dropdown: {
        width: '95%',
        height: 180,


    },

    buttonstyle1: {
        fontSize: 14,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        textAlign: 'left',
        fontFamily: 'Roboto-Regular',
        color: Colors.black,
        marginLeft: -3
    },
    buttonstyle2: {
        fontSize: 14,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        textAlign: 'left',
        fontFamily: 'Roboto-Regular',
        color: Colors.hairline,
        marginLeft: -3
    },
    dropdownText: {
        fontSize: 14,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        textAlign: 'left',
        fontFamily: 'Roboto-Regular',
        backgroundColor: Colors.snow,
        height: 60,
        paddingTop: 14,
        paddingLeft: 10,

    },
    rowtextStyle: {
        marginTop: 4,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        textAlign: 'left',
        fontFamily: 'Roboto-Regular',

    },

});