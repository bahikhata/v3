import React, { useState, useEffect, useContext } from 'react';
import { View, Text, TextInput, ScrollView, ToastAndroid, Keyboard, TouchableOpacity } from 'react-native';
import { Colors, ApplicationStyles } from 'themes';
import { useNavigation } from '@react-navigation/native';
import { useRoute } from '@react-navigation/native';
import createcontext from 'context/Createcontext';
import { useAccount } from '../../hooks/useAccount';

const EditAccount = () => {
  const route = useRoute();
  const { id } = route.params;
  const { getAccountById, updateAccount, checkAccount } = useAccount()

  const [account, setAccount] = useState(() => {
    return getAccountById(id)?.name
  });

  function getPhoneNumber() {
    const result = getAccountById(id)
    let number = String(result.phone);
    return number.length > 10 ? String(result.phone).slice(-10) : result.phone
  }

  const [accountError, setAccountError] = useState("");
  const [phoneError, setPhoneError] = useState("")
  const [phone, setPhone] = useState(getPhoneNumber());


  const navigation = useNavigation();
  const { seteditRefresh, editRefresh } = useContext(createcontext)


  async function onPressButton() {
    try {
      // Clear previous errors
      setAccountError("");
      setPhoneError("");
      // Validate account
      if (!account.trim()) {
        setAccountError('Account is required');
        return;
      }

      const currentPhoneNumber = getPhoneNumber()
      // Validate phone number
      if (!validateMobile(phone)) {
        setPhoneError("Invalid Phone Number");
        return;
      }

      if (currentPhoneNumber !== phone) {
        const result = checkAccount(account, phone)
        if (result.length !== 0) {
          setAccountError('Account already exists');
          return;
        }
      }

      updateAccount(id, account, phone)
      ToastAndroid.show('Account updated successfully', ToastAndroid.SHORT);
      Keyboard.dismiss();
      navigation.goBack();
      seteditRefresh(!editRefresh);
    } catch (error) {
      console.log(error);
    }
  }

  const validateMobile = (mobile) => {
    console.log(mobile)
    const reg = /^[0-9]{10}$/; // Change the regex to match 10 digits
    if (mobile == null || mobile == "") {
      return true
    }
    return reg.test(mobile);
  }

  return (
    <ScrollView style={ApplicationStyles.container} keyboardShouldPersistTaps='always'>
      <TextInput
        style={ApplicationStyles.inputControl}
        placeholder="Account"
        value={account}
        maxLength={35}
        autoFocus
        selectionColor={Colors.black}
        placeholderTextColor={Colors.charcoal}
        underlineColorAndroid={Colors.transparent}
        onChangeText={(text) => setAccount(text)}
      />
      {accountError ? <Text style={ApplicationStyles.inputError}>{accountError}</Text> : null}

      <TextInput
        style={ApplicationStyles.inputControl}
        placeholder="Phone"
        value={phone}
        maxLength={10}
        selectionColor={Colors.black}
        placeholderTextColor={Colors.charcoal}
        underlineColorAndroid={Colors.transparent}
        onChangeText={(text) => setPhone(text)}
        keyboardType="numeric"
      />
      {phoneError ? <Text style={ApplicationStyles.inputError}>{phoneError}</Text> : null}

      <View style={ApplicationStyles.buttonWrapper}>
        <TouchableOpacity onPress={onPressButton} style={{
          paddingHorizontal: 20,
          paddingVertical: 12,
          borderRadius: 3,
          backgroundColor: Colors.primary,

        }}>
          <Text style={{
            color: Colors.snow,
            width: "100%",
            textAlign: 'center'
          }}>
            OK
          </Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

export default EditAccount;
