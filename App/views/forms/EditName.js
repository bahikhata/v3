import React, { useContext, useState } from 'react';
import PropTypes from 'prop-types';
import { useRoute } from '@react-navigation/native';

import {
  ScrollView,
  View,
  Text,
  Button,
  TextInput,
  Keyboard,
} from 'react-native';
import { Colors, ApplicationStyles } from 'themes';

const _ = require('lodash');
const validate = require('validate.js');
import { auth } from 'services';
import {useNetInfo} from '@react-native-community/netinfo';

const constraints = {
  name: {
    presence: {
      message: 'required',
    },
  },
};

export default function EditName({navigation}) {
  const route = useRoute();
  const [name, setName] = useState(route.params.name);
  const [nameError, setNameError] = useState('');
  const { isConnected } = useNetInfo();
  
  const onPressButton = async () => {
    const error = validate({name}, constraints);
    if (!error) {
      const result = await auth.setBusinessName(name);
      if (result) {
        Keyboard.dismiss();
        await auth.fetchUser(true).then(result => {
          console.log('user info refreshed!');
        });
        navigation.goBack();
      }
    } else {
      setNameError(error.name[0]);
    }
  };


  if (!isConnected) {
    return (
      <View style={ApplicationStyles.container}>
        <Text style={ApplicationStyles.noInternetText}>
          No internet connection
        </Text>
      </View>
    );
  }

  return (
    <ScrollView
      style={ApplicationStyles.container}
      keyboardShouldPersistTaps="always">
      <TextInput
        style={ApplicationStyles.inputControl}
        placeholder={'business'}
        value={name}
        keyboardType="default"
        maxLength={30}
        autoFocus
        selectionColor={Colors.black}
        placeholderTextColor={Colors.charcoal}
        underlineColorAndroid={Colors.transparent}
        onChangeText={text => setName(text)}
      />
      {nameError ? (
        <Text style={ApplicationStyles.inputError}>{nameError}</Text>
      ) : null}
      <View style={ApplicationStyles.buttonWrapper}>
        <Button title={'ok'} color={Colors.primary} onPress={onPressButton} />
      </View>
    </ScrollView>
  );
}

EditName.propTypes = {
  navigation: PropTypes.object,
};

// const styles = StyleSheet.create({
//   imageView: {
//     margin: Metrics.doubleSection,
//     alignItems: 'center',
//     alignContent: 'center'
//   },
//   image: {
//     width: Metrics.images.placeholder,
//     height: Metrics.images.placeholder,
//     resizeMode: 'contain'
//   },
//   view: {
//     margin: Metrics.baseMargin,
//     marginTop: 0,
//     marginBottom: 0
//   },
//   textView: {
//     color: Colors.charcoal,
//     fontSize: 14,
//     fontStyle: 'italic'
//   }
// });
