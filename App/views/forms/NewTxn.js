import React, { useEffect, useState, useContext } from "react";
import {
  StyleSheet, Text, View,
  ToastAndroid, TextInput,
  ScrollView, LogBox, BackHandler
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

import moment from "moment";
import DatePicker from '@react-native-community/datetimepicker'
import { Colors, Metrics, ApplicationStyles, Fonts } from "themes";
import { sms, mixpanel } from "services";
import { ActivityIndicator } from 'react-native-paper';
import { Autocomplete } from 'components';

import { IconButton } from 'components';
// import { launchImageLibrary, launchCamera } from 'react-native-image-picker';
// const _ = require("lodash");
const validate = require("validate.js");
const accounting = require("accounting");

import createcontext from "context/Createcontext";
import Sound from 'react-native-sound';
import { useTransaction } from "hooks/useTransaction";
import { useAccount } from "hooks/useAccount";
import { useStore } from "../../store";
import { SmsCheckbox } from "../../components/SmsCheckbox";

LogBox.ignoreLogs(['Animated: `useNativeDriver`'])

const option2 = {
  symbol: '₹',
  decimal: '.',
  thousand: ',',
  precision: 0,
  format: '%s%v',
};

const NewTxn = ({ navigation, props }) => {
  const [amount, setAmount] = useState('');
  const [error, setError] = useState('')
  const [detail, setDetail] = useState("");
  const account = useStore(state => state.accounts.filter(account => !account.cancelled));
  const [loding, setLoading] = useState(false);
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const [selectedImage, setSelectedImage] = useState(null);
  const [imageName, setImageName] = useState('');
  const [actionSheet, setActionSheet] = useState(false);
  const [imageBase64, setImageBase64] = useState('');
  const [checkBox, setCheckBox] = useState(false);
  const { selectedValue, setSelectedValue, refreshData, setRefreshData, showSuccess, setShowSuccess, deviceId } = useContext(createcontext);
  const { addTransaction, getCloseAmount } = useTransaction();
  const { updateBalance, getAccountById } = useAccount();



  useEffect(() => {
    const backAction = () => {
      setSelectedValue({ name: '', phone: '' });
      return false;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  }, []);

  // const actionItems = [
  //   {
  //     id: 1,
  //     label: 'Camera',
  //     onPress: () => {
  //       handleCameraLaunch();
  //     }
  //   },
  //   {
  //     id: 2,
  //     label: 'Gallery',
  //     onPress: () => {
  //       openImagePicker();
  //     }
  //   },
  // ];

  const playSound = () => {
    const newSound = new Sound("cash_add.mp3", Sound.MAIN_BUNDLE, (error) => {
      if (error) {
        console.log('Error loading sound:', error);
        return;
      }
      // setSound(newSound);
      newSound.play((success) => {
        if (success) {
          console.log('Sound played successfully');
        } else {
          console.log('Playback failed due to audio decoding errors');
        }
      });
    });
  };

  const onTxnDone = multiplier => {
    setLoading(true);

    mixpanel.track('Transaction Started');

    const amountUnformatted = accounting.unformat(amount).toString();
    let constraints;
    const closeBalance = getCloseAmount(new Date());

    if (multiplier < 0) {
      constraints = {
        amount: {
          presence: {
            message: 'required',
          },
          // numericality: {
          //   lessThanOrEqualTo: closeBalance,
          //   message: '- Not Enough Cash',
          // },
          format: {
            pattern: /^[0-9]+$/,
            message: 'invalidNo',
          },
        },
      };
    } else {
      constraints = {
        amount: {
          presence: {
            message: 'required',
          },
          format: {
            pattern: /^[0-9]+$/,
            message: 'invalidNo',
          },
        },
      };
    }
    const error = validate({ amount: amountUnformatted }, constraints);

    if (amountUnformatted === '0') {
      setAmount({
        value: amountUnformatted,
        error: 'Enter Amount between 1 to 99,99,999',
      });
      setLoading(false);
      return;
    }

    if (!error) {
      const result = addTransaction(
        selectedValue.name,
        amountUnformatted,
        detail,
        selectedImage,
        date,
        multiplier,
        imageBase64,
        imageName,
      );
      console.log("result => ", result);

      if (result.data) {
        console.log("data => ", result);
        mixpanel.track('Transaction Done');
        updateBalance(result.data.accountId);
        ToastAndroid.show('Transaction Added', ToastAndroid.SHORT);
        setLoading(false);
        setRefreshData(!refreshData);
        setShowSuccess(!showSuccess);
        setSelectedValue({ name: '', phone: '' });
        navigation.navigate('Home');
        playSound();
        const account = getAccountById(result.data.accountId);

        if (checkBox && account.phone !== null) {
          let phoneNo = account.phone.replace(/\s/g, '').slice(-10);
          phoneNo = '+91' + phoneNo;

          if (amountUnformatted > 0) {
            sms
              .sendPaidSMS(
                amountUnformatted,
                phoneNo,
                account.objectId,
              )
              .then(() => {
                console.log("Message Sent");
              });
          } else {
            sms
              .sendReceiveSMS(
                amountUnformatted,
                phoneNo,
                account.objectId,
              )
              .then(() => {
                console.log("Message Sent");
              });
          }
        }
      } else {
        setLoading(false);
        mixpanel.track('Transaction Failed');
        ToastAndroid.show('Transaction Failed', ToastAndroid.SHORT);
      }
    } else {
      if (error) {
        setAmount(amountUnformatted);
        setError(error.amount[0]);

        setLoading(false);
      } else {
        setAmount(amountUnformatted);
        setError('');
        setLoading(false);
      }
    }
  };

  const datePicker = () => {
    setShow(true);
  }

  // const openImagePicker = () => {
  //   const options = {
  //     mediaType: 'photo',
  //     includeBase64: true,
  //     maxHeight: 2000,
  //     maxWidth: 2000,
  //     quality: 0.5
  //   };

  //   launchImageLibrary(options, (response) => {
  //     if (response.didCancel) {
  //       console.log('User cancelled image picker');
  //     } else if (response.error) {
  //       console.log('Image picker error: ', response.error);
  //     } else {
  //       console.log(response)
  //       setSelectedImage(response.assets?.[0]);
  //       setImageBase64(response.assets?.[0]?.base64);
  //       setImageName(response.assets?.[0]?.fileName);
  //     }
  //   });
  // };

  // const handleCameraLaunch = () => {
  //   const options = {
  //     mediaType: 'photo',
  //     includeBase64: true,
  //     maxHeight: 2000,
  //     maxWidth: 2000,
  //     quality: 0.5
  //   };

  //   launchCamera(options, response => {
  //     // console.log('Response = ', response);
  //     if (response.didCancel) {
  //       console.log('User cancelled camera');
  //     } else if (response.error) {
  //       console.log('Camera Error: ', response.error);
  //     } else {
  //       let imageUri = response.uri || response.assets?.[0]?.uri;
  //       setSelectedImage(response.assets?.[0]);
  //       setImageBase64(response.assets?.[0]?.base64);
  //       setImageName(response.assets?.[0]?.fileName);
  //     }
  //   });
  // }




  const handleCheckbox = () => {
    if (selectedValue.name.length === 0) {
      ToastAndroid.show('Select Account', ToastAndroid.SHORT);
      setCheckBox(false)
      return
    }
    if (selectedValue.phone === '') {
      ToastAndroid.show('Mobile No. Not added  ', ToastAndroid.SHORT);
      setCheckBox(false)
      return
    }
    if (amount == '') {
      ToastAndroid.show('Enter Amount', ToastAndroid.SHORT);
      setCheckBox(false)
    }
    else {
      setCheckBox(!checkBox)
    }
  }




  return (
    <>
      <ScrollView
        style={styles.containers} keyboardShouldPersistTaps='always'>
        {show && <DatePicker
          testID="dateTimePicker"
          value={date}
          mode='date'
          is24Hour={true}
          display="default"
          themeVariant="light"
          style={{ backgroundColor: Colors.whitish }}
          onChange={(event, selectedDate) => {
            const currentDate = selectedDate || date;
            setShow(false);
            setDate(currentDate);
          }}
          maximumDate={new Date()}
        />
        }
        <View style={styles.form}>
          <View style={styles.formRow}>
            <View style={styles.subRow}>
              <TextInput
                style={ApplicationStyles.inputControl}
                placeholder={('Amount')}
                keyboardType='numeric'
                spellCheck={false}
                autoCorrect={false}
                maxLength={11}
                autoFocus
                value={amount}
                selectionColor={Colors.primary}
                // placeholderTextColor={Colors.charcoal}
                underlineColorAndroid={Colors.transparent}
                onChangeText={(text) => {
                  let val;
                  if (text === '₹') {
                    val = '';
                  } else {
                    val = accounting.formatMoney(text, option2);
                  }
                  setAmount(val)
                  setError(null);
                }}
              />
              <View style={styles.dateView}>
                <Icon
                  name='calendar'
                  size={Metrics.icons.small}
                  color={Colors.primary}
                  style={styles.calendarMenu}
                  onPress={() => datePicker()}
                />
                <Text style={styles.dateText} onPress={() => datePicker()}>
                  {moment(date).format('DD MMM')}
                </Text>
              </View>
            </View>
            {
              error ?
                <Text style={{
                  ...ApplicationStyles.inputError,
                  marginLeft: Metrics.baseMargin,
                }}>{error}</Text>
                : null
            }
            <View style={styles.subRow}>


              <Autocomplete
                account={account}
                setCheckBox={setCheckBox}
              />

              <View style={styles.contactPicker}>
                <Icon
                  name='id-card'
                  size={Metrics.icons.small}
                  color={Colors.primary}
                  onPress={() => navigation.navigate('ContactPicker', { path: 'tnx' })}
                  style={styles.contactIcon}
                />
              </View>

            </View>

            {
              account.error ?
                <Text style={{ ...ApplicationStyles.inputError, marginLeft: Metrics.baseMargin, }}>{account.error}</Text>
                : null
            }

            <View style={{
              flex: 1, flexDirection: 'row', alignItems: 'center',
            }} >
              <TextInput
                style={{ ...ApplicationStyles.inputControl }}
                placeholder={('Detail')}
                multiline
                autoCorrect={false}
                maxLength={60}
                value={detail}
                selectionColor={Colors.black}
                underlineColorAndroid={Colors.transparent}
                onChangeText={(text) => setDetail(text)}
              />


              {/* <View style={{ ...styles.attachImage }}>
                {
                  selectedImage && (
                    <TouchableOpacity
                      onPress={() => setSelectedImage(null)}
                      style={{
                        position: 'absolute',
                        left: 35,
                        top: -19,
                        zIndex: 1,

                      }}
                    >
                      <Icon
                        name='times-circle'
                        size={Metrics.icons.small}
                        color={Colors.primary}
                        style={styles.calculatorIcon}
                      />
                    </TouchableOpacity>
                  )
                }


                {selectedImage && (
                  <Image
                    source={{ uri: selectedImage.uri }}
                    style={{ flex: 1, height: 50, width: 50, marginLeft: 10 }}
                    resizeMode="contain"
                  />
                )}
                <View style={{ flex: 1, flexDirection: 'row', marginLeft: 10 }}>
                  <TouchableOpacity
                    onPress={() => setActionSheet(true)}
                    style={{ marginRight: Metrics.baseMargin }}
                  >
                    <Icon
                      name='paperclip'
                      size={Metrics.icons.small}
                      color={Colors.primary}
                      style={styles.calculatorIcon}
                    />
                  </TouchableOpacity>

                </View>
              </View> */}

            </View>

            <View style={{ marginTop: 20, marginBottom: 15, flex: 0.1, flexDirection: 'row', alignItems: 'center', marginLeft: Metrics.baseMargin }}>
              <SmsCheckbox value={checkBox} onValueChange={handleCheckbox} />
            </View>

          </View>
        </View>
        <View style={styles.buttonGroup}>
          <View style={styles.inButton}>
            <IconButton
              iconName='arrow-circle-down'
              disabled={!amount || loding}
              text={('Cash In')}
              onPress={() => onTxnDone(1)}
              btn={"Add"}
            />
          </View>
          <View style={styles.outButton}>
            <IconButton
              iconName='arrow-circle-up'
              disabled={!amount || loding}
              text={('Cash Out')}
              onPress={() => onTxnDone(-1)}
              btn={"Remove"}

            />
          </View>
        </View>

        {loding && <View style={{
          flex: 1, justifyContent: 'center', alignItems: 'center',
          marginTop: Metrics.doubleBaseMargin
        }}>
          <ActivityIndicator size="large"
            color={Colors.primary}
          />
        </View>}

      </ScrollView >
      {/* <FloatingAction
        color={Colors.primary}
        onPressMain={openCalculator}
        showBackground={false}
        position="right"
        distanceToEdge={15}
        floatingIcon={<Icon name="calculator" color={Colors.snow} size={22} />} /> */}


      {/* <Modal
        isVisible={actionSheet}
        style={{
          margin: 0,
          justifyContent: 'flex-end'
        }}
        onBackdropPress={closeActionSheet}
      >
        <ActionSheet
          actionItems={actionItems}
          onCancel={closeActionSheet}
          dissmiss={dissmissContextualMenu}
        />
      </Modal> */}
    </>
  );
}

export default NewTxn;

let styles = StyleSheet.create({
  selectfield: {
    flex: 0.9,
    fontSize: Fonts.size.normal,
    margin: Metrics.baseMargin,
    borderBottomColor: Colors.fire,
    borderBottomWidth: 0.5
  },
  searchTextInput: {
    color: Colors.charcoal,
    padding: 10,
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#efefef',
    height: 50,
    width: '90%',
    paddingHorizontal: 10,
    zIndex: 1,
  },
  buttonText: {
    flex: 1,
    textAlign: 'center',
  },
  contactIcon: {
    flex: 0.1,
    // marginLeft: Metrics.baseMargin,
    fontSize: 30,
  },
  containers: {
    flex: 1,
    backgroundColor: Colors.whitish,
    width: '100%',
  },
  camera: {
    flex: 0.25,
    padding: Metrics.smallMargin,
    marginRight: Metrics.smallMargin,
    marginTop: Metrics.doubleBaseMargin,
    width: Metrics.images.placeholder,
    height: Metrics.images.placeholder,
    alignItems: 'center',
    justifyContent: 'center'
  },
  attachment: {
    width: Metrics.images.placeholder,
    height: Metrics.images.placeholder,
    resizeMode: 'stretch'
  },
  calculatorIcon: {
    flex: 0.1,
    marginTop: 1.5 * Metrics.baseMargin,
    marginRight: Metrics.baseMargin
  },
  attachImage: {
    flex: 0.3,
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    // marginLeft: Metrics.baseMargin,
  },
  inButton: {
    flex: 0.5,
    marginRight: Metrics.smallMargin,
    marginLeft: Metrics.baseMargin,
    // backgroundColor:Colors.darkGreen
  },
  outButton: {
    flex: 0.5,
    marginLeft: Metrics.smallMargin,
    marginRight: Metrics.baseMargin,
  },
  buttonGroup: {
    flex: 0.2,
    flexDirection: 'row',
    marginTop: 20
  },
  dropdownControl: {
    flex: 0.8,
    fontSize: Fonts.size.normal,
    margin: Metrics.baseMargin,
    borderBottomColor: Colors.fire,
    borderBottomWidth: 0.5
  },
  dropdown: {
    padding: Metrics.baseMargin,
    backgroundColor: Colors.whitish,
  },
  dropdownText: {
    color: Colors.black,
    fontSize: 18
  },
  // dropdownControl: {
  //   flex: 0.76,
  //   fontSize: Fonts.size.normal,
  //   margin: Metrics.baseMargin,
  //   borderBottomColor: Colors.fire,
  //   backgroundColor: Colors.whitish,
  //   borderBottomWidth: 0.5
  // },
  // dropdown: {
  //   padding: Metrics.baseMargin,
  //   backgroundColor: Colors.whitish,
  //   height: 'auto',
  //   width: "82%",
  // },
  // dropdownText: {
  //   color: Colors.black,
  //   marginLeft: 10,
  //   fontSize: 16,
  //   flex: 1,
  //   justifyContent: 'flex-start',
  //   textAlign: 'left',
  // },
  contactPicker: {
    flex: 0.25,
    marginTop: 25,
    marginLeft: 10,
  },
  subRow: {
    flex: 1,
    flexDirection: 'row'
  },
  form: {
    flex: 1,
    flexDirection: 'row'
  },
  formRow: {
    flex: 1,
    flexDirection: 'column'
  },
  amountInput: {
    flex: 0.6,
    fontSize: Fonts.size.normal,
    margin: Metrics.baseMargin,
    borderBottomColor: Colors.fire,
    borderBottomWidth: 0.5
  },
  dateView: {
    flex: 0.4,
    flexDirection: 'row',
    padding: 10,
    marginTop: 10
  },
  dateText: {
    fontSize: 18,
    paddingLeft: 10,
    color: Colors.hairline,
    fontWeight: "bold"
  }
});
