import React, { useState, useEffect, useCallback, useContext } from 'react';
import { 
  StyleSheet, View, Text, Button, TextInput, 
  ScrollView, Keyboard, ToastAndroid 
} from 'react-native';
import { Colors, Metrics, ApplicationStyles } from 'themes';
import { api } from 'services';
import { useRoute, useNavigation } from '@react-navigation/native';
import createcontext from 'context/Createcontext';
import { useTransaction } from 'hooks/useTransaction';

const EditTxn = () => {
  const [detail, setDetail] = useState('');
  const navigation = useNavigation();
  const route = useRoute();
  const id = route.params.id;
  const { setRefreshData, refreshData } = useContext(createcontext);
  const {updateTransaction} = useTransaction();
  useEffect(() => {
    setDetail(id.detail);
  }, []);

  const onPressButton = async () => {
     try {
      updateTransaction(id.objectId, detail)
      ToastAndroid.show('Transaction updated successfully', ToastAndroid.SHORT);
      Keyboard.dismiss();
      setRefreshData (!refreshData );
      navigation.goBack();
     } catch (error) {
        console.log(error);
     }
  };

  return (
    <ScrollView
      style={styles.container}
      keyboardShouldPersistTaps='always'
    >
      <TextInput
        style={ApplicationStyles.inputControl}
        placeholder="Enter detail"
        value={detail}
        maxLength={35}
        autoFocus
        selectionColor={Colors.black}
        placeholderTextColor={Colors.charcoal}
        underlineColorAndroid={Colors.transparent}
        onChangeText={(text) => setDetail(text)}
      />
     <View style={ApplicationStyles.buttonWrapper}>
        <Button
          title={'ok'}
          color={Colors.primary}
          onPress={onPressButton}
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.whitish
  },
  button: {
    margin: Metrics.baseMargin,
    padding: Metrics.baseMargin,
    backgroundColor: Colors.charcoal
  }
});

export default EditTxn;
