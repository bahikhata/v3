// leave off @2x/@3x
const Images = {
  swastika: require('./assets/images/swastika.png'),
  background: require('./assets/images/background.png'),
  placehold: require('./assets/images/placehold.jpeg'),
  india: require('./assets/images/indianflag.png'),
  success: require('./assets/images/success_icon.svg'),
  logo: require('./assets/images/logo.png'),
  syncBackground: require('./assets/images/sync-background.png'),
  avatar: require('./assets/images/avatar.png'),
};

export default Images;
