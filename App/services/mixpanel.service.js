import  { Mixpanel }  from "mixpanel-react-native";

const MIXPANEL_TOKEN = process.env.REACT_APP_MIXPANEL_TOKEN;

const trackAutomaticEvents = false;
const mixpanel = new Mixpanel(MIXPANEL_TOKEN, trackAutomaticEvents);
mixpanel.init();

mixpanel.registerSuperProperties({Source: 'App'});

export default mixpanel;
