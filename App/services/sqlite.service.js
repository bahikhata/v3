import SQLite from 'react-native-sqlite-storage';

var db = SQLite.openDatabase(
  'test.db',
  '1.0',
  'Test Database',
  200000,
  () => {
    console.log('Database opened');
  },
  () => {
    console.log('Database opened failed');
  },
);

export default db;
