import AsyncStorage from "@react-native-async-storage/async-storage";
import Axios from "./axios.service";
import { useStore } from "../store";

export const fetchUser = async networkOnly => {
  try {
    if (!networkOnly) {
      const user = await AsyncStorage.getItem('user');
      if (user) {
        return JSON.parse(user);
      }
    }

    const result = await Axios.get('users/me');
    if (result.data) {
      await AsyncStorage.setItem('user', JSON.stringify(result.data));
      useStore.setState({ user: result.data });
      return result.data;
    }
    return null;
  } catch (error) {
    console.log(error);
    return null;
  }
};

export const setName = async (_name) => {
  return fetchUser().then((res) => {
    return Axios.put(`users/${res.objectId}`, {name: _name})
    .then((res) => res.data)
    .catch((err) => console.log(err));
  });
}

export const setPhone = async (_phone) => {
  return fetchUser().then((res) => {
    return Axios.put(`users/${res.objectId}`, {phone: _phone})
    .then((res) => res.data)
    .catch((err) => console.log(err));
  });
}

export const setUsername = async (_username) => {
  return fetchUser().then((res) => {
    return Axios.put(`users/${res.objectId}`, {username: _username})
    .then((res) => res.data)
    .catch((err) => console.log(err));
  });
}

export const setBusinessName= async (businessName)=> {
  return fetchUser().then((res) => {
    return Axios.put(`users/${res.objectId}`, {businessName: businessName?businessName:""})
    .then((res) =>{
      console.log(res,"res")
      return res.data
    })
    .catch((err) => console.log(err));
  });
}

export const setEmail = async (_email) => {
  return fetchUser().then((res) => {
    return Axios.put(`users/${res.objectId}`, {email: _email?_email:""})
    .then((res) => res.data)
    .catch((err) => console.log(err));
  });
}

export const sendVerificationEmail = async (_email) => {
  return Axios.post(`verificationEmailRequest`, {email: _email})
    .then((res) => res.status)
    .catch((err) => console.log(err));
}

export const setTypeOfBusiness = async (business) => {
  return fetchUser().then((res) => {
    return Axios.put(`users/${res.objectId}`, {typeOfBusiness: business ? business : ""})
    .then((res) => res.data)
    .catch((err) => console.log(err));
  });
}

export const login = async (_username) => {
  return Axios.post("login", {username: _username, password: "L*g9Wg#6i005$!qT4K4#cEvOp"})
  .then((res) => res.status)
  .catch((err) => console.log(err));
}

export const signup = async (_username) => {
  return Axios.post("users", {username: _username, password: "L*g9Wg#6i005$!qT4K4#cEvOp", phone: _username})
  .then((res) => res)
  .catch((err) => console.log(err));
}

export const logout = async () => {
  return await Axios.post("logout")
    .then((res) => res)
    .catch((err) => console.log(err));
}