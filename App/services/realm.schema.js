import Realm from 'realm';
import {
  convertAllAccountEntries,
  convertAllTransactionEntries,
} from 'converter';
import { generateObjectId } from 'utils';
import AsyncStorage from '@react-native-async-storage/async-storage';
import db from './sqlite.service';

const accObjMap = {};

const TransactionSchema = {
  name: 'Transaction',
  primaryKey: 'objectId',
  properties: {
    objectId: 'string',
    date: 'date',
    accountId: 'string',
    attachment: 'string',
    amount: { type: 'int', default: 0 },
    detail: 'string',
    cancelled: { type: 'bool', default: false },
    userId: 'string',
    createdAt: { type: 'date', default: new Date() },
    updatedAt: { type: 'date', default: new Date() },
    type: 'string',
    synced: { type: 'bool', default: false },
  },
};

const AccountSchema = {
  name: 'Account',
  primaryKey: 'objectId',
  properties: {
    objectId: 'string',
    name: 'string',
    balance: { type: 'int', default: 0 },
    phone: 'string',
    cancelled: { type: 'bool', default: false },
    userId: 'string',
    date: 'date?',
    createdAt: { type: 'date', default: new Date() },
    updatedAt: { type: 'date', default: new Date() },
    checkboxSMS: { type: 'bool', default: false },
    enabled: { type: 'bool', default: true },
    synced: { type: 'bool', default: false },
  },
};

const realm = new Realm({
  schema: [TransactionSchema, AccountSchema],
  schemaVersion: 6,
  onFirstOpen: async realm => {
    // ToastAndroid.show('First Open', ToastAndroid.SHORT);
    console.log('First Open');
  },
});

async function onFirstLaunchApp(realm) {
  try {
    console.log('Migrating data...');
    console.log('Realm path:', realm.path);
    console.log('Migrating data...');
    const isFirstLaunch = await AsyncStorage.getItem('first_launch');

    if (isFirstLaunch === 'true') {
      console.log('Already migrated!');
      return;
    }

    const dbAwaited = await db;
    dbAwaited.transaction(async tx => {
      // verify sqlite working! - echo
      // tx.executeSql('SELECT sqlite_version();', [], (tx, results) => {
      //   console.log('Query completed');
      //   console.log('SQLite version:', results.rows.item(0));
      // });


      console.log("[MIGRATION] Checking if tables exist!")

      // check if tables exist
      const { accounts: oldAccounts, transactions: oldTransactions, users: olderUsers } = await new Promise((resolve, reject) =>
        tx.executeSql(
          `SELECT name FROM sqlite_master WHERE type='table';`,
          [],
          (tx, results) => {
            const tables = processSqlRows(results);

            const tableNames = tables.map(table => table.name);
            const isAccountsExist = tableNames.includes('accounts');
            const isTransactionsExist = tableNames.includes('transactions');

            if (!isAccountsExist || !isTransactionsExist) {
              console.log("[MIGRATION] I couldn't believe myself... they don't exist... tables... I thought...! 😥");

              return resolve({
                users: [],
                accounts: [],
                transactions: [],
              });
            }

            console.log("[MIGRATION] It's true! tables actually exist! 🤩");


            tx.executeSql('SELECT * FROM accounts', [], (tx, results) => {
              console.log('Query completed');
              console.log('Account:', results.rows);
              console.log(`AccountLength: ${results.rows.length}`);
              const accountsResult = processSqlRows(results);

              tx.executeSql('SELECT * FROM transactions', [], (tx, results) => {
                console.log('Query completed');
                console.log('Transactions:', results.rows);
                console.log(`TransactionLength: ${results.rows.length}`);
                const transactionResult = processSqlRows(results);

                tx.executeSql('SELECT * FROM users', [], (tx, results) => {
                  console.log('Query completed');
                  console.log('Users:', results.rows);
                  console.log(`UsersLength: ${results.rows.length}`);
                  const usersResult = processSqlRows(results);

                  resolve({
                    users: usersResult,
                    accounts: accountsResult,
                    transactions: transactionResult,
                  });
                });
              });
            })
          },
          (tx, error) => {
            console.error('Error:', error);
            reject(error);
          },
        ),
      );

      const userId = generateObjectId();
      await AsyncStorage.setItem('temp_user_id', userId);
      if(olderUsers?.[0]?.id) await AsyncStorage.setItem('old_user_id', olderUsers?.[0]?.id);

      const newAccEntries = convertAllAccountEntries(
        oldAccounts,
        accObjMap,
        userId,
      );
      console.log('log of NewAccEntries', newAccEntries);
      const newTnxEntries = convertAllTransactionEntries(
        oldTransactions,
        accObjMap,
        userId,
      );
      console.log('log of NewTransEntries', newTnxEntries);
      newAccEntries.forEach(acc => {
        realm.write(() => {
          realm.create('Account', acc);
        });
      });
      newTnxEntries.forEach(entry => {
        realm.write(() => {
          realm.create('Transaction', entry);
        });
      });

      console.log('Migration completed!');
      await AsyncStorage.setItem('first_launch', 'true');
    });
  } catch (error) {
    console.error('error on migration', error);
  }
}


function processSqlRows(sqlRows) {
  const results = [];
  for (let i = 0; i < sqlRows.rows.length; i++) {
    console.log('Account:', sqlRows.rows.item(i));
    results.push(sqlRows.rows.item(i));
  }

  return results;
}

export { realm, TransactionSchema, AccountSchema, onFirstLaunchApp };
