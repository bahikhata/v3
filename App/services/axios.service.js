import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

// export const BASE_URL = process.env.NODE_ENV == "development" ? process.env.REACT_APP_SERVER_URL : process.env.REACT_APP_SERVER_URL_PROD;
// export const BASE_URL = "https://stagingapi.bahikhata.org/parse"
export const BASE_URL = "https://api.bahikhata.org/parse"
// process.env.REACT_APP_APP_ID = "QSNMhCaCyz4KCAo6" // staging

console.log(process.env, process.env.REACT_APP_APP_ID,  process.env.REACT_APP_MIXPANEL_TOKEN)

console.log("URL2 : " + BASE_URL);

const Axios = axios.create({
  baseURL: BASE_URL,
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'X-Parse-Application-Id': process.env.REACT_APP_APP_ID,
    'X-Parse-Revocable-Session': 1,
  },
});

// Use an interceptor to set the session token before each request
Axios.interceptors.request.use(
  async config => {
    const sessionToken = await AsyncStorage.getItem('sessionToken');
    if (sessionToken) {
      config.headers['X-Parse-Session-Token'] = sessionToken;
    }
    return config;
  },
  error => {
    return Promise.reject(error);
  },
);

export default Axios;
