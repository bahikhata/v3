import { BASE_URL } from './axios.service';
import { fetchUser } from './auth.service';
import { SendDirectSms } from 'react-native-send-direct-sms';
import { realm } from './realm.schema';

const fetchApi = async (route, data, method = 'POST') => {
  const url = `${BASE_URL}/${route}`;

  const headers = {
    'Content-Type': 'application/json',
    'X-Parse-Application-Id': process.env.REACT_APP_APP_ID,
  };

  const options = {
    method,
    headers,
    body: JSON.stringify(data),
  };

  try {
    const res = await fetch(url, options);
    const responseData = await res.json();
    return responseData;
  } catch (error) {
    console.error('Fetch error:', error);
    throw error;
  }
};

export const sendOTP = async mobile => {
  const params = { mobile: mobile };

  return await fetchApi('functions/sendOTP', params)
    .then(responseData => {
      console.log(responseData);
      return responseData.result;
    })
    .catch(error => console.log(error.originalError));
};

export const resendOTP = async (mobile, type) => {
  const params = { mobile: mobile, OTPtype: type };
  return await fetchApi('functions/resendOTP', params)
    .then(responseData => {
      return responseData;
    })
    .catch(error => console.log(error));
};

export const verifyOTP = async (mobile, otp) => {
  const params = { mobile: mobile, otp: otp };
  return await fetchApi('functions/verifyUser', params)
    .then(responseData => {
      return responseData;
    })
    .catch(error => console.log(error));
};

export const sendPaidSMS = async (amount, party, accountId) => {
  const user = await fetchUser();
  const message = `You received Rs. ${amount} from ${user.businessName}

Ledger: https://web.bahikhata.org/p/${accountId}.

- Sent via Bahi Khata App`
  const account = realm.objectForPrimaryKey('Account', accountId)
  
  const newMessage = `Bahi Khata
Credit of Rs. ${amount}
Added by ${user.businessName}
Balance Rs. ${account.balance}
Details https://web.bahikhata.org/p/${accountId}
  `

  console.log('Sending SMS to', party, 'with message', newMessage);
  return SendDirectSms(party, newMessage).catch(error =>
    console.log('Error sending SMS:', error),
  );
};

export const sendReceiveSMS = async (amount, party, accountId) => {
  const user = await fetchUser();
  const message = `You paid Rs. ${amount} to ${user.businessName}

Ledger: https://web.bahikhata.org/p/${accountId}.

- Sent via Bahi Khata App`

  const account = realm.objectForPrimaryKey('Account', accountId)

  const newMessage = `Bahi Khata
Payment of Rs. ${amount}
Added by ${user.businessName}
Balance Rs. ${account.balance}
Details https://web.bahikhata.org/p/${accountId}
`

  console.log('Sending SMS to', party, 'with message', newMessage);
  return SendDirectSms(party, newMessage).catch(error =>
    console.log('Error sending SMS:', error),
  );
};
