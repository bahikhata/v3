const REST_ID= process.env.NODE_ENV === "development" ? process.env.REACT_APP_ONESIGNAL_REST_API_KEY : process.env.REACT_APP_ONESIGNAL_REST_API_KEY_PROD;
const APP_ID = process.env.NODE_ENV === "development" ? process.env.REACT_APP_ONESIGNAL_APP_ID : process.env.REACT_APP_ONESIGNAL_APP_ID_PROD;


const sendPushNotification = async (message, recipientId) => {
  return fetch('https://onesignal.com/api/v1/notifications', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Basic ${REST_ID}`
    },
    body: JSON.stringify({
      app_id: APP_ID,
      include_player_ids: [recipientId],
      contents: { en: message }
    })
  })
  .then(response => response.json())
  .then(data => {
    console.log("Push notification sent:", data);
    return data;
  })
  .catch(error => {
    console.error("Error sending push notification:", error);
    throw error;
  });
};

export default sendPushNotification;
