import { realm } from './realm.schema'

export function transaction(_account, amount, phone, detail, attachment, date, multiplier) {
  if (_account.length > 0) {
    const accounts = realm.objects('Account').filtered(`name = "${_account}"`);
    if (accounts.length === 0) {
      realm.write(() => {
          realm.create('Account', {
            id: (realm.objects('Account').length > 0 ?
              realm.objects('Account').max('id') : 0) + 1,
            name: _account,
            amount: amount * multiplier,
            phone
          });

          const newAccount = realm.objects('Account')
            .filtered(`name = "${_account}"`);
          realm.create('Transaction', {
            id: (realm.objects('Transaction').length > 0 ?
              realm.objects('Transaction').max('id') : 0) + 1,
            account: newAccount[0],
            amount: amount * multiplier,
            detail,
            attach: attachment === '' ? '' : attachment.uri,
            updated: date,
            created: date
          });
      });
    } else {
      realm.write(() => {
        realm.create('Transaction', {
          id: (realm.objects('Transaction').length > 0 ?
            realm.objects('Transaction').max('id') : 0) + 1,
          account: accounts[0],
          amount: amount * multiplier,
          detail,
          attach: attachment === '' ? '' : attachment.uri,
          updated: date,
          created: date
        });
        realm.create('Account', {
          id: accounts[0].id,
          amount: accounts[0].amount + (amount * multiplier),
          updated: new Date()
        }, true);
      });
    }
  } else {
    realm.write(() => {
      realm.create('Transaction', {
        id: (realm.objects('Transaction').length > 0 ?
          realm.objects('Transaction').max('id') : 0) + 1,
        account: null,
        amount: amount * multiplier,
        detail,
        attach: attachment === '' ? '' : attachment.uri,
        updated: date,
        created: date
      });
    });
  }
}

export function account(name, phone) {
  /*db.transaction((txn) => {
    txn.executeSql('CREATE TABLE IF NOT EXISTS accounts(id INTEGER PRIMARY KEY NOT NULL, name VARCHAR(30), phone VARCHAR(30), balance INTEGER)', []);
    txn.executeSql('INSERT INTO accounts (name, phone, balance) VALUES (:name, :phone, :balance)', [name, phone, 0]);
  });*/

  realm.write(() => {
    realm.create('Account', {
      id: (realm.objects('Account').length > 0 ?
          realm.objects('Account').max('id') : 0) + 1,
      name,
      amount: 0,
      phone
    });
  });
}

export function user(name, date) {
  realm.write(() => {
    realm.create('User', {
      id: 3,
      name: name,
      period: date,
      phone: ''
    });
  });
}

export function bank() {
    realm.write(() => {
      realm.create('Bank', {
        id: (realm.objects('Bank').length > 0 ?
          realm.objects('Bank').max('id') : 0) + 1,

      });
    });
}

export function updateAccount(id, _account) {
  realm.write(() => {
    realm.create('Account', {
      id,
      name: _account,
      updated: new Date()
    }, true);
  });
}

export function updateUser(_user, _date) {
  realm.write(() => {
    realm.create('User', {
      id: 1,
      name: _user,
      period: _date,
      updated: new Date()
    }, true);
  });
}

export function updateTransaction(id, _detail) {
  realm.write(() => {
    realm.create('Transaction', {
      id,
      detail: _detail,
      updated: new Date()
    }, true);
  });
}

export function getUser() {
  return realm.objects('User');
}

export function getUsername() {
  const user = realm.objects('User');
  if (user.length > 0) {
    return user[0].name;
  }
  return '';
}

export function getAccounts() {
  const accounts = realm.objects('Account');
  if (accounts.length > 0) {
    return accounts;
  }
  return accounts;
}

export function getAccountByName(name) {
  return realm.objects('Account').filtered('name = $0', name);
}

export function getAccountById(id) {
  return realm.objects('Account').filtered('id = $0', id);
}

export function getTxns(first, next) {
  return realm.objects('Transaction')
    .filtered('created >= $0 AND created < $1', first, next);
}

export function getTxnsByAccountId(id) {
  return realm.objects('Transaction').filtered(`account.id = "${id}"`);
}

export function getTxnById(id) {
  return realm.objects('Transaction').filtered('id = $0', id);
}

export function sumTillDate(date) {
  return realm.objects('Transaction')
    .filtered('created <= $0 AND cancelled = false', date).sum('amount');
}

export function closeAccount(_account) {
  realm.write(() => {
    realm.delete(_account);
  });
}

export function deleteTransaction(txn) {
  if (txn.account != null) {
    realm.write(() => {
      realm.create('Account', {
        id: txn.account.id,
        amount: txn.account.amount - txn.amount,
        updated: new Date()
      }, true);
      realm.delete(txn);
    });
  } else {
    realm.write(() => {
      realm.delete(txn);
    });
  }
}

export function deleteAll() {
  realm.write(() => {
    realm.deleteAll();
  });
}

export function getReminderById(id) {
  return realm.objects('Account').filtered('id = $0 AND enabled = true', id);
}

export function addReminder(id,dates){
  realm.write(() => {
    realm.create('Account', {
      id,
      date: dates,
      enabled: true
    }, true);
  });
}
export function deleteReminderById(id){
  realm.write(() => {
    realm.create('Account', {
      id,
      date: null,
      enabled: false
    }, true);
  });
}