import Axios from './axios.service';
import { fetchUser } from './auth.service';
// import RNFetchBlob from 'rn-fetch-blob';
// import RN from 'react-native';
import { Platform } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import netInfo from '@react-native-community/netinfo';
import { useStore } from '../store';
import {produce} from 'immer'
export const account = async (_name, _phone, balance = 0) => {
  const user = await fetchUser();

  const body = {
      name: _name ? _name : null,
      balance: balance,
      phone: _phone ? _phone : null,
      cancelled: false,
      userId: {
        __type: "Pointer",
        className: "_User",
        objectId: user.objectId
      }
  };

  return Axios
    .post('classes/Account', body)
    .then(res => res.data)
    .catch(err => console.log(err));
};

export const getAccounts = async () => {
  const user = await fetchUser();
  const params = {
    where: {
      userId: {
        __type: "Pointer",
        className: "_User",
        objectId: user.objectId
      }
    },
    order: "name, cancelled",
    limit: 1000000
  }

  return Axios
    .get('classes/Account', { params })
    .then(res => res.data.results)
    .catch(err => console.log(err));
};

// export const AddReminder = async (id, date, state) => {
//   return getAccountById(id).then(async (myAccount) => {
//     myAccount.set("date", date);
//     myAccount.set("enabled", state);
//     return await myAccount.save();
//   });
// };

export const AddReminder = async (id, date, state) => {
  const user = await fetchUser();

  const body = {
    date: {
      __type: "Date",
      "iso": date ? date : null
    },
    enabled: state
  }


  return Axios
    .put(`classes/Account/${id}`, body)
    .then(res => {
      console.log(res)
      return res.data
    })
    .catch(error => error.message);
}

export const getReminder = async (id) => {
  const user = await fetchUser();
  const params = {
    where: {
      objectId: id,
      userId: {
        __type: "Pointer",
        className: "_User",
        objectId: user.objectId
      }
    }
  }

  return Axios
    .get('classes/Account', { params })
    .then(res => res.data.results[0])
    .catch(error => error.message);
}

// delete reminder by id 
export const deleteReminder = async (id) => {
  const user = await fetchUser();
  const body = {
    date: null,
    enabled: false
  }

  return Axios
    .put(`classes/Account/${id}`, body)
    .then(res => res.data)
    .catch(error => error.message);
}

// update username 
export const updateUsername = async (_name) => {
  const user = await fetchUser();
  const body = {
    username: _name
  }

  return Axios
    .put(`users/${user.objectId}`, body)
    .then(res => res.data)
    .catch(error => error.message);
}

// export const downloadMonthlyReport = async (id, startMonth, endMonth) => {
//   const params = {
//     id: id,
//     startMonth: startMonth,
//     endMonth: endMonth,
//   };
//   return await Parse.Cloud.run("getMonthlyreport", params)
//     .then((res) => {
//       if (startMonth !== "lifetime") {
//         startMonth.setDate(startMonth.getDate() + 1);
//       }
//       const blob = base64toBlob(res, { type: "application/pdf" });
//       const url = window.URL.createObjectURL(blob);
//       const a = document.createElement("a");
//       a.href = url;
//       a.download = `Report(${
//         startMonth === "lifetime"
//           ? ""
//           : moment(startMonth).format("MMM DD YYYY") + " - "
//       }${moment(endMonth).format("MMM DD YYYY")}).pdf`;
//       // a.download = "report.pdf";
//       document.body.appendChild(a);
//       a.click();
//       document.body.removeChild(a);
//       window.URL.revokeObjectURL(url);
//       return "success";
//     })
//     .catch((err) => {
//       return err;
//     });
// };

// check accout alredy exit using name and mobile number

// export const downloadReport = async (id, startMonth, endMonth) => {
//   const params = {
//     id: id,
//     startMonth: {
//       __type: "Date",
//       "iso": startMonth
//     },
//     endMonth: {
//       __type: "Date",
//       "iso": endMonth
//     }
//   };

//   try {
//     const response = await Axios.post('functions/getMonthlyreport', params);
//     const base64Data = response.data.result;
    
//     // convert base64 to pdf 
//     const dirs = RNFetchBlob.fs.dirs;
//     const path = `${dirs.DownloadDir}/report.pdf`;
//     RNFetchBlob.fs.writeFile(path, base64Data, 'base64')
//       .then(() => {
//         console.log('File written');
//       }

//       )
//       .catch((error) => {
//         console.error(error);
//       }
//       );
//     console.log(path)
//       return path;
    
//   } catch (error) {
//     console.error(error);
//     return "error";
//   }
// };


export const checkAccount = async (_name, _phone) => {
  const user = await fetchUser();
  const params = {
    where: {
      name: _name,
      phone: _phone,
      userId: {
        __type: "Pointer",
        className: "_User",
        objectId: user.objectId
      }
    }
  }

  return Axios
    .get('classes/Account', { params })
    .then(res => res.data.results)
    .catch(err => console.log(err));
};

export const getAccountByName = async (_name) => {
  const user = await fetchUser();
  const params = {
    limit: 1,
    where: {
      name: _name,
      userId: {
        __type: "Pointer",
        className: "_User",
        objectId: user.objectId
      }
    }
  };

  return Axios
    .get('classes/Account', { params })
    .then(res => res.data.results[0])
    .catch(error => error.message);
}

export const getAccountByNameAndNumber = async (_name, number) => {
  const user = await fetchUser();
  const params = {
    limit: 1,
    where: {
      name: _name,
      number: number,
      userId: {
        __type: "Pointer",
        className: "_User",
        objectId: user.objectId
      }
    }
  };

  return Axios
    .get('classes/Account', { params })
    .then(res => res.data.results[0])
    .catch(error => error.message);
}

// get installaion id fromm seccion collection using token 
export const getInstallationId = async (token) => {
  const user = await fetchUser();
  const params = {
    limit: 1,
    where: {
      sessionToken: token,
      userId: {
        __type: "Pointer",
        className: "_User",
        objectId: user.objectId 
      }
    }
  };
   
  return Axios
    .get('classes/Session', { params })
    .then(res => res.data.results[0])
    .catch(error => error.message);
}

// set auto sms status of account to send sms
export const setAutoSms = async (id, checkboxSMS) => {
  const body = {
    checkboxSMS: checkboxSMS
  }

  return Axios
    .put(`classes/Account/${id}`, body)
    .then(res => res.data)
    .catch(error => error.message);
}

export const getAccountById = async (id) => {
  const user = await fetchUser();

  const params = {
    limit: 1,
    where: {
      objectId: id,
      userId: {
        __type: "Pointer",
        className: "_User",
        objectId: user.objectId
      }
    }
  };

  return Axios
    .get('classes/Account', { params })
    .then(res => res.data.results[0])
    .catch(error => error.message);
}

// Close account only if balance is zero 
export const closeAccount = async (id) => {
  const user = await fetchUser();

  const body = {
    cancelled: true
  }

  return Axios
    .put(`classes/Account/${id}`, body)
    .then(res => res.data)
    .catch(error => error.message);
}

// reopern accout 
export const reOpenAccount = async (id) => {
  const user = await fetchUser();

  const body = {
    cancelled: false
  }

  return Axios
    .put(`classes/Account/${id}`, body)
    .then(res => res.data)
    .catch(error => error.message);
}

export const updateAccount = async (id, _name, _phone, _cancelled) => {
  const user = await fetchUser();

  const body = {
    ... (_name && { name: _name }),
    ... ( { phone: _phone }),
    ... (_cancelled && { cancelled: _cancelled })
  }
  return Axios
    .put(`classes/Account/${id}`, body)
    .then(res => res.data)
    .catch(error => error.message);
}

export const getContacts = async networkOnly => {
  const user = await fetchUser();
  const params = {
    limit: 1,
    where: {
      userId: {
        __type: 'Pointer',
        className: '_User',
        objectId: user.objectId,
      },
    },
  };

  if (!networkOnly) {
    const contacts = await AsyncStorage.getItem('phoneBook');
    if (contacts) {
      return JSON.parse(contacts);
    }
  }

  return Axios.get('classes/Contacts', {params})
    .then(res => res.data.results)
    .catch(error => error.message);
};

// set contact list in contact collection
export const setContacts = async (phonebook) => {
  const user = await fetchUser();
  const connectionState = await netInfo.fetch()
  await AsyncStorage.setItem('phoneBook', JSON.stringify(phonebook));
  if (!connectionState.isConnected) {
    console.log('No internet connection');
    await AsyncStorage.setItem('syncPending', 'true');
    return;
  }

  const params = {
    limit: 1,
    where: {
      userId: {
        __type: "Pointer",
        className: "_User",
        objectId: user.objectId
      }
    }
  };

  return Axios
    .get('classes/Contacts', { params })
    .then(async(res) => {
      if (res.data.results.length > 0) {
        const body = {
          phonebook: phonebook
        }
        return Axios
          .put(`classes/Contacts/${res.data.results[0].objectId}`, body)
          .then(res => res.data)
          .catch(error => error.message);
      }
      else {
        const body = {
          phonebook: phonebook,
          userId: {
            __type: "Pointer",
            className: "_User",
            objectId: user.objectId
          }
        }
        return Axios
          .post('classes/Contacts', body)
          .then(res => res.data)
          .catch(error => error.message);
      }
    })
    .catch(error => {
      return error.message;
    });
}

const uploadImage = async (imageUri) => {
  const filename = "photo." + imageUri.split('/').pop().split('.').pop()
  const data = new FormData();
  data.append('file', {
    uri: Platform.OS === 'android' ? imageUri : imageUri.replace('file://', ''),
    type: 'image/png',
    name: filename,
  });
  try {

    return Axios.post(`/files/${filename}`, data, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    }).then((response) => {
      return response.data.url;
    });



  } catch (error) {

    console.error(error);
    return error;
  }
};

export const transaction = async (_account, amount, detail, attachment, date, multiplier) => {
  const user = await fetchUser();
  try {

    let url = null;

    if (attachment) {
      url = await uploadImage(attachment);
    }

    if (_account) {
      const accounts = await getAccountByName(_account);
      if (accounts) {
        try {
          const transactionBody = {
            amount: parseInt(amount) * multiplier,
            detail: detail,
            attachment: url ? url : null,
            cancelled: false,
            userId: {
              __type: "Pointer",
              className: "_User",
              objectId: user.objectId
            },
            accountId: {
              __type: "Pointer",
              className: "Account",
              objectId: accounts.objectId
            },
            date: {
              __type: "Date",
              "iso": date
            }
          };
          const transactionResponse = await Axios.post('classes/Transaction', transactionBody);
          return { status: transactionResponse.status, id: accounts.objectId };

        } catch (error) {
          console.error("Error occurred:", error);
          throw error; 
        }
      }
      else {
        try {
          const accountResponse = await account(_account, null);
          const transactionBody = {
            amount: parseInt(amount) * multiplier,
            detail: detail,
            attachment: url ? url : null,
            cancelled: false,
            userId: {
              __type: "Pointer",
              className: "_User",
              objectId: user.objectId
            },
            accountId: {
              __type: "Pointer",
              className: "Account",
              objectId: accountResponse.objectId
            },
            date: {
              __type: "Date",
              "iso": date
            }
          };

          const transactionResponse = await Axios.post('classes/Transaction', transactionBody);
          return { status: transactionResponse.status, id: accountResponse.objectId };

        } catch (error) {
          console.error("Error occurred:", error);
          throw error; // Throw the error to handle it at the caller level
        }
      }
    }
    else {
      try {
        const transactionBody = {
          attachment: url ? url : null,
          amount: parseInt(amount) * multiplier,
          detail: detail,
          cancelled: false,
          userId: {
            __type: "Pointer",
            className: "_User",
            objectId: user.objectId
          },
          date: {
            __type: "Date",
            "iso": date
          }
        };
        const transactionResponse = await Axios.post('classes/Transaction', transactionBody);
        return { status: transactionResponse.status };
        
      } catch (error) {
        console.error("Error occurred:", error);
        throw error; 
      }
    }
  } catch (err) {
    console.log(err);
  }
}

export const updateBalance = async (id) => {

  const user = await fetchUser();

  const params = {
    where: {
      userId: {
        __type: "Pointer",
        className: "_User",
        objectId: user.objectId
      },
      accountId: {
        __type: "Pointer",
        className: "Account",
        objectId: id
      }
    },
    limit: 1000000
  };
  return Axios
    .get('classes/Transaction', { params })
    .then(async (res) => {
      var sum = 0;
      var results = res.data.results;
      results.map((result) => {
        if (result.cancelled == false) {
          sum += result.amount
        }
      })

      const body = {
        balance: sum
      }
      return Axios
        .put(`classes/Account/${id}`, body)
        .then(res => res.data)
        .catch(error => error.message);
    }
    )
    .catch(error => error.message);

}

export const getTransactions = async (first, next) => {
  const user = await fetchUser();
  const params = {
    where: {
      date: {
        $gt: {
          __type: 'Date',
          iso: first
        },
        $lte: {
          __type: 'Date',
          iso: next
        }
      },
      userId: {
        __type: "Pointer",
        className: "_User",
        objectId: user.objectId
      }
    },
    include: "accountId",
    limit: 1000000
  };

  return Axios
    .get('classes/Transaction', { params })
    .then(res => res.data)
    .catch(error => error.message);
}

export const getSumOfAmountById = async (id) => {
  const user = await fetchUser();
  const params = {
    where: {
      accountId: {
        __type: "Pointer",
        className: "Account",
        objectId: id
      },
      userId: {
        __type: "Pointer",
        className: "_User",
        objectId: user.objectId
      }
    }
  };

  return Axios
    .get('classes/Transaction', { params })
    .then(res => {
      var sum = 0;
      var results = res.data.results;
      results.map((result) => {
        if (result.cancelled == false) {
          sum += result.amount;
        }
      }
      )
      return sum;
    })
    .catch(error => error.message);
}

export const getAllTransactions = async thresholdTime => {
  const user = await fetchUser();

  const params = {
    where: {
      userId: {
        __type: 'Pointer',
        className: '_User',
        objectId: user.objectId,
      },
      $or: thresholdTime
        ? [
            {
              createdAt: {
                $gte: {
                  __type: 'Date',
                  iso: new Date(thresholdTime).toISOString(),
                },
              },
            },
            {
              updatedAt: {
                $gte: {
                  __type: 'Date',
                  iso: new Date(thresholdTime).toISOString(),
                },
              },
            },
          ]
        : undefined,
    },
    limit: 100,
    count: 1,
    order: '-updatedAt',
  };

  const {data, count} = await Axios.get('classes/Transaction', {params}).then(
    res => {
      return {data: res.data.results, count: res.data.count};
    },
  );

  const write = useStore.getState().write;
  write(
    produce(state => {
      state.syncInfo.transactions.total = count;
      state.syncInfo.transactions.synced = data.length;
    }),
  );

  const max_pages = Math.ceil(count / 100);
  const concurrentRequestsLimit = 5;

  let promises = [];

  for (let i = 1; i < max_pages; i++) {
    params.skip = i * 100;
    params.count = undefined; // count is not needed for subsequent requests

    promises.push(Axios.get('classes/Transaction', {params}));

    if (i % concurrentRequestsLimit === 0 || i === max_pages - 1) {
      await Promise.all(promises).then(res => {
        res.map(r => {
          data.push(...r.data.results);
        });
      });
      promises = [];
      write(
        produce(state => {
          state.syncInfo.transactions.synced = data.length;
        }),
      );
    }
  }

  return data;
};

export const getAllAccounts = async thresholdTime => {
  const user = await fetchUser();
  const params = {
    where: {
      userId: {
        __type: 'Pointer',
        className: '_User',
        objectId: user.objectId,
      },
      $or: thresholdTime
        ? [
            {
              createdAt: {
                $gte: {
                  __type: 'Date',
                  iso: new Date(thresholdTime).toISOString(),
                },
              },
            },
            {
              updatedAt: {
                $gte: {
                  __type: 'Date',
                  iso: new Date(thresholdTime).toISOString(),
                },
              },
            },
          ]
        : undefined,
    },
    limit: 100,
    count: 1,
    order: '-updatedAt',
  };

  const {data, count} = await Axios.get('classes/Account', {params}).then(
    res => {
      return {data: res.data.results, count: res.data.count};
    },
  );
  const write = useStore.getState().write;
  write(
    produce(state => {
      state.syncInfo.accounts.total = count;
      state.syncInfo.accounts.synced = data.length;
    }),
  );

  const max_pages = Math.ceil(count / 100);
  const concurrentRequestsLimit = 5;

  let promises = [];

  for (let i = 1; i < max_pages; i++) {
    params.skip = i * 100;
    params.count = undefined; // count is not needed for subsequent requests

    promises.push(Axios.get('classes/Account', {params}));

    if (i % concurrentRequestsLimit === 0 || i === max_pages - 1) {
      await Promise.all(promises).then(res => {
        res.map(r => {
          data.push(...r.data.results);
        });
      });
      promises = [];
      write(
        produce(state => {
          state.syncInfo.accounts.synced = data.length;
        }),
      );
    }
  }

  return data;
};

// get transaction by month and id of user 
export const getTxnByMonth = async (id, first, next) => {
  const user = await fetchUser();
  const params = {
    where: {
      accountId: {
        __type: "Pointer",
        className: "Account",
        objectId: id
      },
      date: {
        $gt: {
          __type: 'Date',
          iso: first
        },
        $lte: {
          __type: 'Date',
          iso: next
        }
      },
      userId: {
        __type: "Pointer",
        className: "_User",
        objectId: user.objectId
      }
    },
    limit: 1000000,
  };

  return Axios
    .get('classes/Transaction', { params })
    .then(res => res.data.results)
    .catch(error => error.message);
}

export const getTxnById = async (id) => {
  const user = await fetchUser();

  const params = {
    limit: 1,
    where: {
      objectId: id,
      userId: {
        __type: "Pointer",
        className: "_User",
        objectId: user.objectId
      }
    }
  };
  return Axios
    .get('classes/Transaction', { params })
    .then(res => res.data)
    .catch(error => error.message);
}

export const getTxnsByAccountId = async (accountId, first, next) => {
  const user = await fetchUser();
  const params = {
    where: {
      accountId: {
        __type: "Pointer",
        className: "Account",
        objectId: accountId
      },
      date: {
        $gt: {
          __type: 'Date',
          iso: first
        },
        $lte: {
          __type: 'Date',
          iso: next
        }
      },
      userId: {
        __type: "Pointer",
        className: "_User",
        objectId: user.objectId
      }
    }
  };

  return Axios
    .get('classes/Transaction', { params })
    .then(res => res.data.results)
    .catch(error => error.message);
}

export const updateTransaction = async (id, _detail) => {
  const user = await fetchUser();

  const body = {
    detail: _detail
  }

  return Axios
    .put(`classes/Transaction/${id}`, body)
    .then(res => res.data)
    .catch(error => error.message);

}

export const deleteTransaction = async (id) => {
  const user = await fetchUser();

  const body = {
    cancelled: true
  }

  return Axios
    .put(`classes/Transaction/${id}`, body)
    .then(res => res.data)
    .catch(error => error.message);

}

export const getOpenAmount = async (_date) => {
  const user = await fetchUser();
  const params = {
    where: {
      userId: {
        __type: "Pointer",
        className: "_User",
        objectId: user.objectId
      },
      date: {
        $lt: {
          __type: 'Date',
          iso: _date
        }
      }
    },
    limit: 1000000,

  };

  return Axios
    .get('classes/Transaction', { params })
    .then(res => {
      var sum = 0;
      var results = res.data.results;
      results.map((result) => {
        if (result.cancelled == false) {
          sum += result.amount;
        }
      })
      return sum;
    })
    .catch(error => error.message);
}

export const getCloseAmount = async (_date) => {
  const user = await fetchUser();
  const params = {
    where: {
      userId: {
        __type: "Pointer",
        className: "_User",
        objectId: user.objectId
      },
      date: {
        $lte: {
          __type: 'Date',
          iso: _date
        }
      }
    },
    limit: 1000000,


  };

  return Axios
    .get('classes/Transaction', { params })
    .then(res => {
      var sum = 0;
      var results = res.data.results;
      results.map((result) => {
        if (result.cancelled == false) {
          sum += result.amount;
        }
      })
      return sum;
    })
    .catch(error => error.message);
}

// get closing balance date and id of account
export const getClosingBalance = async (id, _date) => {
  const user = await fetchUser();
  const params = {
    where: {
      accountId: {
        __type: "Pointer",
        className: "Account",
        objectId: id
      },
      date: {
        $lte: {
          __type: 'Date',
          iso: _date
        }
      },
      userId: {
        __type: "Pointer",
        className: "_User",
        objectId: user.objectId
      }
    },
    limit: 1000000,

  };

  return Axios
    .get('classes/Transaction', { params })
    .then(res => {
      var sum = 0;
      var results = res.data.results;
      results.map((result) => {
        if (result.cancelled == false) {
          sum += result.amount;
        }
      })
      return sum;
    })
    .catch(error => error.message);
}

export const getOpeningBalance = async (id, _date) => {
  const user = await fetchUser();
  const params = {
    where: {
      accountId: {
        __type: "Pointer",
        className: "Account",
        objectId: id
      },
      date: {
        $lt: {
          __type: 'Date',
          iso: _date
        }
      },
      userId: {
        __type: "Pointer",
        className: "_User",
        objectId: user.objectId
      }
    },
    limit: 1000000,

  };

  return Axios
    .get('classes/Transaction', { params })
    .then(res => {
      var sum = 0;
      var results = res.data.results;
      results.map((result) => {
        if (result.cancelled == false) {
          sum += result.amount;
        }
      })
      return sum;
    })
    .catch(error => error.message);
} 