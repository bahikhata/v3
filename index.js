/**
 * @format
 */
import React from 'react';
import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import Dataprovider from "./App/context/Dataprovider"
import { RealmInit } from "./App/components/realm.init";

const RNRedux = () => (
  <Dataprovider>
    <App />
    <RealmInit />
  </Dataprovider>
);

AppRegistry.registerComponent(appName, () => RNRedux);
